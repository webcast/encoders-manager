#!/bin/bash

echo 'Starting app...'
# We run the application using uwsgi
uwsgi --ini /usr/src/server/uwsgi.ini --mimefile /usr/src/etc/mime.types
