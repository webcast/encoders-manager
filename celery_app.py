import urllib3

from app.app_factory import celery_init_app, create_app
from app.utils.logger import setup_logs

urllib3.disable_warnings()

# Initialize the Celery worker to run background tasks
app = create_app()
celery = celery_init_app(app)

print("Initializing logs...", end="")
setup_logs(app, "job", to_file=app.config.get("LOG_FILE_ENABLED", False))
print("ok")
