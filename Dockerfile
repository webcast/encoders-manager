FROM registry.cern.ch/docker.io/library/python:3.13

EXPOSE 8080

# The user id must be a generic one, since it is used in the OpenShift cluster
ENV USER_ID 1001

WORKDIR /usr/src

ENV PYTHONPATH "${PYTHONPATH}:/usr/src"
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV POETRY_VERSION 2.1.1
ENV POETRY_HOME /opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT true
ENV POETRY_CACHE_DIR /usr/src/.cache
ENV VIRTUAL_ENVIRONMENT_PATH /usr/src/.venv

# Adding the virtual environment to PATH in order to "activate" it.
# https://docs.python.org/3/library/venv.html#how-venvs-work
ENV PATH="$VIRTUAL_ENVIRONMENT_PATH/bin:$PATH"

RUN apt-get update && \
    apt-get install -y \
    default-libmysqlclient-dev \
    gcc \
    netcat-openbsd \
    pkg-config && \
    rm -rf /var/lib/apt/lists/*

# Install Poetry and dependencies
COPY pyproject.toml ./
COPY poetry.lock ./

# Using Poetry to install dependencies without requiring the project main files to be present
RUN pip install poetry==${POETRY_VERSION} && poetry install --only main --no-root --no-directory

COPY ./app /usr/src/app
COPY ./etc /usr/src/etc
COPY ./server /usr/src/server
COPY ./wsgi.py /usr/src/wsgi.py
COPY ./celery_app.py /usr/src/celery_app.py
COPY ./migrations /usr/src/migrations

RUN chmod +x /usr/src/etc/entrypoint.sh

ENTRYPOINT ["/usr/src/etc/entrypoint.sh"]
