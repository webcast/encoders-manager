import { toast } from "react-toastify";
import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";
import apiClient from "api/api-headers";
import queryClient from "api/query-client";

export const putStartTest = async (groupId: number) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/test/${groupId}/start/`,
  );
  return data;
};

export interface GroupForm {
  id: number;
}

export interface StreamForm {
  id: number;
  indicoId: string;
}

export function useStartTest(options = {}) {
  return useMutation(
    (groupDetails: GroupForm) => putStartTest(groupDetails.id),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(
            `Test streaming started on encoder group ${variables.id}`,
          );
        } else {
          toast.error(
            `Error starting test streaming on encoder group ${variables.id}`,
          );
        }
      },
    },
  );
}

export const putStopTest = async (groupId: number) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/test/${groupId}/stop/`,
  );
  return data;
};

export function useStopTest(options = {}) {
  return useMutation(
    (groupDetails: GroupForm) => putStopTest(groupDetails.id),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Test stream stopped on encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error stopping test stream on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(
            `Error stopping test stream (${error?.response?.status})`,
          );
        }
      },
    },
  );
}

export const putSartStream = async (groupId: number, indicoId: string) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/stream/${groupId}/start/`,
    { indicoId },
  );
  return data;
};

export function useStartStream(options = {}) {
  return useMutation(
    (groupDetails: StreamForm) =>
      putSartStream(groupDetails.id, groupDetails.indicoId),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Streaming started on encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error starting streaming on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(`Error starting stream (${error?.response?.status})`);
        }
      },
    },
  );
}

export const putStopStream = async (groupId: number) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/stream/${groupId}/stop/`,
  );
  return data;
};

export function useStopStream(options = {}) {
  return useMutation(
    (groupDetails: GroupForm) => putStopStream(groupDetails.id),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Streaming stopped on encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error stopping streaming on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(
            `Error stopping test stream (${error?.response?.status})`,
          );
        }
      },
    },
  );
}

export const putRestartStream = async (groupId: number, indicoId: string) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/stream/${groupId}/restart/`,
    { indicoId },
  );
  return data;
};

export function useRestartStream(options = {}) {
  return useMutation(
    (groupDetails: StreamForm) =>
      putRestartStream(groupDetails.id, groupDetails.indicoId),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Stream restarted on encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error restarting streaming on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(`Error restarting stream (${error?.response?.status})`);
        }
      },
    },
  );
}

export const putSartAll = async (groupId: number, indicoId: string) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/all/${groupId}/start/`,
    { indicoId },
  );
  return data;
};

export function useStartAll(options = {}) {
  return useMutation(
    (groupDetails: StreamForm) =>
      putSartAll(groupDetails.id, groupDetails.indicoId),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(
            `Recording and streaming started on encoder group ${variables.id}`,
          );
        } else {
          toast.error(
            `Error starting recording and streaming on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(`Error starting encoders (${error?.response?.status})`);
        }
      },
    },
  );
}

export const putStopAll = async (groupId: number) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/all/${groupId}/stop/`,
  );
  return data;
};

export function useStopAll(options = {}) {
  return useMutation((groupDetails: GroupForm) => putStopAll(groupDetails.id), {
    ...options,
    onSuccess: (data, variables) => {
      queryClient.invalidateQueries(["group-encoders"]);
      queryClient.invalidateQueries(["indico"]);
      if (data && !data.error) {
        toast.success(
          `Recording and Streaming stopping on encoder group ${variables.id}`,
        );
      } else {
        toast.error(
          `Error stopping recording and streaming on encoder group ${variables.id}`,
        );
      }
    },
    onError: (error: AxiosError) => {
      console.error(error);
      // If its a 401 error, it means the user is not authenticated
      if (error?.response?.status === 401) {
        toast.error("You session has expired, please reload the page.");
      } else {
        toast.error(`Error stopping encoders (${error?.response?.status})`);
      }
    },
  });
}

export const putSartRecording = async (groupId: number, indicoId: string) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/recording/${groupId}/start/`,
    { indicoId },
  );
  return data;
};

export function useStartRecording(options = {}) {
  return useMutation(
    (groupDetails: StreamForm) =>
      putSartRecording(groupDetails.id, groupDetails.indicoId),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Recording started on encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error starting recording on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(`Error starting recording (${error?.response?.status})`);
        }
      },
    },
  );
}

export const putStopRecording = async (groupId: number) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/recording/${groupId}/stop/`,
  );
  return data;
};

export function useStopRecording(options = {}) {
  return useMutation(
    (groupDetails: GroupForm) => putStopRecording(groupDetails.id),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Recording stopped on encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error stopping recording on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(`Error stopping recording (${error?.response?.status})`);
        }
      },
    },
  );
}

export const putRestartRecording = async (
  groupId: number,
  indicoId: string,
) => {
  const { data } = await apiClient.put<IActionResponse>(
    `/recording/${groupId}/restart/`,
    { indicoId },
  );
  return data;
};

export function useRestartRecording(options = {}) {
  return useMutation(
    (groupDetails: StreamForm) =>
      putRestartRecording(groupDetails.id, groupDetails.indicoId),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Recording restarted on encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error restarting recording on encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error("You session has expired, please reload the page.");
        } else {
          toast.error(
            `Error restarting recording (${error?.response?.status})`,
          );
        }
      },
    },
  );
}

export const putGetStatus = async (groupId: number) => {
  const { data } = await apiClient.put<IActionResponse>(`/status/${groupId}/`);
  return data;
};

export function useGetStatus(options = {}) {
  return useMutation(
    (groupDetails: GroupForm) => putGetStatus(groupDetails.id),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["group-encoders"]);
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(`Status updated for encoder group ${variables.id}`);
        } else {
          toast.error(
            `Error updating status for encoder group ${variables.id}`,
          );
        }
      },
      onError: (error: AxiosError) => {
        console.error(error);
        // If its a 401 error, it means the user is not authenticated
        if (error?.response?.status === 401) {
          toast.error(
            `You session has expired, please reload the page. (${error?.response?.status})`,
          );
        } else {
          toast.error(`Error getting status (${error?.response?.status})`);
        }
      },
    },
  );
}
