import { toast } from "react-toastify";
import { useMutation, useQuery } from "@tanstack/react-query";
import apiClient from "api/api-headers";
import queryClient from "api/query-client";

export const fetchIndicoEvents = async () => {
  const { data } = await apiClient.get("/indico/");
  return data;
};

export function useIndicoEvents() {
  return useQuery<IIndicoEvent[], Error>(["indico"], fetchIndicoEvents);
}

interface UpdateLockForm {
  id: number;
  isLocked: boolean;
}

export const patchUpdateLock = async (eventId: number, isLocked: boolean) => {
  const { data } = await apiClient.patch<IActionResponse>(
    `/indico/${eventId}/`,
    {
      isLocked,
    },
  );
  return data;
};

export function useUpdateLock(options = {}) {
  return useMutation(
    (groupDetails: UpdateLockForm) =>
      patchUpdateLock(groupDetails.id, groupDetails.isLocked),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          if (variables.isLocked) {
            toast.success(`Event #${variables.id} has been archived`);
          } else {
            toast.success(`Event #${variables.id} locked to the dashboard`);
          }
        } else {
          toast.error(
            `Error changing the locked status of event #${variables.id}`,
          );
        }
      },
      onError: (data, variables) => {
        toast.error<string>(
          `Error changing the locked status of event #${variables.id}`,
        );
      },
    },
  );
}

interface UpdateAutoStopForm {
  id: number;
  autoStopDatetime: string | null;
}

export const patchUpdateAutoStop = async (
  eventId: number,
  autoStopDatetime: string | null,
) => {
  const { data } = await apiClient.patch<IActionResponse>(
    `/indico/${eventId}/`,
    {
      autoStopDatetime,
    },
  );
  return data;
};

export function useUpdateAutoStop(options = {}) {
  return useMutation(
    (groupDetails: UpdateAutoStopForm) =>
      patchUpdateAutoStop(groupDetails.id, groupDetails.autoStopDatetime),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["indico"]);
        if (data && !data.error) {
          toast.success(
            `Event #${variables.id} auto stop set set to ${variables.autoStopDatetime}`,
          );
        } else {
          toast.error(`Error changing the auto stop of event #${variables.id}`);
        }
      },
      onError: () => {
        toast.error<string>("Unable to change the auto stop of the event");
      },
    },
  );
}

interface FetchSingleIndicoEventForm {
  indicoId: string;
  audience: IIndicoEventAudience;
  eventType: IIndicoEventType;
}

export const fetchSingleIndicoEvent = async (
  indicoId: string,
  audience: IIndicoEventAudience,
  eventType: IIndicoEventType,
) => {
  const { data } = await apiClient.post<IIndicoEvent>(`/indico/fetch-event/`, {
    indicoId,
    audience,
    eventType,
  });
  return data;
};

export function useFetchSingleIndicoEvent(options = {}) {
  return useMutation(
    (details: FetchSingleIndicoEventForm) =>
      fetchSingleIndicoEvent(
        details.indicoId,
        details.audience,
        details.eventType,
      ),
    {
      ...options,
      onSuccess: (data, variables) => {
        queryClient.invalidateQueries(["indico"]);
        if (data) {
          toast.success(`Event ${variables.indicoId} fetched from Indico`);
        } else {
          toast.error(`Unable to fetch Indico Event ${variables.indicoId}`);
        }
      },
      onError: (data, variables) => {
        toast.error<string>(
          `Unable to fetch Indico Event ${variables.indicoId}`,
        );
      },
    },
  );
}

export const fetchNewIndicoEvents = async () => {
  const { data } = await apiClient.post<IIndicoEvent[]>(`/indico/fetch/`);
  return data;
};

export function useFetchIndicoEvents(options = {}) {
  return useMutation(() => fetchNewIndicoEvents(), {
    ...options,
    onSuccess: (data) => {
      queryClient.invalidateQueries(["indico"]);
      if (data) {
        toast.success(`Events fetched from Indico: ${data.length}`);
      } else {
        toast.error(`Unable to fetch new events from Indico`);
      }
    },
    onError: () => {
      toast.error<string>(`Unable to fetch new events from Indico`);
    },
  });
}
