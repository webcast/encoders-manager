import { toast } from "react-toastify";
import { useMutation, useQuery } from "@tanstack/react-query";
import { AxiosError } from "axios";
import apiClient from "api/api-headers";

export interface EncoderForm {
  id: number;
}

export const fetchEncoders = async () => {
  const { data } = await apiClient.get(`/encoders/`);
  return data;
};

export function useEncoders() {
  return useQuery<IEncoder[], Error>(["encoders"], fetchEncoders);
}

export const postSendEmail = async (encoderId: number) => {
  const { data } = await apiClient.post<IActionResponse>(
    `/encoders/${encoderId}/status-email/`,
  );
  return data;
};

export function useSendEmail(options = {}) {
  return useMutation((encoderId: EncoderForm) => postSendEmail(encoderId.id), {
    ...options,
    onSuccess: (data, variables) => {
      if (data && !data.error) {
        toast.success(`Status email sent (Encoder: ${variables.id})`);
      } else {
        toast.error(`Unable to send status email (Encoder: ${variables.id})`);
      }
    },
    onError: (error: AxiosError) => {
      console.error(error);
      // If its a 401 error, it means the user is not authenticated
      if (error?.response?.status === 401) {
        toast.error(
          `You session has expired, please reload the page. (${error?.response?.status})`,
        );
      } else {
        toast.error(
          `Error sending the status email (${error?.response?.status})`,
        );
      }
    },
  });
}

export const postCheckAllEncoders = async () => {
  const { data } = await apiClient.post<IActionResponse>(`/encoders/check/`);
  return data;
};

export function useCheckAllEncoders(options = {}) {
  return useMutation(() => postCheckAllEncoders(), {
    ...options,
    onSuccess: (data) => {
      if (data && !data.error) {
        toast.success(
          `Getting the status of all encoders. 
          This task will run on the background and you will need to reload the page`,
        );
      } else {
        toast.error(`Unable to check the status of all the encoders`);
      }
    },
    onError: (error: AxiosError) => {
      console.error(error);
      // If its a 401 error, it means the user is not authenticated
      if (error?.response?.status === 401) {
        toast.error(
          `You session has expired, please reload the page. (${error?.response?.status})`,
        );
      } else {
        toast.error(
          `Error getting the status of all the encoders (${error?.response?.status})`,
        );
      }
    },
  });
}
