import { useQuery } from "@tanstack/react-query";
import apiClient from "api/api-headers";

export const fetchGroupEncodersInRoom = async (roomId: number) => {
  const { data } = await apiClient.get(`/encoder-groups/room/${roomId}/`);
  return data;
};

export function useRoomGroupEncoders(roomId: number) {
  return useQuery<IEncoderGroup[], Error>(["group-encoders", roomId], () =>
    fetchGroupEncodersInRoom(roomId),
  );
}

export const fetchGroupEncoders = async () => {
  const { data } = await apiClient.get(`/encoder-groups/`);
  return data;
};

export function useGroupEncoders() {
  return useQuery<IEncoderGroup[], Error>(
    ["group-encoders"],
    fetchGroupEncoders,
  );
}

export const fetchGroupEncoderGroup = async (id: string | undefined) => {
  const { data } = await apiClient.get(`/encoder-groups/${id}/`);
  return data;
};

export function useEncoderGroup(id: string | undefined) {
  return useQuery<IEncoderGroup, Error>(
    ["group-encoders", id],
    () => fetchGroupEncoderGroup(id),
    {
      enabled: !!id,
    },
  );
}

export const fetchRunningGroupEncoders = async () => {
  const { data } = await apiClient.get(`/encoder-groups/?running=true`);
  return data;
};

export function useGroupEncodersRunning() {
  return useQuery<IEncoderGroup[], Error>(
    ["group-encoders", "running"],
    fetchRunningGroupEncoders,
  );
}

export const fetchNotRunningGroupEncoders = async () => {
  const { data } = await apiClient.get(`/encoder-groups/?notRunning=true`);
  return data;
};

export function useGroupEncodersNotRunning() {
  return useQuery<IEncoderGroup[], Error>(
    ["group-encoders", "not-running"],
    fetchNotRunningGroupEncoders,
  );
}
