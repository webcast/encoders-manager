import { BrowserRouter, Route, Routes } from "react-router-dom";
import { QueryClientProvider } from "@tanstack/react-query";
import { Dimmer, Loader, Message } from "semantic-ui-react";
import "./App.css";
import queryClient from "./api/query-client";
import App from "./App";
import AuthService from "./services/auth-service";
// import config from "config";
import EncodersStatusPage from "pages/EncodersStatusPage/EncodersStatusPage";
import HomePage from "pages/HomePage/HomePage";
import LoginPage from "pages/LoginPage/LoginPage";
import MonitorPage from "pages/MonitorPage/MonitorPage";

interface Props {
  initialized: boolean;
}

function Main({ initialized }: Props) {
  if (!initialized) {
    return (
      <div style={{ height: 100, width: 100 }}>
        <Dimmer active inverted>
          <Loader inverted />
        </Dimmer>
      </div>
    );
  }

  if (
    AuthService.isLoggedIn()
    // &&
    // AuthService.hasRole(config.OIDC ? [config.OIDC.ADMIN_ROLE] : ["admin"])
  ) {
    return (
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Routes>
            <Route path="/app-login" element={<LoginPage />} />
            <Route path="/" element={<App />}>
              <Route path="" element={<HomePage />} />
              <Route path="monitor/:id/" element={<MonitorPage />} />
              <Route path="status" element={<EncodersStatusPage />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </QueryClientProvider>
    );
  }

  return (
    <div style={{ height: 100, width: 100 }}>
      <Dimmer active>
        <Message
          error
          icon="error"
          header="You don't have access to this application"
          content="if you think this is a mistake, please contact your administrator"
        />
      </Dimmer>
    </div>
  );
}

export default Main;
