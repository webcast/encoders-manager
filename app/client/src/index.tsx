import * as Sentry from "@sentry/react";
import ReactDOM from "react-dom/client";
import setupInterceptors from "api/axios-interceptors";
import "semantic-ui-css-offline/semantic.min.css";
import "react-toastify/dist/ReactToastify.css";
import "./index.css";
import config from "config";
import MainAuth from "MainAuth";

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    dsn: config.SENTRY_DSN,
    environment: config.SENTRY_ENV,
    integrations: [
      new Sentry.BrowserTracing({
        // Set 'tracePropagationTargets' to control for which URLs distributed tracing should be enabled
        tracePropagationTargets: [
          "localhost",
          /^https:\/\/encoders-qa\.web\.cern\.ch/,
          /^https:\/\/encoders\.web\.cern\.ch/,
        ],
      }),
      new Sentry.Replay(),
    ],
    // Performance Monitoring
    tracesSampleRate: 1.0, //  Capture 100% of the transactions
    // Session Replay
    replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
    replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
  });
}

setupInterceptors();

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

root.render(<MainAuth />);
