import { useEffect } from "react";
import { Button } from "semantic-ui-react";
import { useStopAll } from "hooks/encoder-actions/use-encoder-actions";

type Props = {
  groupId: number;
  onData: (data: any) => void;
  onError: (error: any) => void;
};

export default function StopallForm({ groupId, onData, onError }: Props) {
  const { mutate, isLoading, isSuccess, isError, data, error } = useStopAll();

  const onClick = () => {
    mutate({ id: groupId });
  };

  useEffect(() => {
    if (isSuccess) {
      onData(data);
    }
  }, [data, isError, isSuccess, onData]);

  useEffect(() => {
    if (isError) {
      onError(error);
    }
  }, [error, isError, isSuccess, onError]);

  return (
    <Button
      color="red"
      loading={isLoading}
      onClick={onClick}
      icon="stop"
      labelPosition="left"
      content="Stop all"
    />
  );
}
