import { Menu, Icon } from "semantic-ui-react";
import config from "../config";

export default function DevBanner() {
  if (!config.IS_DEV_INSTALL) {
    return null;
  }

  return (
    <Menu color="yellow" inverted widths={3} className="yellowMenu">
      <Menu.Item>
        <Icon name="exclamation triangle" /> This is a DEV Installation
      </Menu.Item>
    </Menu>
  );
}
