import { Menu, Container } from "semantic-ui-react";
import AuthService from "services/auth-service";

export default function TopMenu() {
  const logout = () => {
    AuthService.doLogout({
      redirectUri: `${encodeURI(window.location.href)}/`,
    });
  };

  const login = () => {
    AuthService.doLogin({
      redirectUri: encodeURI(window.location.href),
    });
  };

  if (AuthService.isLoggedIn()) {
    return (
      <Menu inverted fixed="top" style={{ display: "block" }}>
        <Container fluid>
          <Menu.Item name="CERN" />
          <Menu.Menu position="right">
            <Menu.Item name="loggedin">
              Logged in as: Logged in as: {AuthService.getFirstName()}{" "}
              {AuthService.getLastName()} ({AuthService.getUsername()})
            </Menu.Item>
            <Menu.Item name="logout" onClick={logout}>
              Logout
            </Menu.Item>
          </Menu.Menu>
        </Container>
      </Menu>
    );
  }

  return (
    <Menu inverted fixed="top" style={{ display: "block" }}>
      <Container fluid>
        <Menu.Item name="CERN" />
        <Menu.Menu position="right">
          <Menu.Item name="signup" onClick={login}>
            Login
          </Menu.Item>
        </Menu.Menu>
      </Container>
    </Menu>
  );
}
