import { DateTime } from "luxon";
import { Icon } from "semantic-ui-react";

interface IProps {
  startDate: string;
  endDate: string | undefined;
  showCalendarIcon?: boolean;
}

const convertDatetime = (date: string) => DateTime.fromISO(`${date}`);

const formatDatetime = (date: DateTime) =>
  date.toFormat("cccc dd LLL yyyy, HH:mm");

export default function EventDateText({
  startDate,
  endDate,
  showCalendarIcon = true,
}: IProps) {
  const startDatetime = convertDatetime(startDate);
  const formattedStartDatetime = formatDatetime(startDatetime);

  if (!endDate) {
    return (
      <span>
        {showCalendarIcon && <Icon name="calendar" />}
        {formattedStartDatetime}
      </span>
    );
  }

  const endDateTime = convertDatetime(endDate);
  const formattedEndDateTime = formatDatetime(endDateTime);

  if (startDatetime.day === endDateTime.day) {
    return (
      <span>
        {showCalendarIcon && <Icon name="calendar" />} {formattedStartDatetime}{" "}
        &rarr; {DateTime.fromISO(endDate).toFormat("HH:mm")}
      </span>
    );
  }

  return (
    <span>
      {showCalendarIcon && <Icon name="calendar" />} {formattedStartDatetime}{" "}
      &rarr; {formattedEndDateTime}
    </span>
  );
}

EventDateText.defaultProps = {
  showCalendarIcon: true,
};
