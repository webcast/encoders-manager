import { useEffect } from "react";
import { Button } from "semantic-ui-react";
import { useStopStream } from "hooks/encoder-actions/use-encoder-actions";

type Props = {
  groupId: number;
  onData: (data: any) => void;
  onError: (error: any) => void;
  basic?: boolean;
};

export default function StopStreamForm({
  groupId,
  onData,
  onError,
  basic,
}: Props) {
  const { mutate, data, error, isError, isSuccess, isLoading } =
    useStopStream();

  const onClick = () => {
    mutate({ id: groupId });
  };

  useEffect(() => {
    if (isSuccess) {
      onData(data);
    }
  }, [data, isError, isSuccess, onData]);

  useEffect(() => {
    if (isError) {
      onError(error);
    }
  }, [error, isError, isSuccess, onError]);

  return (
    <Button
      color="red"
      basic={basic}
      loading={isLoading}
      onClick={onClick}
      icon="stop"
      labelPosition="left"
      content="Stop streaming"
    />
  );
}

StopStreamForm.defaultProps = {
  basic: false,
};
