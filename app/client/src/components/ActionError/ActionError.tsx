import { Message } from "semantic-ui-react";
import handleErrorsFromEncoderAction from "utils/encoder-actions";

type Props = { data: IActionResponse; actionName?: string; title?: string };

export default function ActionError({ data, actionName, title }: Props) {
  return (
    <Message error>
      {title && <Message.Header>{title}</Message.Header>}
      <Message.Content>
        {actionName && data.error
          ? `Error on the encoder ${actionName}`
          : `Error on the encoder action`}
        {data.error && (
          <Message.List>
            {handleErrorsFromEncoderAction(data.result).map((result: any) => (
              <Message.Item key={result.hostname}>
                {result.hostname}: {result.message}
              </Message.Item>
            ))}
          </Message.List>
        )}
        {data.message ? data.message : null}
        {data.rollback_error ? "Error during the rollback of an encoder" : null}
        {data.rollback_error && (
          <Message.List>
            {handleErrorsFromEncoderAction(data.rollback).map((result: any) => (
              <Message.Item key={result.hostname}>
                {result.hostname}: {result.message}
              </Message.Item>
            ))}
          </Message.List>
        )}
      </Message.Content>
    </Message>
  );
}

ActionError.defaultProps = {
  actionName: null,
  title: null,
};
