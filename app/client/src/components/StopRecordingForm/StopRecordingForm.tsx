import { useEffect } from "react";
import { Button } from "semantic-ui-react";
import { useStopRecording } from "hooks/encoder-actions/use-encoder-actions";

type Props = {
  groupId: number;
  onData: (data: any) => void;
  onError: (error: any) => void;
  basic?: boolean;
};

export default function StopRecordingForm({
  groupId,
  onData,
  onError,
  basic,
}: Props) {
  const { mutate, data, isSuccess, error, isError, isLoading } =
    useStopRecording();

  const onClick = () => {
    mutate({ id: groupId });
  };

  useEffect(() => {
    if (isSuccess) {
      onData(data);
    }
  }, [data, isError, isSuccess, onData]);

  useEffect(() => {
    if (isError) {
      onError(error);
    }
  }, [error, isError, isSuccess, onError]);

  return (
    <Button
      basic={basic}
      color="red"
      loading={isLoading}
      onClick={onClick}
      icon="stop"
      labelPosition="left"
      content="Stop Recording"
    />
  );
}

StopRecordingForm.defaultProps = {
  basic: false,
};
