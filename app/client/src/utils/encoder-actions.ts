const handleErrorsFromEncoderAction = (resultDict: any) => {
  const resultList: any[] = [];
  Object.keys(resultDict).forEach((hostname) => {
    const { status, message } = resultDict[hostname];
    resultList.push({ hostname, status, message });
  });
  return resultList;
};

export default handleErrorsFromEncoderAction;
