const appBaseRoute = "/app/";
const homeRoute = "home";
const loginRoute = "app-login";
const logoutRoute = "app-logout";

const getRouteWithBase = (route: string) => `/${appBaseRoute}/${route}`;

export default {
  appBaseRoute,
  homeRoute,
  loginRoute,
  logoutRoute,
  getRouteWithBase,
};
