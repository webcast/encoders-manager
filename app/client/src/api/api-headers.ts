import axios from "axios";
import config from "config";

const apiClient = axios.create({
  baseURL: `${config.API.ENDPOINT}/api/private/v1`,
});

export default apiClient;
