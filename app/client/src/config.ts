import { IConfig } from "./types/config";

const dev: IConfig = {
  API: {
    ENDPOINT: "http://127.0.0.1:5000",
  },
  IS_DEV_INSTALL: true,
  OIDC: {
    ADMIN_ROLE: "admins",
    CLIENT_ID: import.meta.env.VITE_OIDC_CLIENT_ID
      ? import.meta.env.VITE_OIDC_CLIENT_ID
      : "mbp_rene",
    CLIENT_URL: import.meta.env.VITE_OIDC_CLIENT_URL
      ? import.meta.env.VITE_OIDC_CLIENT_URL
      : "http://localhost:3000",
  },
  SENTRY_DSN: import.meta.env.VITE_SENTRY_DSN
    ? import.meta.env.VITE_SENTRY_DSN
    : "",
  SENTRY_ENV: import.meta.env.VITE_SENTRY_ENV
    ? import.meta.env.VITE_SENTRY_ENV
    : "",
};

const prod: IConfig = {
  API: {
    ENDPOINT: import.meta.env.VITE_API_ENDPOINT
      ? import.meta.env.VITE_API_ENDPOINT
      : "<TODO>",
  },
  IS_DEV_INSTALL: import.meta.env.VITE_IS_DEV_INSTALL === "true",
  OIDC: {
    ADMIN_ROLE: "admins",
    CLIENT_ID: import.meta.env.VITE_OIDC_CLIENT_ID
      ? import.meta.env.VITE_OIDC_CLIENT_ID
      : "mbp_rene",
    CLIENT_URL: import.meta.env.VITE_OIDC_CLIENT_URL
      ? import.meta.env.VITE_OIDC_CLIENT_URL
      : "http://localhost:3000",
  },
  SENTRY_DSN: import.meta.env.VITE_SENTRY_DSN
    ? import.meta.env.VITE_SENTRY_DSN
    : "",
  SENTRY_ENV: import.meta.env.VITE_SENTRY_ENV
    ? import.meta.env.VITE_SENTRY_ENV
    : "",
};

const test: IConfig = {
  API: {
    ENDPOINT: "http://127.0.0.1:5000",
  },
  IS_DEV_INSTALL: true,
  OIDC: {
    CLIENT_ID: import.meta.env.VITE_OIDC_CLIENT_ID
      ? import.meta.env.VITE_OIDC_CLIENT_ID
      : "mbp_rene",
    CLIENT_URL: import.meta.env.VITE_OIDC_CLIENT_URL
      ? import.meta.env.VITE_OIDC_CLIENT_URL
      : "http://localhost:3000",
    ADMIN_ROLE: "admins",
  },
  SENTRY_DSN: "12345",
  SENTRY_ENV: "",
};

let tempConfig = test;

if (import.meta.env.PROD === true) {
  tempConfig = prod;
}

if (import.meta.env.DEV === true) {
  tempConfig = dev;
}
const config = tempConfig;

export default config;
