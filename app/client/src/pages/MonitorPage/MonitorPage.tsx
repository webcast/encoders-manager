import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Grid, Header, Segment } from "semantic-ui-react";
import EncoderDetails from "./components/EncoderDetails/EncoderDetails";
import ActionError from "components/ActionError/ActionError";
import StopAllForm from "components/StopAllForm/StopAllForm";
import StopRecordingForm from "components/StopRecordingForm/StopRecordingForm";
import StopStreamForm from "components/StopStreamForm/StopStreamForm";
import StopTestForm from "components/StopTestForm/StopTestForm";
import { useEncoderGroup } from "hooks/encoder-groups/use-encoder-groups";

export default function MonitorPage() {
  const { id } = useParams();
  const { data, isLoading, isError, error } = useEncoderGroup(id);
  const navigate = useNavigate();
  const [stopData, setStopData] = useState<IActionResponse | null>(null);
  const [stopError, setStopError] = useState(null);

  const updateData = (newData: any) => {
    setStopData(newData);
  };

  const updateError = (newError: any) => {
    setStopError(newError);
  };

  useEffect(() => {
    if (data && !data.isTestRunning && !data.isRunning) {
      console.log("Not running");
      navigate("/");
    }
  }, [data, navigate]);

  return (
    <Segment>
      {data && (
        <Header as="h1">
          {data.name} - {data.roomName} - {data.indicoEventTitle}
        </Header>
      )}
      {isLoading && <div>Loading...</div>}
      {isError && <div>Error: {JSON.stringify(error)}</div>}
      {stopData && stopData.error ? <ActionError data={stopData} /> : null}

      {stopError && JSON.stringify(stopError)}
      {data && (
        <div style={{ marginBottom: 20 }}>
          <div style={{ marginBottom: 20 }}>
            {!data.indicoEventType && (
              <StopTestForm
                groupId={data.id}
                onData={updateData}
                onError={updateError}
              />
            )}
            {data.indicoEventType === "STREAM" && (
              <StopStreamForm
                groupId={data.id}
                onData={updateData}
                onError={updateError}
              />
            )}
            {data.indicoEventType === "RECORDING" && (
              <StopRecordingForm
                groupId={data.id}
                onData={updateData}
                onError={updateError}
              />
            )}
            {data.indicoEventType === "STREAM_RECORDING" && (
              <StopAllForm
                groupId={data.id}
                onData={updateData}
                onError={updateError}
              />
            )}
          </div>
          <Grid columns={2}>
            {data.encoders.map((encoder: IEncoder) => (
              <Grid.Column key={encoder.id}>
                <EncoderDetails
                  encoder={encoder}
                  isTestRunning={data.isTestRunning}
                  indicoEventType={data.indicoEventType}
                />
              </Grid.Column>
            ))}
          </Grid>
        </div>
      )}
    </Segment>
  );
}
