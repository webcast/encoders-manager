import { Header, Message } from "semantic-ui-react";
import { VideoJS } from "../VideoJS/VideoJS";

type Props = {
  encoder: IEncoder;
  isTestRunning: boolean;
  indicoEventType: IIndicoEventType;
};

export default function EncoderDetails({
  encoder,
  isTestRunning,
  indicoEventType,
}: Props) {
  const buildVideoM3u8Src = () => {
    const videoSrc = `https://${encoder.originServer}/${
      isTestRunning || indicoEventType === "RECORDING"
        ? "test"
        : encoder.streamApplication
    }/${encoder.streamName}/playlist.m3u8`;
    return videoSrc;
  };

  const buildVideoRtmpSrc = () => {
    const videoSrc = `rtmp://${encoder.originServer}/${
      isTestRunning || indicoEventType === "RECORDING"
        ? "test"
        : encoder.streamApplication
    }/${encoder.streamName}`;
    return videoSrc;
  };

  const buildVideoRtspSrc = () => {
    const videoSrc = `rtsp://${encoder.originServer}/${
      isTestRunning || indicoEventType === "RECORDING"
        ? "test"
        : encoder.streamApplication
    }/${encoder.streamName}`;
    return videoSrc;
  };

  const videoJsOptions = {
    autoplay: true,
    controls: true,
    width: 480,
    height: "auto",
    sources: [
      {
        src: buildVideoM3u8Src(),
        type: "application/x-mpegURL",
      },
    ],
  };
  return (
    <div>
      <Header as="h3">{encoder.hostname}</Header>
      <Header as="h4">
        {encoder.streamRunning ? "Stream ON" : " Stream OFF"}{" "}
        {encoder.recordingRunning ? "Recording ON" : " Recording OFF"}
      </Header>
      <div style={{ marginBottom: 20 }}>
        <VideoJS options={videoJsOptions} />
      </div>
      <div style={{ marginBottom: 20 }}>
        <Message>
          Please notice that above&apos;s streams uses HLS and it might have
          some delay. If you need a real time one, please use{" "}
          <a href="https://www.videolan.org/vlc" title="VLC website">
            VLC
          </a>{" "}
          with the RTMP/RTSP links.
        </Message>
      </div>
      <div>
        <ul>
          <li>
            <strong>Stream name:</strong> {encoder.streamName}
          </li>
          <li>
            <strong>RTMP: </strong>{" "}
            <a href={buildVideoRtmpSrc()} title="RTMP stream">
              {buildVideoRtmpSrc()}
            </a>
          </li>
          <li>
            <strong>RTSP: </strong>{" "}
            <a href={buildVideoRtspSrc()} title="RTSP stream">
              {buildVideoRtspSrc()}
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
