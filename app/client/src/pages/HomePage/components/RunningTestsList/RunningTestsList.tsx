import { Card, Header } from "semantic-ui-react";
import RunningTestDetails from "./components/RunningTestDetails/RunningTestDetails";
import { useGroupEncodersRunning } from "hooks/encoder-groups/use-encoder-groups";

export default function RunningTestsList() {
  const { data: runningTests, error, isLoading } = useGroupEncodersRunning();

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {JSON.stringify(error)}</div>;
  }

  if (runningTests.length > 0) {
    return (
      <>
        <Header as="h2">Running tests</Header>
        <Card.Group>
          {runningTests.map((runningTest: any) => (
            <RunningTestDetails
              key={runningTest.id}
              runningTest={runningTest}
            />
          ))}
        </Card.Group>
      </>
    );
  }
  return null;
}
