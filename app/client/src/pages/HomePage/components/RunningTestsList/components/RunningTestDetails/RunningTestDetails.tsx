import { Link } from "react-router-dom";
import { Button, Card, Message } from "semantic-ui-react";
import ActionError from "components/ActionError/ActionError";
import { useStopTest } from "hooks/encoder-actions/use-encoder-actions";

type Props = { runningTest: IEncoderGroup };

export default function RunningTestDetails({ runningTest }: Props) {
  const {
    data: stoppedData,
    mutate: stopTest,
    isLoading: isStopping,
    isSuccess,
    isError: isErrorStopping,
    error: errorStopping,
  } = useStopTest();

  const handleStopTest = (encoderGroupId: number) => {
    stopTest({ id: encoderGroupId });
  };

  return (
    <Card key={runningTest.id} fluid>
      <Card.Content>
        <Card.Header>
          {runningTest.roomName} - {runningTest.name}
        </Card.Header>
        <Card.Meta>
          {" "}
          Encoders:{" "}
          {runningTest.encoders
            .map((encoder: IEncoder) => encoder.hostname)
            .join(", ")}
        </Card.Meta>
      </Card.Content>
      <Card.Content extra>
        <Button
          color="red"
          loading={isStopping}
          onClick={() => handleStopTest(runningTest.id)}
          icon="stop"
          content="Stop test"
          labelPosition="left"
        />{" "}
        <Button
          color="blue"
          icon="video"
          content="Monitor"
          labelPosition="left"
          as={Link}
          to={`/monitor/${runningTest.id}/`}
        />
      </Card.Content>
      <Card.Content extra>
        {isSuccess && stoppedData.error ? (
          <ActionError
            data={stoppedData}
            actionName="stop test"
            title="Error stopping the test stream"
          />
        ) : null}
        {isErrorStopping ? (
          <Message
            error
            header="Unable to stop the test stream"
            list={[
              `An error occurred:
              ${errorStopping instanceof Error ? errorStopping.message : ""}`,
              `Error stopping the test stream: ${JSON.stringify(
                errorStopping,
              )}`,
            ]}
          />
        ) : null}
      </Card.Content>
    </Card>
  );
}
