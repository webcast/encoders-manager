import { useForm } from "react-hook-form";
import { Button, Card, Form, Message } from "semantic-ui-react";
import ActionError from "components/ActionError/ActionError";
import { useStartTest } from "hooks/encoder-actions/use-encoder-actions";
import { useGroupEncodersNotRunning } from "hooks/encoder-groups/use-encoder-groups";

type FormData = {
  encoderGroupId: number;
};

export default function StartTestForm() {
  const {
    data: encoderGroupList,
    error,
    isLoading,
  } = useGroupEncodersNotRunning();
  const {
    mutate: startTest,
    data: startedData,
    isSuccess: isStarted,
    isLoading: isStarting,
    isError: isErrorStarting,
    error: errorStarting,
  } = useStartTest();

  const {
    // register,
    handleSubmit,
    setValue,
  } = useForm<FormData>();
  const onSubmit = handleSubmit((formData) => {
    console.log(formData);

    startTest({ id: formData.encoderGroupId });
    console.log(formData);
  });

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {JSON.stringify(error)}</div>;
  }

  return (
    <Card fluid>
      <Card.Content>
        <Form onSubmit={onSubmit}>
          <Form.Group inline>
            <Form.Field>
              <Form.Select
                loading={isLoading}
                placeholder="Select encoder group"
                label="Encoder group"
                name="encoderGroupId"
                search
                onChange={async (e, { name, value }) => {
                  setValue(name, value);
                }}
                lazyLoad
                options={
                  encoderGroupList &&
                  encoderGroupList
                    .map((value) => ({
                      key: value.id,
                      text: `${value.roomName} - ${value.name} (${value.encoders
                        .map((encoder: any) => encoder.hostname)
                        .join(", ")})`,
                      value: value.id,
                    }))
                    .sort((a, b) => {
                      // Use localeCompare to compare strings
                      return a.text.localeCompare(b.text);
                    })
                }
              />
            </Form.Field>
            <Form.Field>
              <Button type="submit" loading={isStarting}>
                Start test
              </Button>
            </Form.Field>
          </Form.Group>
        </Form>
      </Card.Content>
      {((isStarted && startedData.error) || isErrorStarting) && (
        <Card.Content extra>
          {isStarted && startedData.error ? (
            <ActionError
              data={startedData}
              actionName="start test"
              title="Error starting the test stream"
            />
          ) : null}
          {isErrorStarting ? (
            <Message
              error
              header="Unable to start the test stream"
              list={[
                `An error occurred:
              ${errorStarting instanceof Error ? errorStarting.message : ""}`,
                `Error Starting the test stream: ${JSON.stringify(
                  errorStarting,
                )}`,
              ]}
            />
          ) : null}
        </Card.Content>
      )}
    </Card>
  );
}
