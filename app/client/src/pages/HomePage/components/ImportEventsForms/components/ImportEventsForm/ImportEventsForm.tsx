import { Button } from "semantic-ui-react";
import { useFetchIndicoEvents } from "hooks/indico-events/use-indico-events";

export default function ImportEventsForm() {
  const { isLoading, mutate } = useFetchIndicoEvents();

  const onClick = () => {
    mutate();
  };

  return (
    <Button
      icon="cloud download"
      onClick={onClick}
      loading={isLoading}
      content="Fetch next events from Indico"
      labelPosition="left"
    />
  );
}
