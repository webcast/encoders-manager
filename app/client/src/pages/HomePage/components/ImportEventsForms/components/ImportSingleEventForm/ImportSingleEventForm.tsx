import { useState } from "react";
import { Button, Form, Modal } from "semantic-ui-react";
import { useFetchSingleIndicoEvent } from "hooks/indico-events/use-indico-events";

function ImportSingleEventForm() {
  const [open, setOpen] = useState(false);
  const { isError, error, isLoading, mutate } = useFetchSingleIndicoEvent();
  const [audience, setAudience] = useState<IIndicoEventAudience>("PUBLIC");
  const [eventType, setEventType] = useState<IIndicoEventType>("STREAM");
  const [indicoId, setIndicoId] = useState("");

  const audienceOptions = [
    { key: "public", value: "PUBLIC", text: "Public" },
    { key: "atlas", value: "ATLAS", text: "ATLAS" },
    { key: "cms", value: "CMS", text: "CMS" },
  ];

  const eventTypeOptions = [
    {
      key: "stream_recording",
      value: "STREAM_RECORDING",
      text: "Stream & Recording",
    },
    { key: "stream", value: "STREAM", text: "Stream" },
    { key: "recording", value: "RECORDING", text: "Recording" },
  ];

  const onModalAccept = () => {
    if (indicoId !== "") {
      console.log(indicoId, audience, eventType);
      mutate({ indicoId, audience, eventType });
      setIndicoId("");
      setAudience("PUBLIC");
      setEventType("STREAM");
    }
    if (!isError) {
      setOpen(false);
    }
  };

  return (
    <Modal
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button icon="download" content="Fetch an event" />}
    >
      <Modal.Header>Fetch an event from Indico</Modal.Header>
      <Modal.Content>
        {isError && <div>Error: {JSON.stringify(error)}</div>}
        <p>
          Set the time when the encoder will automatically stop. If you
          don&apos;t set a time, the encoder will run until you manually stop
          it.
        </p>
        <Form>
          <Form.Field>
            <Form.Input
              fluid
              label="Indico Id"
              placeholder="Indico Id"
              onChange={(e) => setIndicoId(e.target.value)}
            />
          </Form.Field>
          <Form.Select
            fluid
            label="Audience"
            options={audienceOptions}
            placeholder="Audience"
            onChange={(e, data) =>
              setAudience(data.value as IIndicoEventAudience)
            }
          />
          <Form.Select
            fluid
            label="Event Type"
            options={eventTypeOptions}
            placeholder="Event type"
            onChange={(e, data) => setEventType(data.value as IIndicoEventType)}
          />
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button
          basic
          loading={isLoading}
          disabled={isLoading || indicoId === ""}
          color="green"
          icon="clock"
          labelPosition="left"
          content="Import event"
          onClick={onModalAccept}
        />
        <Button
          basic
          color="red"
          icon="cancel"
          content="Cancel"
          onClick={() => setOpen(false)}
        />
      </Modal.Actions>
    </Modal>
  );
}

export default ImportSingleEventForm;
