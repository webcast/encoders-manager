import { Grid } from "semantic-ui-react";
import ImportEventsForm from "./components/ImportEventsForm/ImportEventsForm";
import ImportSingleEventForm from "./components/ImportSingleEventForm/ImportSingleEventForm";

export default function ImportEventsForms() {
  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <ImportEventsForm />
          <ImportSingleEventForm />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
