import { Button } from "semantic-ui-react";
import { useStartAll } from "hooks/encoder-actions/use-encoder-actions";

type Props = { groupId: number; indicoId: string };

export default function StartAllForm({ groupId, indicoId }: Props) {
  const { mutate, isLoading } = useStartAll();

  const onClick = () => {
    mutate({ id: groupId, indicoId });
  };

  return (
    <Button
      color="green"
      loading={isLoading}
      onClick={onClick}
      icon="play"
      labelPosition="left"
      content="Start all"
    />
  );
}
