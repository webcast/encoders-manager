import {
  Card,
  Grid,
  GridColumn,
  GridRow,
  Header,
  Icon,
  Label,
  Segment,
} from "semantic-ui-react";
import AutoStopTimerForm from "./components/EncoderGroupDetails/components/AutoStopTimerForm/AutoStopTimerForm";
import LockForm from "./components/EncoderGroupDetails/components/LockForm/LockForm";
import EncoderGroupDetails from "./components/EncoderGroupDetails/EncoderGroupDetails";
import EventDateText from "components/EventDateText/EventDateText";
import { useRoomGroupEncoders } from "hooks/encoder-groups/use-encoder-groups";

type Props = { event: any };

export default function EventDetails({ event }: Readonly<Props>) {
  const {
    data: encodersGroups,
    error,
    isLoading,
  } = useRoomGroupEncoders(event.roomId);

  const getCardColor = (isLocked: boolean, isRunning: boolean) => {
    if (isRunning) {
      return "#21ba45";
    }
    if (isLocked) {
      return "#fbbd08";
    }
    return "";
  };

  return (
    <Grid columns={16}>
      <GridRow>
        {(event.isLocked || event.isRunning) && (
          <GridColumn width={2}>
            <Segment
              style={{
                backgroundColor: getCardColor(event.isLocked, event.isRunning),
                height: "100%",
              }}
              placeholder
            >
              <Header icon>
                {event.isLocked && !event.isRunning && (
                  <>
                    <Icon name="lock" />
                    This event is locked.
                  </>
                )}
                {event.isRunning && (
                  <>
                    <Icon name="play" />
                    This event is running.
                  </>
                )}
              </Header>
            </Segment>
          </GridColumn>
        )}
        <GridColumn width={event.isLocked || event.isRunning ? 14 : 16}>
          <Card key={event.id} fluid>
            <Card.Content>
              <Card.Header>
                <Grid>
                  <Grid.Column floated="left" width={10}>
                    {event.title} (
                    <a
                      href={event.indicoUrl}
                      title={`Event "${event.title}" on Indico`}
                      target="_blank"
                      rel="noreferrer"
                    >
                      #{event.indicoId}
                    </a>
                    )
                  </Grid.Column>
                  <Grid.Column floated="right" width={5}>
                    <LockForm eventId={event.id} isLocked={event.isLocked} />
                  </Grid.Column>
                </Grid>
              </Card.Header>
              <Card.Meta style={{ marginTop: 20, marginBottom: 20 }}>
                <EventDateText
                  startDate={event.startDateTime}
                  endDate={event.endDateTime}
                />
                <AutoStopTimerForm
                  indicoId={event.indicoId}
                  eventTitle={event.title}
                  eventId={event.id}
                  autoStopDateTime={event.autoStopDateTime}
                />
              </Card.Meta>
              <Card.Meta style={{ marginTop: 10, marginBottom: 10 }}>
                <Label color="olive" size="medium">
                  {event.eventType}
                </Label>
                <Label
                  color={event.audience === "PUBLIC" ? "teal" : "orange"}
                  size="medium"
                >
                  <Icon name="bullhorn" />
                  Audience
                  <Label.Detail>{event.audience}</Label.Detail>
                </Label>
                <Label color="grey" size="medium">
                  <Icon name="location arrow" />
                  Room
                  <Label.Detail>{event.roomIndicoName}</Label.Detail>
                </Label>
                <span>
                  {" - "}
                  <a
                    href={`/admin/indicoevent/edit/?id=${event.id}`}
                    title={`Edit event ${event.title}`}
                  >
                    Edit
                  </a>
                </span>
              </Card.Meta>
              <Card.Description>
                {isLoading && <div>Loading...</div>}
                {error && <div>Error: {JSON.stringify(error)}</div>}
                {encodersGroups?.map((group: any) => (
                  <EncoderGroupDetails
                    key={group.id}
                    group={group}
                    eventType={event.eventType}
                    eventId={event.indicoId}
                    eventEncoderGroupId={event.encoderGroupId}
                    style={null}
                  />
                ))}
                {encodersGroups && encodersGroups.length === 0 && (
                  <div>This room has no encoders</div>
                )}
              </Card.Description>
            </Card.Content>
          </Card>
        </GridColumn>
      </GridRow>
    </Grid>
  );
}
