import { useState } from "react";
import { DateTimePicker, DateValue } from "@mantine/dates";
import dayjs from "dayjs";
import { Button, Modal } from "semantic-ui-react";
import { useUpdateAutoStop } from "hooks/indico-events/use-indico-events";

type Props = {
  indicoId: string;
  eventTitle: string;
  eventId: number;
  autoStopDateTime: string;
};

export default function AutoStopTimerForm({
  eventId,
  indicoId,
  eventTitle,
  autoStopDateTime,
}: Props) {
  const { isError, error, mutate, isLoading } = useUpdateAutoStop();
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState<DateValue | undefined>(
    autoStopDateTime ? new Date(autoStopDateTime) : new Date(),
  );

  const removeAutoStop = () => {
    mutate({ id: eventId, autoStopDatetime: null });
  };

  const onModalAccept = () => {
    if (value !== null) {
      const isoDate = dayjs(value).format("YYYY-MM-DD HH:mm");
      console.log(isoDate);
      mutate({ id: eventId, autoStopDatetime: isoDate });
    }
    if (!isError) {
      setOpen(false);
    }
  };

  return (
    <>
      {autoStopDateTime && (
        <div style={{ float: "right" }}>
          Auto Stop Time: {dayjs(autoStopDateTime).format("DD/MM/YYYY HH:mm")}{" "}
          <Button basic floated="right" icon="x" onClick={removeAutoStop} />
        </div>
      )}
      {!autoStopDateTime && (
        <Modal
          onOpen={() => setOpen(true)}
          open={open}
          trigger={
            <Button
              basic
              floated="right"
              icon="clock"
              content="Set auto stop"
            />
          }
        >
          <Modal.Header>Auto Stop Timer</Modal.Header>
          <Modal.Content>
            <p>
              Event: <strong>{eventTitle}</strong>
            </p>
            <p>
              Indico ID: <strong>{indicoId}</strong>
            </p>
            <p>
              Current Auto Stop Time:{" "}
              <strong>{autoStopDateTime || "Not set"}</strong>
            </p>
            {isError && <div>Error: {JSON.stringify(error)}</div>}
            <p>
              Set the time when the encoder will automatically stop. If you
              don&apos;t set a time, the encoder will run until you manually
              stop it.
            </p>
            <DateTimePicker
              valueFormat="DD/MM/YYYY HH:mm"
              placeholder="Pick date and time"
              maw={400}
              mx="auto"
              onChange={setValue}
              value={value}
            />
          </Modal.Content>
          <Modal.Actions>
            <Button
              basic
              loading={isLoading}
              color="green"
              icon="clock"
              labelPosition="left"
              content="Set Auto Stop"
              onClick={onModalAccept}
            />
            <Button
              basic
              color="red"
              icon="cancel"
              content="Cancel"
              onClick={() => setOpen(false)}
            />
          </Modal.Actions>
        </Modal>
      )}
    </>
  );
}
