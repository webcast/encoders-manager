import { useState } from "react";
import {
  Button,
  Card,
  Header,
  Icon,
  Label,
  List,
  Message,
  Popup,
  PopupContent,
  PopupHeader,
} from "semantic-ui-react";
import GetStatusForm from "./components/GetStatusForm/GetStatusForm";
import RecodingButtons from "./components/RecordingButtons/RecordingButtons";
import StreamButtons from "./components/StreamButtons/StreamButtons";
import StreamRecodingButtons from "./components/StreamRecordingButtons/StreamRecodingButtons";
import ActionError from "components/ActionError/ActionError";
import { IActionResponse } from "types/action-response";
import { IEncoderGroup } from "types/encoder";

type Props = {
  group: IEncoderGroup;
  eventType: string;
  eventId: string;
  eventEncoderGroupId: number | null;
  style: any;
};

export default function EncoderGroupDetails({
  group,
  eventType,
  eventId,
  eventEncoderGroupId,
  style,
}: Readonly<Props>) {
  const [data, setData] = useState<IActionResponse | null>(null);
  const [error, setError] = useState(null);

  const updateData = (newData: any) => {
    setData(newData);
  };

  const updateError = (newError: any) => {
    setError(newError);
  };

  return (
    <Card key={group.id} fluid style={style}>
      <Card.Content>
        <Card.Header>
          {group.name} - (
          <a
            href={`/admin/encodergroup/edit/?id=${group.id}`}
            title="Edit the encoder group"
          >
            {group.id}
          </a>
          )
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <div>
          {group.encoders && (
            <>
              <Header as="h4">Encoders</Header>
              <Card.Content extra>
                {group.startAsComposite && (
                  <Popup
                    header="Help"
                    trigger={
                      <Label color={group.startAsComposite ? "green" : "grey"}>
                        <Icon name="mail" />
                        Composite layout
                      </Label>
                    }
                  >
                    <PopupHeader>About</PopupHeader>
                    <PopupContent>
                      This configuration only applies to Epiphan Pearl 2
                      encoders.
                      <ul>
                        <li>
                          <strong>Normal layout:</strong> Camera and slides in
                          separate streams.
                        </li>
                        <li>
                          <strong>Composite layout:</strong> Only one stream
                          with both inputs.
                        </li>
                      </ul>
                      Edit the encoder group to change this setting.
                    </PopupContent>
                  </Popup>
                )}
              </Card.Content>

              <List horizontal>
                {group.encoders?.map((encoder: any) => (
                  <List.Item key={encoder.id}>
                    <List.Icon
                      name="video camera"
                      size="large"
                      verticalAlign="middle"
                      className="imaged"
                      color={encoder.streamRunning ? "green" : "red"}
                      style={{ marginRight: "0.5em" }}
                      title={`Streaming is ${
                        encoder.streamRunning ? "running" : "not running"
                      }`}
                    />
                    <List.Icon
                      name="hdd"
                      size="large"
                      verticalAlign="middle"
                      className="imaged"
                      color={encoder.recordingRunning ? "green" : "red"}
                      title={`Recording is ${
                        encoder.recordingRunning ? "running" : "not running"
                      }`}
                    />
                    <List.Content>
                      <List.Header>{encoder.hostname}</List.Header>
                      {encoder.signalType} - {encoder.encoderType}
                    </List.Content>
                  </List.Item>
                ))}
                <List.Item key={`refresh-button-${group.id}`}>
                  <GetStatusForm
                    groupId={group.id}
                    onData={updateData}
                    onError={updateError}
                  />
                </List.Item>
              </List>
            </>
          )}
        </div>
      </Card.Content>
      {(eventEncoderGroupId == null || eventEncoderGroupId === group.id) && (
        <Card.Content extra>
          {eventType === "STREAM_RECORDING" && (
            <StreamRecodingButtons
              group={group}
              eventId={eventId}
              onData={updateData}
              onError={updateError}
            />
          )}
          {eventType === "STREAM" && (
            <StreamButtons
              group={group}
              eventId={eventId}
              onData={updateData}
              onError={updateError}
            />
          )}
          {eventType === "RECORDING" && (
            <RecodingButtons
              group={group}
              eventId={eventId}
              onData={updateData}
              onError={updateError}
            />
          )}
          {(group.indicoEventId && group.indicoEventId !== eventId) ||
            (eventEncoderGroupId !== null &&
              eventEncoderGroupId !== group.id) ||
            (group.isTestRunning && (
              <Button disabled color="grey">
                Actions disabled
              </Button>
            ))}
        </Card.Content>
      )}
      {group.encoders?.some(
        (encoder: any) =>
          (encoder.recordingRunning || encoder.streamRunning === true) &&
          group.indicoEventId !== eventId,
      ) && (
        <Card.Content extra>
          <Message color="orange">
            <Icon name="warning sign" /> These encoders are running on other
            event
          </Message>
        </Card.Content>
      )}
      {(data?.error ||
        (group.indicoEventId && group.indicoEventId !== eventId) ||
        (eventEncoderGroupId !== null && eventEncoderGroupId !== group.id) ||
        group.isTestRunning) && (
        <Card.Content extra>
          {data?.error ? <ActionError data={data} /> : null}
          {error && JSON.stringify(error)}

          {group.indicoEventId !== null && group.indicoEventId !== eventId && (
            <div>
              <Icon name="warning sign" /> This encoder is being used in another
              event.
            </div>
          )}

          {eventEncoderGroupId !== null && eventEncoderGroupId !== group.id && (
            <div>
              <Icon name="warning sign" /> This event has another group of
              encoders active.
            </div>
          )}
          {group.isTestRunning && <div>Test is running for this encoder</div>}
        </Card.Content>
      )}
    </Card>
  );
}
