import { useEffect } from "react";
import { Button } from "semantic-ui-react";
import { useGetStatus } from "hooks/encoder-actions/use-encoder-actions";

type Props = {
  groupId: number;
  onData: (data: any) => void;
  onError: (error: any) => void;
};

export default function GetStatusForm({ groupId, onData, onError }: Props) {
  const { mutate, data, error, isError, isSuccess, isLoading } = useGetStatus();

  const onClick = () => {
    mutate({ id: groupId });
  };

  useEffect(() => {
    if (isSuccess) {
      onData(data);
    }
  }, [data, isError, isSuccess, onData]);

  useEffect(() => {
    if (isError) {
      onError(error);
    }
  }, [error, isError, isSuccess, onError]);

  return (
    <Button
      basic
      color="grey"
      loading={isLoading}
      onClick={onClick}
      icon="refresh"
    />
  );
}
