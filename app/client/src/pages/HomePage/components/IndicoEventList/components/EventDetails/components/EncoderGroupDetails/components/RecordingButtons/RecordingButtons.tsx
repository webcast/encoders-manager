import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import StopRecordingForm from "components/StopRecordingForm/StopRecordingForm";
import StartRecordingForm from "pages/HomePage/components/IndicoEventList/components/EventDetails/components/EncoderGroupDetails/components/StartRecordingForm/StartRecordingForm";
import { IEncoderGroup } from "types/encoder";

type Props = {
  group: IEncoderGroup;
  eventId: string;
  onData: (data: any) => void;
  onError: (error: any) => void;
};

function StartRecording({ group, eventId, onData, onError }: Readonly<Props>) {
  const canStartRecording = (groupToStart: IEncoderGroup) =>
    !groupToStart.isRunning && !groupToStart.isTestRunning;

  if (canStartRecording(group)) {
    return (
      <StartRecordingForm
        groupId={group.id}
        indicoId={eventId}
        onData={onData}
        onError={onError}
      />
    );
  }
  return null;
}

function StopRecording({ group, eventId, onData, onError }: Readonly<Props>) {
  const canStopRecording = (
    groupToStop: IEncoderGroup,
    currentEventId: string,
  ) => groupToStop.isRunning && groupToStop.indicoEventId === currentEventId;

  if (canStopRecording(group, eventId)) {
    return (
      <>
        <StopRecordingForm
          groupId={group.id}
          onData={onData}
          onError={onError}
        />
        <Button
          color="blue"
          icon="video"
          content="Monitor"
          as={Link}
          to={`/monitor/${group.id}/`}
        />
      </>
    );
  }
  return null;
}

export default function RecordingButtons({
  group,
  eventId,
  onData,
  onError,
}: Readonly<Props>) {
  return (
    <>
      <StartRecording
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
      <StopRecording
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
    </>
  );
}
