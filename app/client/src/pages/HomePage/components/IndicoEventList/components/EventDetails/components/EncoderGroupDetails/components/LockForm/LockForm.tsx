import React from "react";
import { Button } from "semantic-ui-react";
import { useUpdateLock } from "hooks/indico-events/use-indico-events";

type Props = { eventId: number; isLocked: boolean };

export default function LockForm({ eventId, isLocked }: Props) {
  const { mutate, isLoading, isError, error } = useUpdateLock();

  const handleLock = () => {
    mutate({ id: eventId, isLocked: !isLocked });
  };

  if (isError) {
    return <div>Error: {JSON.stringify(error)}</div>;
  }

  return (
    <Button
      floated="right"
      basic
      loading={isLoading}
      onClick={handleLock}
      color={isLocked ? "red" : "green"}
      icon="lock"
      content={isLocked ? "Locked" : "Unlocked"}
    />
  );
}
