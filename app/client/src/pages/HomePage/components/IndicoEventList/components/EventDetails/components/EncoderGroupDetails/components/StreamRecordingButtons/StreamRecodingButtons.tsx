import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import StopAllForm from "components/StopAllForm/StopAllForm";
import StopRecordingForm from "components/StopRecordingForm/StopRecordingForm";
import StopStreamForm from "components/StopStreamForm/StopStreamForm";
import RestartRecordingForm from "pages/HomePage/components/IndicoEventList/components/EventDetails/components/EncoderGroupDetails/components/RestartRecordingForm/RestartRecordingForm";
import RestartStreamForm from "pages/HomePage/components/IndicoEventList/components/EventDetails/components/EncoderGroupDetails/components/RestartStreamForm/RestartStreamForm";
import StartAllForm from "pages/HomePage/components/IndicoEventList/components/EventDetails/components/EncoderGroupDetails/components/StartAllForm/StartAllForm";
import { IEncoderGroup } from "types/encoder";

type Props = {
  group: IEncoderGroup;
  eventId: string;
  onData: (data: any) => void;
  onError: (error: any) => void;
};

function StartAll({
  group,
  eventId,
}: Readonly<{
  group: IEncoderGroup;
  eventId: string;
}>) {
  if (group.isRunning || group.isTestRunning) {
    return null;
  }
  return <StartAllForm groupId={group.id} indicoId={eventId} />;
}

function StopAll({ group, eventId, onData, onError }: Readonly<Props>) {
  const shouldShowStopAll = () =>
    (group.isRunning || group.isTestRunning) &&
    group.isRecordingRunning &&
    group.isStreamRunning &&
    group.indicoEventId === eventId;

  if (shouldShowStopAll()) {
    return <StopAllForm groupId={group.id} onData={onData} onError={onError} />;
  }
  return null;
}

function StopRecording({ group, eventId, onData, onError }: Readonly<Props>) {
  const shouldShowStopRecordingForm = () =>
    group.isRecordingRunning && group.indicoEventId === eventId;

  if (shouldShowStopRecordingForm()) {
    return (
      <StopRecordingForm
        groupId={group.id}
        onData={onData}
        onError={onError}
        basic
      />
    );
  }
  return null;
}

function StopStream({ group, eventId, onData, onError }: Readonly<Props>) {
  const shouldShowStopStreamForm = () =>
    group.isStreamRunning && group.indicoEventId === eventId;

  if (shouldShowStopStreamForm()) {
    return (
      <StopStreamForm
        groupId={group.id}
        onData={onData}
        onError={onError}
        basic
      />
    );
  }
  return null;
}

function RestartRecording({
  group,
  eventId,
  onData,
  onError,
}: Readonly<Props>) {
  const shouldShowRestartRecordingForm = () =>
    group.isRunning &&
    !group.isRecordingRunning &&
    group.indicoEventId === eventId;

  if (shouldShowRestartRecordingForm()) {
    return (
      <RestartRecordingForm
        indicoId={eventId}
        groupId={group.id}
        onData={onData}
        onError={onError}
      />
    );
  }
  return null;
}

function RestartStream({ group, eventId, onData, onError }: Readonly<Props>) {
  const shouldShowRestartStreamForm = () =>
    group.isRunning &&
    !group.isStreamRunning &&
    group.indicoEventId === eventId;

  if (shouldShowRestartStreamForm()) {
    return (
      <RestartStreamForm
        indicoId={eventId}
        groupId={group.id}
        onData={onData}
        onError={onError}
      />
    );
  }
  return null;
}

export default function StreamRecodingButtons({
  group,
  eventId,
  onData,
  onError,
}: Readonly<Props>) {
  return (
    <>
      <StartAll group={group} eventId={eventId} />
      <StopAll
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
      <StopRecording
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
      <StopStream
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
      <RestartRecording
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
      <RestartStream
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
      {group.isRunning && (
        <Button
          color="blue"
          icon="video"
          content="Monitor"
          as={Link}
          to={`/monitor/${group.id}/`}
        />
      )}
    </>
  );
}
