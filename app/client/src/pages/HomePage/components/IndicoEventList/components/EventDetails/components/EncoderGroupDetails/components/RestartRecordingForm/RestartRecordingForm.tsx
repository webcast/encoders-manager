import { useEffect } from "react";
import { Button } from "semantic-ui-react";
import { useRestartRecording } from "hooks/encoder-actions/use-encoder-actions";

type Props = {
  groupId: number;
  indicoId: string;
  onData: (data: any) => void;
  onError: (error: any) => void;
};

export default function RestartRecordingForm({
  groupId,
  indicoId,
  onData,
  onError,
}: Props) {
  const { mutate, data, error, isError, isSuccess, isLoading } =
    useRestartRecording();

  const onClick = () => {
    mutate({ id: groupId, indicoId });
  };

  useEffect(() => {
    if (isSuccess) {
      onData(data);
    }
  }, [data, isError, isSuccess, onData]);

  useEffect(() => {
    if (isError) {
      onError(error);
    }
  }, [error, isError, isSuccess, onError]);

  return (
    <Button
      color="blue"
      loading={isLoading}
      onClick={onClick}
      icon="play"
      labelPosition="left"
      content="Restart Recording"
    />
  );
}
