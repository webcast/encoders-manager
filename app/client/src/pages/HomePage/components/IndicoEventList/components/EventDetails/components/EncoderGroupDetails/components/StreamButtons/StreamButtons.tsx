import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import StartStreamForm from "../StartStreamForm/StartStreamForm";
import StopStreamForm from "components/StopStreamForm/StopStreamForm";
import { IEncoderGroup } from "types/encoder";

type Props = {
  group: IEncoderGroup;
  eventId: string;
  onData: (data: any) => void;
  onError: (error: any) => void;
};

function StartStreamButton({
  group,
  eventId,
  onData,
  onError,
}: Readonly<Props>) {
  function shouldShowStartStreamForm() {
    return !group.isRunning && !group.isTestRunning;
  }

  if (shouldShowStartStreamForm()) {
    return (
      <StartStreamForm
        groupId={group.id}
        indicoId={eventId}
        onData={onData}
        onError={onError}
      />
    );
  }
  return null;
}

function StopStreamButton({
  group,
  eventId,
  onData,
  onError,
}: Readonly<Props>) {
  function shouldShowStopStreamForm() {
    return (
      group.isRunning && group.indicoEventId && group.indicoEventId === eventId
    );
  }

  if (shouldShowStopStreamForm()) {
    return (
      <>
        <StopStreamForm groupId={group.id} onData={onData} onError={onError} />
        <Button
          color="blue"
          icon="video"
          content="Monitor"
          as={Link}
          to={`/monitor/${group.id}/`}
        />
      </>
    );
  }
  return null;
}

export default function StreamButtons({
  group,
  eventId,
  onData,
  onError,
}: Readonly<Props>) {
  return (
    <>
      <StartStreamButton
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
      <StopStreamButton
        group={group}
        eventId={eventId}
        onData={onData}
        onError={onError}
      />
    </>
  );
}
