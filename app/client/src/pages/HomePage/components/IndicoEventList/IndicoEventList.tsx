import { Header } from "semantic-ui-react";
import EventDetails from "./components/EventDetails/EventDetails";
import { useIndicoEvents } from "hooks/indico-events/use-indico-events";

export default function IndicoEventList() {
  const { data, error, isLoading } = useIndicoEvents();

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {JSON.stringify(error)}</div>;
  }

  return (
    <>
      <Header as="h2">Next events</Header>

      {data?.map((event: any) => (
        <EventDetails key={event.id} event={event} />
      ))}
    </>
  );
}
