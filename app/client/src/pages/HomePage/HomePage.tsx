import ImportEventsForms from "./components/ImportEventsForms/ImportEventsForms";
import IndicoEventList from "./components/IndicoEventList/IndicoEventList";
import RunningTestsList from "./components/RunningTestsList/RunningTestsList";
import StartTestForm from "./components/StartTestForm/StartTestForm";

export default function HomePage() {
  return (
    <div>
      <StartTestForm />
      <RunningTestsList />
      <ImportEventsForms />
      <IndicoEventList />
    </div>
  );
}
