import { useState } from "react";
import { Button } from "semantic-ui-react";
import { useCheckAllEncoders } from "hooks/encoders/use-encoders";

export default function ButtonCheckEncodersStatus() {
  const { mutate, isLoading } = useCheckAllEncoders();
  const [disabled, setDisabled] = useState(false);

  const handleClick = () => {
    mutate();
    setDisabled(true);
  };

  return (
    <Button loading={isLoading} onClick={handleClick} disabled={disabled}>
      Refresh
    </Button>
  );
}
