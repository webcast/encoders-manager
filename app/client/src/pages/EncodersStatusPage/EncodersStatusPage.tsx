import dayjs from "dayjs";
import { Button, Grid, Header, Label, Segment, Table } from "semantic-ui-react";
import ButtonCheckEncodersStatus from "./components/ButtonEncodersStatus/ButtonCheckEncodersStatus";
import { useEncoders, useSendEmail } from "hooks/encoders/use-encoders";

export default function EncodersStatusPage() {
  const { data, isLoading } = useEncoders();
  const { mutate: sendEmail, isLoading: sendingEmail } = useSendEmail();

  return (
    <Segment>
      <Grid>
        <Grid.Column floated="left" width={5}>
          <Header as="h1">Encoders Status</Header>
        </Grid.Column>
        <Grid.Column floated="right" width={2}>
          <ButtonCheckEncodersStatus />
        </Grid.Column>
      </Grid>

      {isLoading && <div>Loading encoders...</div>}

      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Hostname</Table.HeaderCell>
            <Table.HeaderCell>Type/Signal</Table.HeaderCell>
            <Table.HeaderCell>Reachable</Table.HeaderCell>
            <Table.HeaderCell>Last heartbeat</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data &&
            data.map((encoder: IEncoder) => (
              <Table.Row key={encoder.hostname}>
                <Table.Cell>{encoder.hostname}</Table.Cell>
                <Table.Cell>
                  {encoder.encoderType} / {encoder.signalType}
                </Table.Cell>
                <Table.Cell>
                  <Label
                    color={encoder.lastHeartbeat.isReachable ? "green" : "red"}
                  >
                    {encoder.lastHeartbeat.message || "Unknown"}
                  </Label>
                </Table.Cell>
                <Table.Cell>
                  {encoder.lastHeartbeat.createdOn
                    ? dayjs(encoder.lastHeartbeat.createdOn).format(
                        "DD-MM-YYYY HH:mm",
                      )
                    : "-"}
                </Table.Cell>
                <Table.Cell>
                  <Button
                    disabled={encoder.lastHeartbeat.isReachable}
                    onClick={() => sendEmail({ id: encoder.id })}
                    loading={sendingEmail}
                  >
                    Notify owner
                  </Button>
                </Table.Cell>
              </Table.Row>
            ))}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="5" />
          </Table.Row>
        </Table.Footer>
      </Table>
    </Segment>
  );
}
