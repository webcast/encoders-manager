import React from "react";
import { Button, Container, Header, Icon } from "semantic-ui-react";
import AuthService from "services/auth-service";

export default function LoginPage() {
  return (
    <Container className="App">
      <Header as="h2">
        <Icon name="settings" />
        <Header.Content>
          Encoders Manager
          <Header.Subheader>Manage the encoders in the rooms</Header.Subheader>
        </Header.Content>
      </Header>
      <Button
        onClick={() =>
          AuthService.doLogin({
            redirectUri: encodeURI(window.location.origin),
          })
        }
      >
        Login
      </Button>
    </Container>
  );
}
