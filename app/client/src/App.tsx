import { NavLink, Outlet } from "react-router-dom";
import "./App.css";
import { ToastContainer } from "react-toastify";
import { Container, Menu, Header, Icon, Grid } from "semantic-ui-react";
import DevBanner from "components/DevBanner";
import TopMenu from "components/TopMenu/TopMenu";
import { PACKAGE_VERSION } from "version";

function App() {
  return (
    <Container className="App">
      <TopMenu />
      <DevBanner />
      <Header as="h2">
        <Icon name="settings" />
        <Header.Content>
          Encoders Manager
          <Header.Subheader>Manage the encoders in CERN Rooms</Header.Subheader>
        </Header.Content>
      </Header>
      <Menu>
        <Menu.Item as={NavLink} to="/" name="home">
          Home
        </Menu.Item>
        <Menu.Item
          as={NavLink}
          to="status"
          name="status"
          title="Status of the encoders"
        >
          Encoders statuses
        </Menu.Item>
        <Menu.Item as="a" href="/admin/" name="admin" title="Admin dashboard">
          Admin
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item
            as="a"
            href="/api/v1/"
            target="_blank"
            title="API Swagger Docs"
          >
            API
          </Menu.Item>
          {/* <Menu.Item
            as="a"
            href="https://dev-documentation.web.cern.ch/opsdoc/vidyo/ravem2/"
            target="_blank"
            title="Dev Documentation"
          >
            <Icon name="book" />
          </Menu.Item>
          <Menu.Item
            as="a"
            href="https://monit-timber-videoconference.cern.ch/kibana/app/kibana#/dashboards"
            target="_blank"
            title="App Monitoring"
          >
            <Icon name="chart area" />
          </Menu.Item> */}
        </Menu.Menu>
      </Menu>

      {/* <Grid>
        <Grid.Row>
          <Grid.Column>
            <Breadcrumbs routes={routes} />
          </Grid.Column>
        </Grid.Row>
      </Grid> */}
      <Outlet />

      <Grid centered columns={1}>
        <Grid.Column>
          2023 - {new Date().getFullYear()} © CERN - Webcast & Recording Service
          - Prod: {JSON.stringify(import.meta.env.PROD)} - Dev:{" "}
          {JSON.stringify(import.meta.env.DEV)} - Version: {PACKAGE_VERSION}
        </Grid.Column>
      </Grid>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
      />
    </Container>
  );
}

export default App;
