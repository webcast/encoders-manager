interface ResultItem {
  status: string;
  message: string;
  is_reachable: boolean;
  stream_name: string;
  recording_path: string | null;
  signal_type: string;
  room_indico_name: string;
}

interface Result {
  [key: string]: ResultItem;
}

export interface IActionResponse {
  error: boolean;
  rollback_error: boolean;
  result: Result;
  rollback: Result;
  message?: string;
}
