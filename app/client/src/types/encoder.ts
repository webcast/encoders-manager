import { IIndicoEventType } from "./indico-event";

interface IHeartbeat {
  message: string;
  isReachable: boolean;
  createdOn: string;
}

interface IEncoder {
  id: number;
  hostname: string;
  recordingRunning: boolean;
  streamRunning: boolean;
  encoderType: string;
  signalType: string;
  encoderGroupId: number;
  streamStartDatetime: string;
  recordingStartDatetime: string;
  streamServer: string;
  originServer: string;
  streamApplication: string;
  streamName: string;
  edgeServer: string;
  lastHeartbeat: IHeartbeat;
}

export interface IEncoderGroup {
  id: number;
  name: string;
  isTestRunning: boolean;
  isRunning: boolean;
  recordingPath: string;
  roomId: number;
  indicoEventId: string;
  indicoEventTitle: string;
  indicoEventType: IIndicoEventType;
  isStreamRunning: boolean;
  isRecordingRunning: boolean;
  startAsComposite: boolean;
  roomName: string;
  encoders: IEncoder[];
}
