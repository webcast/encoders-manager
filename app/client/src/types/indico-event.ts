export type IIndicoEventType = "RECORDING" | "STREAM" | "STREAM_RECORDING";

type IIndicoEventAudience = "PUBLIC" | "ATLAS" | "CMS";

export interface IIndicoEvent {
  id: number;
  title: string;
  audience: IIndicoEventAudience;
  indicoId: string;
  indicoUrl: string;
  startDatetime: string;
  endDatetime: string;
  isLocked: boolean;
  isStreamed: boolean;
  isRecorded: boolean;
  roomIndicoName: string;
  roomId: number;
  autoStopDatetime: string;
  eventType: IIndicoEventType;
  encoderGroupId: number;
}
