interface IAPI {
  ENDPOINT: string;
}

interface IOIDC {
  CLIENT_ID: string;
  CLIENT_URL: string;
  ADMIN_ROLE: string;
}

export interface IConfig {
  API: IAPI;
  IS_DEV_INSTALL: boolean;
  OIDC: IOIDC;
  SENTRY_DSN: string;
  SENTRY_ENV: string;
}
