from typing import Any, Dict

from celery.schedules import crontab

# Whether or not to use the werkzeug proxy for the requests. Required on Openshift.
# Values: boolean (Default: False)
USE_PROXY = False
# Secret key for the application. Used for the CSRF protection.
# Values: string
SECRET_KEY = "secret"
#
#
IS_DEV = False
# Role name for the admin users
# Values: string (Default: "admins")
ADMIN_ROLE = "admins"
# Indico base URL.
# Values: string (Default: "https://indico.cern.ch")
INDICO_ENDPOINT = "https://indico.cern.ch"
# Indico access token. Used to access the Indico API. Generated on the profile page.
# Values: string
INDICO_ACCESS_TOKEN = "<TOKEN>"
# Timezone of the application
# Values: string (Default: "Europe/Zurich")
APPLICATION_TIMEZONE = "Europe/Zurich"
# Log level for the application
# Values: string (Default: "DEBUG")
LOG_LEVEL = "DEBUG"
# Whether or not to enable the log file
# Values: boolean (Default: False)
LOG_FILE_ENABLED = False
# Type of the database to use
# Values: mysql|postgresql|sqlite
# Default: sqlite
DB_ENGINE = "sqlite"
# Username of the database
# Values: string (Default: "root")
# Default: root
DB_USER = "root"
# Password of the database
# Values: string (Default: "root")
# Default: root
DB_PASS = "root"
# Hostname of the database
# Values: string (Default: "localhost")
# Default: localhost
DB_SERVICE_NAME = "127.0.0.1"
# Port of the database
# Values: string (Default: "3306")
# Default: 3306
DB_PORT = "3306"
# Name of the database
# Values: string (Default: "encoders")
# Default: encoders
DB_NAME = "encoders"
# Whether to enable the cache or not
# Values: boolean (Default: True)
CACHE_ENABLE = True
# Type of the cache to use
# Values:
# https://flask-caching.readthedocs.io/en/latest/index.html#configuring-flask-caching
CACHE_TYPE = "SimpleCache"  # Flask-Caching related configs
# Default timeout for the cache
# Values: integer (Default: 300)
CACHE_DEFAULT_TIMEOUT = 300
# Username for the Epiphan encoders
# Values: string
EPIPHAN_USERNAME = "<USERNAME>"
EPIPHAN_PASSWORD = "<PASSWORD>"
# Name of the test stream app used to test the encoders
# Values: string (Default: "test")
TEST_STREAM_APP = "test"
# Default recording path for the Matrox recordings if no recording path is set
# Values: string (Default: r"\\<SERVER>.cern.ch\RECORDINGS\Test\TEST-ROOM")
DEFAULT_RECORDING_PATH = r"\\<SERVER>.cern.ch\RECORDINGS\Test\TEST-ROOM"
# Recording path for the Epiphan encoders
# Values: string (Default: r"\\<SERVER>.cern.ch\RECORDINGS\Epiphan")
EPIPHAN_NANO_RECORDING_PATH = r"\\cernmedia33.cern.ch\RECORDINGS\Epiphan"
# Username for the Monarch encoders
# Values: string
MONARCH_USERNAME = "<USERNAME>"
# Password for the Monarch encoders
# Values: string
MONARCH_PASSWORD = "<PASSWORD>"
# Dictionary with the mapping between the origin and the streamlock URL for
# the Wowza servers
#
STREAMLOCK_ORIGINS = {
    "wowza1.cern.ch": "https://aaaa.streamlock.net",
    "wowza2.cern.ch": "https://bbb.streamlock.net",
    "wowza3.cern.ch": "https://cccc.streamlock.net",
}
# Wowza edge URL to be used for the Wowza encoders
#
WOWZA_EDGE_URL = "https://wowzaqaedge.cern.ch"
# Ravem base URL. Used to notify RAVEM about the new recordings or streams (onair signs)
# Values: string (Default: "https://xxx.cern.ch")
RAVEM_URL = "<TODO>"
# Ravem API key for the API. This API key is generated on the Ravem website.
# Values: string
RAVEM_API_KEY = "<TODO>"
# Webcast website URL. Used to notify the webcast website about the new streams
# Values: string (Default: "https://xxx.cern.ch")
WEBCAST_WEBSITE_URL = "<TODO>"
# Webcast website API key. This API key is generated on the webcast website admin.
# Values: string
WEBCAST_WEBSITE_API_KEY = "<TODO>"
# Webcast website secret key. This secret key is generated on the webcast website admin.
# Values: string
WEBCAST_WEBSITE_SECRET_KEY = "<TODO>"
# CES base URL. Used to notify CES about the new recordings
# Values: string (Default: "https://xxx.cern.ch")
CES_URL = "<TODO>"
# CES API key for the API. This API key is generated on the CES website admin.
# Values: string
CES_API_KEY = "<TODO>"
# Hostname for the redis instance
# Values: string (Default: "redis")
CACHE_REDIS_HOST = "localhost"
# Port to connect to the redis instance
# Values: int (Default: 6379)
CACHE_REDIS_PORT = "6379"
# Password to connect to the redis instance
# Values: string (Default: "redis")
CACHE_REDIS_PASSWORD = ""
# Redis database identifier
# Values: string (Default: "0")
CACHE_REDIS_DB = "0"
# The scheduled tasks configuration for Celery
# Default: A dictionary with the following keys:
# "task_name": {"task": "task_module", "schedule": "crontab(minute=0, hour=0)"}
CELERY_BEAT_SCHEDULE: Dict[Any, Any] = {
    "dummy_task": {
        "task": "app.tasks.example.dummy_task",
        "schedule": crontab(minute="*/5"),
    }
}
# The URL to the Celery's broker (redis)
# Default: 'redis://redis:6379/0'
CELERY_BROKER_URL = f"redis://{CACHE_REDIS_HOST}:6379/0"
# The URL to the Celery's backend (redis or MySQL)
# Default: 'redis://redis:6379/0'
CELERY_RESULTS_BACKEND = f"redis://{CACHE_REDIS_HOST}:6379/0"
# Email address from which the notifications are sent
# Value: string
MAIL_FROM = ""
# Email address to which the notifications are sent
# Value: string
MAIL_TO = ""
# Number of minutes before an event when the notification emails are sent
# Value: integer (Default: 15)
REMINDER_EVENT_MINUTES = 15
# Hostname of the server
# Value: string (Default: "localhost")
INSTANCE_HOSTNAME = "encoders-test.web.cern.ch"
# Whether or not to enable the Sentry integration
# Value: boolean (Default: False)
ENABLE_SENTRY = False
# Sentry DSN
# Value: string
SENTRY_DSN = ""
# Sentry environment
# Value: string (Default: "dev")
SENTRY_ENVIRONMENT = "dev"
#
#
LOGIN_DISABLED = False
# OIDC configuration for the client app
#
oidc_realm = "https://auth.cern.ch/auth/realms/cern"
OIDC_CONFIG_CLIENT = {
    # OIDC configuration
    "OIDC_CLIENT_ID": "<OIDC_CLIENT_ID_CLIENT>",
    "OIDC_JWKS_URL": oidc_realm + "/protocol/openid-connect/certs",
    "OIDC_ISSUER": oidc_realm,
    "OIDC_LOGOUT_URL": oidc_realm + "/protocol/openid-connect/logout",
}
# OIDC configuration for the backend app
#
OIDC_CONFIG_BACKEND = {
    "OIDC_CLIENT_ID": "<OIDC_CLIENT_ID_BACKEND>",
    "OIDC_CLIENT_SECRET": "<OIDC_CLIENT_SECRET>",
    "OIDC_JWKS_URL": oidc_realm + "/protocol/openid-connect/certs",
    "OIDC_ISSUER": oidc_realm,
    "OIDC_LOGOUT_URL": oidc_realm + "/protocol/openid-connect/logout",
}
