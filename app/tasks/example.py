import logging
import time

from celery import shared_task

# logger = get_task_logger(__name__)

logger = logging.getLogger("job.example")


@shared_task(ignore_result=False)
def dummy_task():
    """
    Example task that only returns "OK"
    :return: "OK" when finished
    """
    logger.info(f"Start dummy task : {time.ctime()}")
    print(f"Start dummy task : {time.ctime()}")
    return "OK"


@shared_task(ignore_result=False)
def check():
    logger.info("Checking every 10 seconds")
    print("Print Checking every 10 seconds")
