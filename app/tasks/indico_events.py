import logging

from celery import shared_task

from app.services.indico.indico_service import IndicoService

logger = logging.getLogger("job.indico_events")


@shared_task(ignore_result=False)
def get_next_events_task():
    indico_service = IndicoService()
    indico_service.get_events()
    return "OK"
