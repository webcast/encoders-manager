import logging

from celery import shared_task

from app.daos.encoder_group import EncoderGroupDAO
from app.daos.encoders import EncoderDAO
from app.models.base_encoder import EncoderStatus
from app.services.mailer.mail_service import MailService

logger = logging.getLogger("job.encoders")


@shared_task(ignore_result=False)
def check_encoder_group_task(group_id: int) -> str:
    room_enc = EncoderGroupDAO.get_by_id(group_id)

    # specify the room name
    room_name = f"{room_enc.room.indico_name} ({room_enc.name}): \n"
    add_room = False
    encoders_status = []

    for enc in room_enc.encoders:
        logger.info(f"Checking encoder {enc.hostname}")
        encoder = EncoderDAO.get_by_id(enc.id)
        # get the status of encoder
        response = encoder.get_status(save_heartbeat=True)
        # if the status cannot be fetched get status returns information about it,
        # set the encoder state as unreachable
        logger.debug(f"Encoder {enc.hostname} status: {response}")
        if not response["is_reachable"] or response["status"] == EncoderStatus.ERROR:
            logger.info(enc.hostname + " is unreachable")
            encoders_status.append(
                {
                    "hostname": encoder.hostname,
                    "status": "unreachable",
                }
            )
            add_room = True

    if add_room:
        logger.info(f"Sending status notification email for group {room_enc.name}...")
        service = MailService(logger=logger)
        service.send_encoder_status_email(room_name, encoders_status)

    return "OK"


@shared_task(ignore_result=False)
def check_encoders_task() -> str:
    """
    Example task that only returns "OK"
    :return: "OK" when finished
    """

    """
    Check the status of every encoder, send a mail with summary
    """
    logger.info("Daily encoders check")
    # Check all the encoders registered in the database
    room_encoders = EncoderGroupDAO.get_all()

    for room_enc in room_encoders:
        check_encoder_group_task.delay(room_enc.id)

    return "OK"
