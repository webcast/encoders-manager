import logging
import time
from datetime import datetime

from celery import shared_task
from flask import current_app, render_template

from app.daos.indico_events import IndicoEventsDAO
from app.extensions import db
from app.models.encoder_groups import EncoderGroup
from app.models.indico_events import EventType, IndicoEvent
from app.services.mailer.mail_service import MailService

logger = logging.getLogger("job.auto_stop")


def populate_email(event: IndicoEvent, encoder_group: EncoderGroup):
    """

    :param event:
    :type event: app.models.events.IndicoEvent
    :param encoder:
    :type encoder: app.models.encoders.RoomEncoder
    :return:
    :rtype:
    """
    prefix = ""
    if current_app.config.get("IS_DEV", False):
        prefix = "-TEST"

    subject = "[CES2" + prefix + "] Automatic stop failed for event"

    email_template = render_template("emails/auto_stop_event.html", event=event, encoder=encoder_group.name)

    return subject, email_template


@shared_task(ignore_result=False)
def stop_single_event_task(event_id: int):
    """
    Example task that only returns "OK"
    :return: "OK" when finished
    """
    event = IndicoEventsDAO.get_by_id(event_id)
    # Get the current datetime to the minutes
    event_stop_datetime = event.auto_stop_datetime.replace(second=0, microsecond=0)
    current_datetime = datetime.now().replace(second=0, microsecond=0)

    # Stop the event
    logger.debug(f"Stop event: Checking event {event.indico_id}")

    if event_stop_datetime != current_datetime:
        return "OK"

    if not event.encoder_group:
        logger.debug(f"Event {event.indico_id} has no encoder group")
        return "OK"

    logger.info(f"Stopping event: {event.indico_id} " f"automatically on stop_time: {current_datetime}")

    encoder_group = event.encoder_group
    result = None
    if event.event_type == EventType.STREAM:
        result = encoder_group.stop_stream()
    else:
        result = encoder_group.stop_all()

    if not result:
        logger.warning(f"Automatic stop failed for event. No result. {event.title}")
        return "ERROR"

    if "error" in result and result["error"]:
        logger.info(f"Automatic stop failed for event {event.title}")

        subject, message = populate_email(event, encoder_group)
        service = MailService(logger=logger)
        time.sleep(1)  # Wait at least 1 second between emails
        service.send_html_mail(
            current_app.config["MAIL_FROM"],
            current_app.config["MAIL_TO"],
            subject,
            message,
        )
        return "ERROR"
    else:
        logger.info(f"Automatic stop succeeded for event {event.title}")
        event.auto_stopped = True
        db.session.commit()
    return "OK"


@shared_task(ignore_result=False)
def stop_events_task():
    """
    Example task that only returns "OK"
    :return: "OK" when finished
    """
    events = IndicoEventsDAO.get_all_with_auto_stop()
    for event in events:
        stop_single_event_task.delay(event.id)

    return "OK"
