import logging
import time
from datetime import datetime, timedelta

import pytz
from celery import shared_task
from flask import current_app, render_template
from pytz import utc

from app.daos.encoder_group import EncoderGroupDAO
from app.daos.indico_events import IndicoEventsDAO
from app.extensions import db
from app.models.indico_events import EventType, IndicoEvent
from app.services.mailer.mail_service import MailService

logger = logging.getLogger("job.reminders")


def populate_email(event: IndicoEvent):
    prefix = ""
    if current_app.config.get("IS_DEV", False):
        prefix = "-TEST"

    subject = (
        "[ENCODERS-MANAGER"
        + prefix
        + "] [RECORDING] Reminder for event "
        + f"{event.indico_id} - {event.room.indico_name}"
    )
    operation = "recording and webcast"
    if event.event_type == EventType.STREAM:
        operation = "webcast"
    elif event.event_type == EventType.RECORDING:
        operation = "recording"

    email_template = render_template(
        "emails/reminders.html",
        current_instance=current_app.config["INSTANCE_HOSTNAME"],
        event=event,
        operation=operation,
        before_event=current_app.config["REMINDER_EVENT_MINUTES"],
    )

    return subject, email_template


def change_timezone(dt, timezone):
    """Convert a UTC datetime to a specific timezone"""
    utc_dt_naive = utc.localize(dt)
    utc_dt = utc_dt_naive.replace(tzinfo=pytz.utc)
    # prepare the final timezone
    new_time = utc_dt.astimezone(pytz.timezone(timezone))
    return new_time


@shared_task(ignore_result=False)
def send_single_reminder_task(event_id: int, now: datetime):
    event = IndicoEventsDAO.get_by_id(event_id)
    if event.room and not event.reminder_sent:
        event_start_datetime = event.start_datetime.astimezone(pytz.timezone("Europe/Zurich"))

        # check which ones are marked as running to save them
        reminder_time = event_start_datetime - timedelta(minutes=current_app.config["REMINDER_EVENT_MINUTES"])

        encoders = EncoderGroupDAO.get_by_room_id(event.room_id)
        # Send the reminder only for Monarch encoders 15 minutes before the event.
        if reminder_time <= now <= event_start_datetime and len(encoders) > 0:
            logger.debug(f"Sending reminder for event {event.indico_id}")
            # time to send the reminder
            subject, message = populate_email(event)
            service = MailService(logger=logger)
            time.sleep(1)  # Wait at least 1 second between emails
            service.send_html_mail(
                current_app.config["MAIL_FROM"],
                current_app.config["MAIL_TO"],
                subject,
                message,
            )

            logger.debug("Reminder sent!")
            # store the reminder sent
            event.reminder_sent = True
            db.session.commit()
    return "OK"


@shared_task(ignore_result=False)
def send_reminders_task():
    now = datetime.now().astimezone(pytz.timezone("Europe/Zurich"))
    events = IndicoEventsDAO.get_all_no_reminder_sent()
    for event in events:
        send_single_reminder_task.delay(event.id, now)
    return "OK"
