import logging
import smtplib
import time
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Optional

from flask import current_app, render_template


def populate_encoder_status_email(room, statuses):
    prefix = ""
    if current_app.config.get("IS_DEV", False):
        prefix = "-TEST"

    subject = f"[ENCODERS-MANAGER{prefix}] {room} Daily encoders check"

    email_template = render_template(
        "emails/encoders_check.html",
        room=room,
        statuses=statuses,
        current_date=datetime.now().date(),
    )

    return subject, email_template


class MailService:
    def __init__(self, logger=None):
        """

        :param contribution:
        :param logger:
        :type logger: logging.Logger
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.mail_service")

    def send_html_mail(self, email_from, email_to, subject, html):
        """
        Send html formatted mail.

        :param email_from: Email address that will appear on the FROM
        :type email_from: str
        :param email_to: Email address to send the emait to
        :type email_to: str[]
        :param subject: Subject of the email
        :type subject: str
        :param html: Content of the email
        :type html: str
        :return: Nothing
        :rtype: void
        """
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart("alternative")
        msg["Subject"] = subject
        msg["From"] = email_from
        # set only the real receipent of the message (without BCC etc)
        msg["To"] = email_to[0]

        # encode html message

        body = MIMEText(html, "html", "utf-8")
        msg.attach(body)
        s = smtplib.SMTP(current_app.config["MAIL_HOSTNAME"])

        s.sendmail(email_from, email_to, msg.as_string())
        s.quit()

    def send_encoder_status_email(
        self,
        room_name: str,
        encoders_status: list[dict],
        notification_email: Optional[str] = None,
    ):
        subject, message = populate_encoder_status_email(room_name, encoders_status)
        time.sleep(1)  # Wait at least 1 second between emails

        send_to = notification_email or current_app.config["MAIL_TO"]

        self.send_html_mail(current_app.config["MAIL_FROM"], send_to, subject, message)
