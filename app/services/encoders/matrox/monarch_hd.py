import enum
import logging
import time
from typing import Optional, Union

from requests import Response
from requests.auth import HTTPBasicAuth
from requests.exceptions import ConnectionError
from retry_requests import RSession, retry

from app.services.encoders.matrox import SSLAdapter

logger = logging.getLogger("webapp.monarch_hd_api")


class MonarchHDCommands(enum.Enum):
    GetStatus = "GetStatus"
    StartStreaming = "StartStreaming"
    StopStreaming = "StopStreaming"
    StartRecording = "StartRecording"
    StopRecording = "StopRecording"
    SetRTMP = "SetRTMP"
    SetRecordFileName = "SetRecordFileName"
    StartStreamingAndRecording = "StartStreamingAndRecording"
    StopStreamingAndRecording = "StopStreamingAndRecording"


class MonarchHDApiClient:
    def __init__(self, hostname, username, password):
        self.hostname = hostname
        self.user = username
        self.password = password
        self.requests_session = retry(RSession(timeout=3), retries=3, backoff_factor=0.5)
        self.requests_session.mount("https://", SSLAdapter())
        self.api_path = "/Monarch/syncconnect/sdk.aspx?command="

    def _make_api_call(self, path: str, api_call_name: str, handle_function) -> str:
        full_url = self._get_full_url(path)
        response = self.requests_session.get(
            full_url, auth=HTTPBasicAuth(self.user, self.password), verify=False, timeout=10
        )
        result = handle_function(api_call_name, response)
        return result

    def _get_full_url(self, path: str) -> str:
        hostname = (
            f"{self.user}:{self.password}@{self.hostname}"
            if self.hostname.startswith("https://")
            else f"https://{self.user}:{self.password}@{self.hostname}"
        )
        return hostname + self.api_path + path

    def handle_get_api_response(self, api_call_name: str, response: Response) -> Union[str, None]:
        result = None
        if not response:
            logger.warning(f"{self.hostname}: No response from MonarchHD API")
        elif response.status_code == 200:
            result = response.text
            if result == "FAILED":
                logger.warning(
                    f"{self.hostname}: FAILED calling Matrox API "
                    f"(Status code: {response.status_code}): {result} "
                    f"for API call: {api_call_name}"
                )

            elif result == "RETRY":
                logger.debug(
                    f"{self.hostname}: RETRY response from Matrox API "
                    f"(Status code: {response.status_code}). "
                    "the device is busy "
                    f"for API call: {api_call_name}"
                )

            elif result != "SUCCESS":
                logger.debug(
                    f"{self.hostname}: Unexpected response from Matrox "
                    f"API (Status code: {response.status_code}): "
                    f"{result} "
                    f"for API call: {api_call_name}"
                )
        else:
            logger.debug(
                f"Error in calling MonarchHD API. Status code: {response.status_code} for API call: {api_call_name}"
            )
        return result

    def get_status(self) -> str:
        command = MonarchHDCommands.GetStatus.value
        result = self._make_api_call(command, "get_status", self.handle_get_api_response)
        return result

    def set_rtmp_stream(self, wowza_origin_url, stream_name):
        command = MonarchHDCommands.SetRTMP.value
        full_command = f"{command},{wowza_origin_url},{stream_name}"
        logger.debug(f"{self.hostname}: Setting RTMP stream: {full_command}")
        result = self._make_api_call(full_command, "set_rtmp_stream", self.handle_get_api_response)
        return result

    def start_streaming(self) -> str:
        command = MonarchHDCommands.StartStreaming.value

        start_result = self._make_api_call(command, "start_streaming", self.handle_get_api_response)
        if start_result != "SUCCESS":
            logger.debug(f"{self.hostname}:  start_streaming response: {start_result}")

        return start_result

    def stop_streaming(self) -> str:
        command = MonarchHDCommands.StopStreaming.value
        logger.debug(f"{self.hostname}: Stopping stream")
        result = self._make_api_call(command, "stop_streaming", self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Received stop streaming response from encoder: {result}")
        return result

    def start_recording(self):
        command = MonarchHDCommands.StartRecording.value

        logger.debug(f"{self.hostname}: Starting recording: {command}")
        start_result = self._make_api_call(command, "start_recording", self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Got start recording response from Matrox API: {start_result}")

        if start_result == "SUCCESS":
            logger.debug(f"{self.hostname}: SUCCESS start_recording response from Matrox API")

        return start_result

    def stop_recording(self):
        command = MonarchHDCommands.StopRecording.value

        result = self._make_api_call(command, "stop_recording", self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Received stop recording response from encoder: {result}")
        return result

    def set_recording_path(self, recording_path):
        time.sleep(1)
        command = MonarchHDCommands.SetRecordFileName.value
        full_command = f"{command},{recording_path}"
        logger.debug(f"{self.hostname}: Setting recording path: {full_command}")
        result = self._make_api_call(full_command, "set_recording_path", self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Received set recording path response: {result}")
        return result

    def get_record_filename(self):
        time.sleep(1)
        command = "GetRecordFileName"
        full_command = command
        logger.debug(f"{self.hostname}: Getting recording path: {full_command}")
        result = self._make_api_call(full_command, "get_record_filename", self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Received get record file name: {result}")
        return result

    def configure_stream_recording(
        self,
        stream_name: str,
        wowza_origin_url: Optional[str] = None,
        recording_path: Optional[str] = None,
    ):
        stream_result = "FAILED"
        recording_result = "FAILED"

        if recording_path and stream_name:
            try:
                recording_result = self.set_recording_path(recording_path)
                logger.debug(f"{self.hostname}: Got set recording path result: {recording_result}")
            except ConnectionError as error:
                logger.error(
                    "Error setting recording path",
                    exc_info=True,
                    extra={
                        "error": error,
                        "tags": {
                            "encoder": self.hostname,
                            "recording_path": recording_path,
                        },
                    },
                )
                recording_result = "SUCCESS"
        else:
            recording_result = "SUCCESS"

        if wowza_origin_url:
            try:
                stream_result = self.set_rtmp_stream(wowza_origin_url, stream_name)
                if stream_result != "SUCCESS":
                    logger.debug(f"{self.hostname}: Got set stream result: {stream_result}")
            except ConnectionError as error:
                logger.warning(
                    "Error setting RTMP stream",
                    extra={
                        "error": error,
                        "tags": {"encoder": self.hostname, "stream_name": stream_name},
                    },
                )
                stream_result = "FAILED"
        else:
            stream_result = "SUCCESS"

        return stream_result, recording_result

    def start_both_encoders(self):
        logger.debug(f"{self.hostname}: Starting both encoders")
        command = MonarchHDCommands.StartStreamingAndRecording.value
        result = self._make_api_call(command, "start_both_encoders", self.handle_get_api_response)
        return result

    def stop_both_encoders(self):
        logger.debug(f"{self.hostname}: Stopping both encoders")
        command = MonarchHDCommands.StopStreamingAndRecording.value
        result = self._make_api_call(command, "stop_both_encoders", self.handle_get_api_response)
        return result
