import enum
import logging
import time
from typing import Optional, Union

from requests import Response
from requests.auth import HTTPBasicAuth
from retry_requests import RSession, retry

from app.models.base_encoder import EncoderStatus
from app.services.encoders.matrox import SSLAdapter

logger = logging.getLogger("webapp.monarch_hd_api")


class MonarchHDXCommands(enum.Enum):
    GetStatus = "GetStatus"
    StartStreaming = "StartEncoder1"
    StopStreaming = "StopEncoder1"
    StartBothEncoders = "StartBothEncoders"
    StopBothEncoders = "StopBothEncoders"
    StartRecording = "StartEncoder2"
    StopRecording = "StopEncoder2"
    SetRTMP = "SetRTMP"
    SetRecordFileName = "SetRecordFileName"


class MonarchHDXApiClient:
    def __init__(self, hostname, username, password):
        self.hostname = hostname
        self.user = username
        self.password = password
        self.requests_session = retry(RSession(timeout=3), retries=3, backoff_factor=0.5)
        self.requests_session.mount("https://", SSLAdapter())
        self.api_path = "/Monarch/syncconnect/sdk.aspx?command="
        self.stream_encoder = 1
        self.recording_encoder = 2

    def _make_api_call(self, path: str, handle_function) -> str:
        """

        :param url:
        :type url: str
        :param handle_function:
        :type handle_function: function
        :return:
        :rtype: str
        """
        time.sleep(2)
        hostname = self.hostname if self.hostname.startswith("https://") else "https://" + self.hostname
        full_url = hostname + self.api_path + path
        logger.debug(f"Calling MonarchHD API {full_url}")
        response = self.requests_session.get(
            full_url,
            auth=HTTPBasicAuth(self.user, self.password),
            verify=False,
            timeout=10,
        )
        result = handle_function(response)
        return result

    def handle_get_api_response(self, response: Response) -> Union[str, None]:
        """
        Handle the response of a GET request

        :param response: The response returned by the API
        :type response: requests.Response
        :return: (ERROR|Another string)
        :rtype: str
        """
        result = None
        if not response:
            logger.debug("Error in calling MonarchHD API. NO RESPONSE FROM API")

        if response.status_code == 200:
            result = response.text

            if response == "SUCCESS":
                logger.debug(f"{self.hostname}: SUCCESS response from Matrox API")
            elif response == "FAILED":
                logger.warning(f"{self.hostname}: Error calling Matrox API: {response}")
            else:
                logger.debug(f"{self.hostname}: Status: {response}")

        else:
            logger.debug(f"Error in calling MonarchHD API. Status code: {response.status_code}")
        return result

    def get_status(self) -> str:
        """
        Get status of monarch encoder
        """
        command = MonarchHDXCommands.GetStatus.value
        result = self._make_api_call(command, self.handle_get_api_response)
        return result

    def set_rtmp_stream(self, wowza_origin_url: str, stream_name: str):
        command = MonarchHDXCommands.SetRTMP.value
        full_command = f"{command},{self.stream_encoder},{wowza_origin_url},{stream_name}"
        result = self._make_api_call(full_command, self.handle_get_api_response)
        return result

    def start_streaming(self) -> EncoderStatus:
        """
        Start the streaming on the Epiphan Nano using an API call
        :param wowza_origin_url: The origin URL of the streaming server,
        including the application
        :type wowza_origin_url: str
        :param stream_name: The stream name that will be set on the streaming server
        :type stream_name: str
        :return: (ERROR|Nothing)
        :rtype: str
        """
        result = EncoderStatus.ERROR
        command = MonarchHDXCommands.StartStreaming.value

        start_result = self._make_api_call(command, self.handle_get_api_response)
        logger.debug(f"{self.hostname}: SUCCESSFUL response from Matrox API ({result})")
        if start_result == "SUCCESS":
            logger.debug(f"{self.hostname}: SUCCESS response from Matrox API")
            return EncoderStatus.OK

        logger.debug(f"Received start streaming response from encoder: {result}")
        return result

    def stop_streaming(self) -> str:
        command = MonarchHDXCommands.StopStreaming.value
        result = self._make_api_call(command, self.handle_get_api_response)
        logger.debug(f"Received stop streaming response from encoder: {result}")
        return result

    def start_recording(self):
        command = MonarchHDXCommands.StartRecording.value

        start_result = self._make_api_call(command, self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Got start recording response from Matrox API: {start_result}")
        if start_result == "SUCCESS":
            logger.debug(f"{self.hostname}: SUCCESS start_recording response from Matrox API")

        logger.debug(f"{self.hostname}: Received start recording response from encoder: {start_result}")
        return start_result

    def stop_recording(self):
        command = MonarchHDXCommands.StopRecording.value

        result = self._make_api_call(command, self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Received stop recording response from encoder: {result}")
        return result

    def set_recording_path(self, recording_path: str):
        command = MonarchHDXCommands.SetRecordFileName.value
        full_command = f"{command},{self.recording_encoder},{recording_path}"

        result = self._make_api_call(full_command, self.handle_get_api_response)
        logger.debug(f"{self.hostname}: Received set recording path response: {result}")
        return result

    def start_both_encoders(self):
        command = MonarchHDXCommands.StartBothEncoders.value
        result = self._make_api_call(command, self.handle_get_api_response)
        return result

    def stop_both_encoders(self):
        command = MonarchHDXCommands.StopBothEncoders.value
        result = self._make_api_call(command, self.handle_get_api_response)
        return result

    def configure_stream_recording(
        self,
        stream_name: str,
        wowza_origin_url: Optional[str] = None,
        recording_path: Optional[str] = None,
    ):
        stream_result = "FAILED"
        recording_result = "FAILED"

        if recording_path and stream_name:
            try:
                recording_result = self.set_recording_path(recording_path)
                logger.debug(f"Got recording result: {recording_result}")
            except ConnectionError as error:
                logger.error(
                    "Error setting recording path",
                    exc_info=True,
                    extra={
                        "error": error,
                        "tags": {"encoder": self.hostname, "stream_name": stream_name},
                    },
                )
                recording_result = "SUCCESS"

        if wowza_origin_url:
            try:
                stream_result = self.set_rtmp_stream(wowza_origin_url, stream_name)
                logger.debug(f"Got stream result: {stream_result}")
            except ConnectionError as error:
                logger.warning(
                    "Error setting RTMP stream",
                    exc_info=True,
                    extra={
                        "error": error,
                        "tags": {"encoder": self.hostname, "stream_name": stream_name},
                    },
                )
                stream_result = "SUCCESS"

        return stream_result, recording_result
