import ssl

from requests.adapters import HTTPAdapter
from urllib3.poolmanager import PoolManager


class SSLAdapter(HTTPAdapter):
    def init_poolmanager(self, *args, **kwargs):
        context = ssl.create_default_context()
        custom_ciphers = "DEFAULT:@SECLEVEL=2"
        self.ciphers = custom_ciphers
        context.check_hostname = False
        context.set_ciphers(custom_ciphers)

        self.poolmanager = PoolManager(*args, ssl_context=context, **kwargs)

        kwargs["ssl_context"] = context
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]
        return super().init_poolmanager(*args, **kwargs)
