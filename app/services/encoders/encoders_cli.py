import logging

import click
from flask.cli import AppGroup

from app.daos.encoders import EncoderDAO

logger = logging.getLogger("webapp.cli")

encoders_cli = AppGroup("encoders")


@encoders_cli.command("status")
@click.argument("hostname")
def get_status(hostname: str):
    logger.info("Getting status from encoder")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.get_status(save_heartbeat=True)
    logger.info(status)


@encoders_cli.command("heartbeat")
@click.argument("hostname")
def get_heartbeat(hostname: str):
    logger.info("Getting heartbeat from encoder")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.get_heartbeat()
    logger.info(status)


@encoders_cli.command("start-all")
@click.argument("hostname")
@click.argument("stream_name")
def start_all(hostname: str, stream_name: str):
    logger.info("Starting stream an recording")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.start_all(stream_name, save_heartbeat=True)
    logger.info(status)


@encoders_cli.command("stop-all")
@click.argument("hostname")
def stop_all(hostname: str):
    logger.info("Starting a test stream")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.stop_all(save_heartbeat=True)
    logger.info(status)


@encoders_cli.command("start-test")
@click.argument("hostname")
def start_test(hostname: str):
    logger.info("Starting a test stream")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.start_test(save_heartbeat=True)
    logger.info(status)


@encoders_cli.command("stop-test")
@click.argument("hostname")
def stop_test(hostname: str):
    logger.info("Stopping a test stream")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.stop_test()
    logger.info(status)


@encoders_cli.command("start-stream")
@click.argument("hostname")
@click.argument("stream_name")
def start_stream(hostname: str, stream_name: str):
    logger.info(f"Starting a stream: {stream_name}")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.start_stream(stream_name, save_heartbeat=True)
    logger.info(status)


@encoders_cli.command("stop-stream")
@click.argument("hostname")
def stop_stream(hostname: str):
    logger.info("Stopping a stream")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.stop_stream(save_heartbeat=True)
    logger.info(status)


@encoders_cli.command("start-recording")
@click.argument("hostname")
@click.argument("stream_name")
def start_recording(hostname: str, stream_name: str):
    logger.info(f"Starting a stream: {stream_name}")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.start_recording(stream_name)
    logger.info(status)


@encoders_cli.command("stop-recording")
@click.argument("hostname")
def stop_recording(hostname: str):
    logger.info("Stopping a stream")
    encoder = EncoderDAO.get_by_hostname(hostname)
    status = encoder.stop_recording()
    logger.info(status)
