import logging

import click
from flask.cli import AppGroup

from app.daos.encoder_group import EncoderGroupDAO

logger = logging.getLogger("webapp.cli")

groups_cli = AppGroup("groups")


@groups_cli.command("status")
@click.argument("id")
def get_group_status(id: int):
    logger.info("Getting status from encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.get_status()
    logger.info(status)


@groups_cli.command("start-test")
@click.argument("id")
def start_test_group(id: int):
    logger.info("Starting test on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.start_test()
    logger.info(status)


@groups_cli.command("stop-test")
@click.argument("id")
def stop_test_group(id: int):
    logger.info("Stopping test on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.stop_test()
    logger.info(status)


@groups_cli.command("start-stream")
@click.argument("id")
@click.argument("stream_name")
def start_stream_group(id: int, stream_name: str):
    logger.info("Starting stream on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.start_stream(stream_name)
    logger.info(status)


@groups_cli.command("stop-stream")
@click.argument("id")
def stop_stream_group(id: int):
    logger.info("Stopping stream on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.stop_stream()
    logger.info(status)


@groups_cli.command("start-recording")
@click.argument("id")
@click.argument("stream_name")
def start_recordng_group(id: int, stream_name: str):
    logger.info("Starting recording on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.start_recording(stream_name)
    logger.info(status)


@groups_cli.command("stop-recording")
@click.argument("id")
def stop_recording_group(id: int):
    logger.info("Stopping recording on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.stop_recording()
    logger.info(status)


@groups_cli.command("start-all")
@click.argument("id")
@click.argument("stream_name")
def start_all_group(id: int, stream_name: str):
    logger.info("Starting all on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.start_all(stream_name)
    logger.info(status)


@groups_cli.command("stop-all")
@click.argument("id")
def stop_all_group(id: int):
    logger.info("Stopping all on group encoder")
    encoder_group = EncoderGroupDAO.get_by_id(id)
    status = encoder_group.stop_all()
    logger.info(status)
