import enum
import logging
import re

from bs4 import BeautifulSoup
from requests import Response
from requests.auth import HTTPBasicAuth
from retry_requests import RSession, retry

logger = logging.getLogger("webapp.epiphan_nano_api")


class AllowedEpiphanMethods(enum.Enum):
    GET = "GET"
    SET = "SET"


class RecordingFileSizeLimits(enum.Enum):
    ONE_GB = "1000000"
    TWO_GB = "2000000"
    FOUR_GB = "4000000"
    EIGHT_GB = "8000000"
    TWELVE_GB = "12000000"
    SIXTEEN_GB = "16000000"


class RecordingTimeLimits(enum.Enum):
    ONE_HOUR = "1:00:00"
    TWO_HOURS = "2:00:00"
    THREE_HOURS = "3:00:00"
    SIX_HOURS = "6:00:00"
    TWELVE_HOURS = "12:00:00"
    UNLIMITED = "0:00:00"


class EpiphanNanoAPIClient:
    AVAILABLE_REQUESTS = {
        "START_STREAMING_API_PATH": (
            "/admin/channel1/set_params.cgi?" "publish_enabled=on&rtmp_url={wowza_origin_url}&rtmp_stream={stream_name}"
        ),
        "STOP_STREAMING_API_PATH": "/admin/channel1/set_params.cgi?publish_enabled=",
        "START_RECORDING_API_PATH": "/admin/channel1/set_params.cgi?rec_enabled=on&rec_prefix={recording_prefix}",  # noqa
        "STOP_RECORDING_API_PATH": "/admin/channel1/set_params.cgi?rec_enabled=",
        "GET_STATUS_API_PATH": (
            "/admin/channel1/get_params.cgi?"
            "product_name"
            "&firmware_version"
            "&rec_enabled"
            "&publish_enabled"
            "&publish_type"
            "&rec_format"
            "&rec_sizelimit"
            "&rec_timelimit"
        ),
        "SET_STATUS_API_PATH": ("/admin/channel1/set_params.cgi?{key}={value}"),
    }

    SUCCESSFUL_RESPONSE_SET = ""  # When a SET API call succeeds, it returns nothing

    def __init__(self, hostname, username, password):
        self.hostname = hostname
        self.user = username
        self.password = password
        self.requests_session = retry(RSession(timeout=3), retries=3, backoff_factor=0.5)

    def _call_with_method(self, path: str, mode: AllowedEpiphanMethods) -> str:
        if mode == AllowedEpiphanMethods.GET:
            result = self._make_api_call(path, self.handle_get_api_response)
        else:
            result = self._make_api_call(path, self.handle_set_api_response)
        return result

    def _make_api_call(self, path: str, handle_function) -> str:
        full_url = self._get_full_url(path)
        response = self.requests_session.get(
            full_url,
            auth=HTTPBasicAuth(self.user, self.password),
            verify=False,
            timeout=3,
        )
        result = handle_function(response)
        return result

    def _get_full_url(self, path: str) -> str:
        hostname = self.hostname if self.hostname.startswith("https://") else "https://" + self.hostname
        return hostname + path

    def handle_set_api_response(self, response) -> str:
        result = "ERROR"
        if not response:
            logger.debug("No response from Epiphan Nano API")
        elif response.status_code == 200:
            if response.text == self.SUCCESSFUL_RESPONSE_SET:
                result = "SUCCESS"
            else:
                logger.debug(f"Error in calling Epiphan Nano API. Response: {response.text}")
        else:
            logger.debug("Error in calling Epiphan Nano API. " f"Status code: {response.status_code}")
        return result

    def handle_get_api_response(self, response: Response) -> str:
        result = "ERROR"
        if not response:
            logger.debug("No response from Epiphan Nano API")
        elif response.status_code == 200:
            if not bool(BeautifulSoup(response.text, "html.parser").find()):
                result = response.text
            else:
                logger.debug(f"Error in calling Epiphan Nano API. Response: {response.text}")
        else:
            logger.debug("Error in calling Epiphan Nano API. " f"Status code: {response.status_code}")
        return result

    def get_status(self) -> dict:
        device_status = self._call_with_method(
            self.AVAILABLE_REQUESTS["GET_STATUS_API_PATH"], AllowedEpiphanMethods.GET
        )
        result = self.parse_status_response(device_status)
        return result

    def set_param(self, key: str, value: str) -> dict:
        device_status = self._call_with_method(
            self.AVAILABLE_REQUESTS["SET_STATUS_API_PATH"].format(key, value),
            AllowedEpiphanMethods.SET,
        )
        result = self.parse_status_response(device_status)
        return result

    def parse_status_response(self, device_status: str):
        status_lines = device_status.split("\n")
        result = {}
        for line in status_lines:
            line_pieces = line.split("=")
            if len(line_pieces) == 2:
                result[line_pieces[0].strip()] = line_pieces[1].strip()
        return result

    def start_streaming(self, wowza_origin_url, stream_name) -> str:
        wowza_url = self.convert_url_to_rtmp(wowza_origin_url)
        request_path = self.AVAILABLE_REQUESTS["START_STREAMING_API_PATH"].format(
            wowza_origin_url=wowza_url, stream_name=stream_name
        )
        result = self._call_with_method(request_path, AllowedEpiphanMethods.SET)
        logger.debug(f"Received start streaming response from encoder: {result}")
        return result

    def convert_url_to_rtmp(self, wowza_origin_url):
        rtmp_scheme = "rtmp://"

        def strip_scheme(url: str):
            return re.sub(r"^https?:\/\/", "", url)

        if not wowza_origin_url.startswith(rtmp_scheme):
            wowza_url = rtmp_scheme + strip_scheme(wowza_origin_url)
        else:
            wowza_url = wowza_origin_url
        return wowza_url

    def stop_streaming(self):
        device_status = self._call_with_method(
            self.AVAILABLE_REQUESTS["STOP_STREAMING_API_PATH"],
            AllowedEpiphanMethods.SET,
        )
        return device_status

    def start_recording(self, recording_prefix):
        device_status = self._call_with_method(
            self.AVAILABLE_REQUESTS["START_RECORDING_API_PATH"].format(recording_prefix=recording_prefix),
            AllowedEpiphanMethods.SET,
        )
        return device_status

    def stop_recording(self):
        device_status = self._call_with_method(
            self.AVAILABLE_REQUESTS["STOP_RECORDING_API_PATH"],
            AllowedEpiphanMethods.SET,
        )
        return device_status

    def set_recording_file_size_limit(self, size_limit: RecordingFileSizeLimits):
        device_status = self._call_with_method(
            self.AVAILABLE_REQUESTS["SET_STATUS_API_PATH"].format(key="rec_sizelimit", value=size_limit),
            AllowedEpiphanMethods.SET,
        )
        return device_status

    def set_recording_time_limit(self, size_limit: RecordingTimeLimits):
        device_status = self._call_with_method(
            self.AVAILABLE_REQUESTS["SET_STATUS_API_PATH"].format(key="rec_timelimit", value=size_limit),
            AllowedEpiphanMethods.SET,
        )
        return device_status
