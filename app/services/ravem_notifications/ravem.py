import logging
from urllib.parse import urlencode

import requests
from flask import current_app

from app.daos.settings import SettingsDAO


def get_ravem_notifications_status():
    settings = SettingsDAO.get_all()
    return settings.enable_ravem_notifications


class RavemService:
    """
    Notify ravem for the new service status.
    """

    notify_path = "/api/v2/videoconference/status"

    def __init__(self, room_indico_name, logger=None):
        """

        :param room_indico_name: Name of the room
        :type room_indico_name: str
        :param logger: Logger instance
        :type logger: logging.logger
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.ravem_service")
        self._room = room_indico_name
        self.ravem_url = current_app.config["RAVEM_URL"]
        self.api_key = current_app.config["RAVEM_API_KEY"]
        self.notifications_enabled = get_ravem_notifications_status()

    def _notify(self, url) -> bool:
        self.logger.debug(f"RAVEM: Notify new status: {url}")
        try:
            headers = {"Authorization": "Bearer " + self.api_key}
            response = requests.put(url, headers=headers, timeout=5)
            self.logger.debug(f"RAVEM: Response: {response}")
            return True
        except Exception as ex:
            self.logger.exception(
                "RAVEM: Error while sending notification for the new status",
                exc_info=True,
                extra={
                    "error": ex,
                    "url": url,
                    "room": self._room,
                    "tags": {"service", "ravem"},
                },
            )
            return False

    def notify(self, recording=False, webcast=False) -> bool:
        """
        Notify recording/webcast status changed. Notification is
        always for both to be sure to update Ravem with the latest
        status (for example, in case of previous errors on start/stop).
        """
        if self.notifications_enabled:
            # prepare url for recording
            params = {
                "where": "room_name",
                "value": self._room,
                "service_name": "recording",
                "status": int(recording),
            }
            url = self.ravem_url + self.notify_path + "?" + urlencode(params)
            self.logger.info(f"RAVEM: Sending notification for recording: {url}")
            result_recording = self._notify(url)

            # prepare url for webcast
            params = {
                "where": "room_name",
                "value": self._room,
                "service_name": "webcast",
                "status": int(webcast),
            }
            url = self.ravem_url + self.notify_path + "?" + urlencode(params)
            self.logger.info(f"RAVEM: Sending notification for webcast: {url}")
            result_webcast = self._notify(url)

            return result_recording and result_webcast
        return False

    def notify_all(self):
        return self.notify(recording=True, webcast=True)

    def notify_recording(self):
        return self.notify(recording=True, webcast=False)

    def notify_stream(self):
        return self.notify(recording=False, webcast=True)
