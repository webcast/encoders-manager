import logging
from typing import Optional

import requests
from flask import current_app

from app.daos.indico_events import IndicoEventsDAO
from app.daos.settings import SettingsDAO


def get_ces_notifications_status():
    settings = SettingsDAO.get_all()
    return settings.enable_ces_notifications


class CesService:
    """
    Notify CES there is a new recording to process
    """

    notify_path = "/api/v1/encoders/"

    def __init__(self, logger: Optional[logging.Logger] = None):
        """

        :param room_indico_name: Name of the room
        :type room_indico_name: str
        :param logger: Logger instance
        :type logger: logging.logger
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.ces_service")
        self.ces_url = current_app.config["CES_URL"]
        self.api_key = current_app.config["CES_API_KEY"]
        self.notifications_enabled = get_ces_notifications_status()

    def _notify(self, url: str, data: dict) -> bool:
        self.logger.debug(f"CES: Notify new recording stop: {url}")
        try:
            headers = {
                "Authorization": "Bearer " + self.api_key,
                "Content-Type": "application/json",
            }
            response = requests.post(url, json=data, headers=headers, timeout=5)
            self.logger.debug(f"CES: Response: {response} ({response.text})")
            if response.status_code == 200:
                return True
        except Exception as ex:
            self.logger.exception(
                "CES: Error while sending notification for the new status",
                exc_info=True,
                extra={
                    "error": ex,
                    "url": url,
                },
            )
        return False

    def notify(
        self,
        indico_event_internal_id: int,
    ) -> bool:
        if self.notifications_enabled:
            indico_event = IndicoEventsDAO.get_by_id(indico_event_internal_id)

            result = {
                "indico_id": indico_event.indico_id,
                "event_title": indico_event.title,
                "encoder_camera": indico_event.encoder_camera,
                "encoder_camera_type": indico_event.encoder_camera_type,
                "encoder_slides": indico_event.encoder_slides,
                "encoder_slides_type": indico_event.encoder_slides_type,
                "recording_path": indico_event.recording_path or "",
                "recording_camera_path": indico_event.recording_camera_path or "",
                "recording_slides_path": indico_event.recording_slides_path or "",
                "stream_camera_name": indico_event.stream_camera_name,
                "stream_slides_name": indico_event.stream_slides_name,
                "room_name": indico_event.room.indico_name,
                "event_type": indico_event.event_type.value,
                "audience": indico_event.audience.value,
            }

            url = self.ces_url + self.notify_path
            self.logger.info(f"CES: Sending notification: {url}")
            return self._notify(url, result)
        return False
