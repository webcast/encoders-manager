import hashlib
import hmac
import logging
import time
from urllib.parse import urlencode

import requests
from flask import current_app, current_app as app
from werkzeug.datastructures import MultiDict

from app.daos.settings import SettingsDAO
from app.models.base_encoder import Encoder, EncoderSignalType
from app.models.indico_events import IndicoEvent


def get_webcast_website_notifications_status():
    settings = SettingsDAO.get_all()
    return settings.enable_webcast_website_notifications


class WebcastWebsiteService:
    """
    Notify Webcast Website with the webcast parameters.
    """

    notify_path = "/api/v1/update-stream/"
    webcast_api = "stream_update"

    def __init__(self, logger=None):
        """

        :param room_indico_name: Name of the room
        :type room_indico_name: str
        :param logger: Logger instance
        :type logger: logging.logger
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("webapp.webcast_service")
        self.webcast_website_url = current_app.config["WEBCAST_WEBSITE_URL"]
        self.api_key = current_app.config["WEBCAST_WEBSITE_API_KEY"]
        self.secret_key = current_app.config["WEBCAST_WEBSITE_SECRET_KEY"]
        self.notifications_enabled = get_webcast_website_notifications_status()

    def _build_request(self, path, params, api_key=None, secret_key=None):
        """
        Generates an API request with the hmac signature.
        :param path: Relative path to the API. I.E. /api/
        :param params: Parameters of the request
        :param api_key: Api key
        :param secret_key: Secret Key
        :return: The generated request parameters
        """
        items = params.items() if hasattr(params, "items") else list(params)
        if api_key:
            items.append(("ak", api_key))
        if secret_key:
            items.append(("timestamp", str(int(time.time()))))
            items = sorted(items, key=lambda x: x[0].lower())
            url = f"{path}?{urlencode(items)}"
            signature = hmac.new(bytearray(secret_key, "UTF-8"), bytearray(url, "UTF-8"), hashlib.sha1).hexdigest()
            items.append(("signature", signature))
        return items

    def _api_request(self, method, path, arg_params) -> requests.Response:
        """
        Makes a request to the  Indico API

        :param method: The method type of the request (GET or POST)
        :param path: The JSON path of the request.
        :param arg_params: Extra parameters of the request. They depend on the path.
        :return: The content type of the response and the response text
        """
        webcast_website_url_with_path = self.webcast_website_url + path
        request_values = MultiDict(arg_params)
        method = method.upper()
        params = request_values.items(multi=True)

        data = self._build_request(path, params, self.api_key, self.secret_key)
        request_args = {"params": data} if method == "GET" else {"data": data}
        self.logger.debug(f"Making a {method} request to Webcast Website: " f"{webcast_website_url_with_path}")
        response = requests.request(method, webcast_website_url_with_path, **request_args, timeout=5)
        return response

    def _notify(self, params) -> bool:
        """
        Send notification.
        """
        self.logger.debug("Webcast Website being notified")
        try:
            response = self._api_request("GET", self.notify_path, params)
            if response.status_code != 200:
                self.logger.warning(
                    "Unable to contact the Webcast Website",
                    exc_info=True,
                    extra={
                        "status_code": response.status_code,
                        "tags": {"service": "webcast_website"},
                    },
                )
                return False
            self.logger.debug(f"WEBCAST WEBSITE: Response: {response}")
            return True
        except Exception as exc:
            app.logger.exception(
                "WEBCAST WEBSITE: Error while sending notification for the new status",
                exc_info=True,
                extra={"error": exc},
            )
            return False

    def notify(self, indico_id, room_video_quality, app_name, camera_stream, slides_stream) -> bool:
        """
        Generate notification with the parameters needed byt he webcast website.
        Needed fields:
        {
            indico_id: "indicoid",
            room_video_quality: "HD",
            app_name: "appname",
            camera_stream: "camera_stream",
            slides_stream: "slides_stream"
        }
        """
        if self.notifications_enabled:
            params = {
                "api": self.webcast_api,
                "indico_id": indico_id,
                "room_video_quality": room_video_quality,
                "app_name": app_name,
                "camera_stream": camera_stream,
                "slides_stream": slides_stream,
            }
            result = self._notify(params)
            return result
        return False

    def notify_stream(self, event: IndicoEvent, encoders: list[Encoder]) -> bool:
        """
        Send notification to webcast website with specific parameters
        """

        quality = "HD"

        camera_stream = ""
        slides_stream = ""
        stream_application = ""
        for enc in encoders:
            encoder: Encoder = enc
            stream_application = encoder.stream_application
            if encoder.signal_type == EncoderSignalType.CAMERA:
                camera_stream = encoder.stream_name
            else:
                slides_stream = encoder.stream_name

        self.logger.debug(
            f"WEBCAST WEBSITE: indico_id {event.indico_id}, quality: {quality},"
            f"app: {stream_application}, camera_streamn: "
            f"{camera_stream}, slides stream:  {slides_stream}"
        )

        result = self.notify(
            indico_id=event.indico_id,
            room_video_quality=quality,
            app_name=stream_application,
            camera_stream=camera_stream,
            slides_stream=slides_stream,
        )
        return result
