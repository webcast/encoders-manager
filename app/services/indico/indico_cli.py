import logging

from flask.cli import AppGroup

from app.services.indico.indico_service import IndicoService

logger = logging.getLogger("webapp.cli")

indico_cli = AppGroup("indico")


@indico_cli.command("import")
def import_events():
    logger.info("Importing events from Indico")
    service = IndicoService(logger=logger)
    service.get_events()


# @indico_cli.command("import-event")
# @click.argument("indico_id")
# def import_event(indico_id: str):
#     logger.info("Importing event from Indico")
#     service = IndicoService(logger=logger)
#     service.get_event(indico_id)
