import logging

from app.daos.indico_events import IndicoEventsDAO
from app.extensions import db
from app.models.indico_events import Audience, EventType, IndicoEvent
from app.services.indico.indico_api_client import IndicoApiClient


class IndicoServiceException(Exception):
    pass


class IndicoService:
    """
    Use Indico Export API to fetch next webcast/recordings.
    """

    # Used on events with no room associated
    DEFAULT_ROOM_NAME = "External URI"

    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.indico_api_client")

    def get_events(self) -> list[IndicoEvent]:
        api = IndicoApiClient(logger=self.logger)
        events = api.fetch_webcasts_recordings()
        self.logger.debug(f"Got {len(events['results'])} events from Indico")
        if "count" in events and int(events["count"]) >= 0 and "results" in events:
            if int(events["count"]) == 0:
                self.logger.info("No Indico events for the next 15 days")
                return []

            accepted_events = [event for event in events["results"] if event["status"] == "A"]

            self.logger.debug(f"Found {len(accepted_events)} accepted events on the next 15 days")
            accepted_events_length = 0
            processed_ids = []
            processed_events = []
            for event_data in accepted_events:
                indico_id = event_data["event_id"]
                if indico_id in processed_ids:
                    continue

                audience = self.extract_audience(event_data)

                event_type = EventType.RECORDING
                if "webcast" in event_data["services"] and "recording" in event_data["services"]:
                    event_type = EventType.STREAM_RECORDING
                elif "webcast" in event_data["services"]:
                    event_type = EventType.STREAM
                else:
                    event_type = EventType.RECORDING

                event = self.get_event(
                    indico_id,
                    audience=audience,
                    event_type=event_type,
                    is_locked=False,
                )
                accepted_events_length += 1
                processed_ids.append(indico_id)
                processed_events.append(event)

            return processed_events
        return []

    def process_event(self, event_json: dict, audience: Audience, is_locked: bool = False) -> IndicoEvent:
        indico_id = event_json["event_id"]

        event = db.session.execute(db.select(IndicoEvent).filter_by(indico_id=indico_id)).scalar_one_or_none()

        if event:
            if not event.is_running:
                self.logger.info(f"Updating event {indico_id}: {event_json['title']}")
                event = self.update_event(indico_id, event_json, audience, is_locked=is_locked)
            else:
                self.logger.info(f"Event {indico_id} is running and cannot be updated")
        else:
            self.logger.debug(f"Creating event {indico_id}: {event_json['title']}")
            event = self.create_event(indico_id, event_json, audience=audience, is_locked=is_locked)

        return event

    def create_event(
        self,
        indico_id: str,
        event_json: dict,
        audience: Audience,
        is_locked: bool = False,
    ) -> IndicoEvent:
        event = IndicoEventsDAO.create(indico_id, event_json, audience=audience, is_locked=is_locked)
        return event

    def extract_audience(self, event_json: dict) -> Audience:
        if event_json["audience"] in ["ATLAS collaborators only", "ATLAS"]:
            audience = Audience.ATLAS
        elif event_json["audience"] in ["CMS collaborators only", "CMS"]:
            audience = Audience.CMS
        else:
            audience = Audience.PUBLIC
        return audience

    def get_event(
        self,
        indico_id: str,
        event_type: EventType,
        audience: Audience,
        is_locked: bool = True,
    ) -> IndicoEvent:
        # Lock the event if it was imported manually
        api = IndicoApiClient(logger=self.logger)
        event_dict = api.fetch_event(indico_id)
        if event_dict and "count" in event_dict and int(event_dict["count"]) == 1:
            event_dict = event_dict["results"][0]

            self.logger.debug(f"Event type {event_type}")
            self.logger.debug(f"Event audience {audience}")

            event_dict["event_type"] = event_type
            event_dict["audience"] = audience

            # These fields are different on this endpoint
            event_dict["event_id"] = event_dict["id"]
            event_dict["room_full_name"] = event_dict["roomFullname"]
            event = self.process_event(event_dict, audience=audience, is_locked=is_locked)

            return event

        else:
            raise IndicoServiceException(f"{indico_id} Event not found on fetch")

    def update_event(
        self,
        indico_id: str,
        event_json: dict,
        audience: Audience,
        is_locked: bool = False,
    ) -> IndicoEvent:
        event = IndicoEventsDAO.update(indico_id, event_json, audience, is_locked=is_locked)
        return event
