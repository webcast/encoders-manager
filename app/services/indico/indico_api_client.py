import logging

import requests
from flask import current_app

from app.extensions import cache

logger = logging.getLogger("webapp.indico_api")


class IndicoApiClient:
    """
    Use Indico Export API to fetch next webcast/recordings.
    """

    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.indico_api_client")

        self.INDICO_ENDPOINT = current_app.config["INDICO_ENDPOINT"]
        self.INDICO_ACCESS_TOKEN = current_app.config["INDICO_ACCESS_TOKEN"]
        self.build_api_urls()

    def build_api_urls(self):
        self.INDICO_APIPATH_EVENT = self.INDICO_ENDPOINT + "/export/event/{}.json"
        self.INDICO_APIPATH_WEBCAST_RECORDING = self.INDICO_ENDPOINT + "/export/webcast-recording.json"

    def make_get_request(self, url: str) -> dict:
        try:
            response_json = (
                requests.get(
                    url,
                    headers={"Authorization": f"Bearer {self.INDICO_ACCESS_TOKEN}"},
                    timeout=5,
                )
            ).json()
        except requests.exceptions.JSONDecodeError as ex:
            logger.error(
                "Error decoding JSON from Indico API. Verify Indico credentials.",
                extra={"error": ex, "tags": {"service": "indico"}},
            )
            raise ex

        return response_json

    @cache.memoize(50)
    def fetch_event(self, indico_id: str) -> dict:
        logger.debug(f"Fetching Indico event {indico_id}")
        url = self.INDICO_APIPATH_EVENT.format(indico_id)
        result = self.make_get_request(url)
        return result

    @cache.memoize(50)
    def fetch_webcasts_recordings(self) -> dict:
        query_params = "?from=today&to=15d&tz=UTC&o=start"
        request_url = self.INDICO_APIPATH_WEBCAST_RECORDING + query_params
        result = self.make_get_request(request_url)
        return result
