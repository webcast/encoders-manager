import logging

from flask import Blueprint

logger = logging.getLogger("webapp.blueprints")


def _blueprint_factory(name: str, partial_module_string: str, url_prefix):
    """
    Generates blueprint objects for view modules.

    :param partial_module_string:  String representing a view module
    without the absolute path (e.g. 'home.index' for pypi_portal.views.home.index).
    :param url_prefix: URL prefix passed to the blueprint.
    :return: Blueprint instance for a view module.
    """
    import_name = f"app.views.{partial_module_string}"
    print(f"Creating blueprint for {import_name}")
    blueprint = Blueprint(name, import_name, url_prefix=url_prefix)
    return blueprint


admin_custom_forms = _blueprint_factory(
    "admin_actions_bp", "admin.custom_forms.custom_forms_blueprints", "/_admin_actions/"
)

authentication_blueprint = _blueprint_factory("authentication", "authentication", "")


all_blueprints = (
    authentication_blueprint,
    admin_custom_forms,
)
