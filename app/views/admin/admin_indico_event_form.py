from flask_admin import expose

from app.forms import SendToApiForm
from app.views.admin.admin_indico_event_list import IndicoEventListView
from app.views.admin_protected_view import AdminProtectedView


class IndicoEventFormView(AdminProtectedView):
    edit_template = "admin/edit_event_template.html"
    form_excluded_columns = [
        "stats",
    ]

    @expose("/edit/", methods=("GET", "POST"))
    def edit_view(self):
        # natural_sort(model_contributions, key=lambda x: x.contribution_id)
        send_to_ces_form = SendToApiForm()
        send_to_ravem_form = SendToApiForm()
        send_to_webcast_form = SendToApiForm()

        self._template_args["send_to_ces_form"] = send_to_ces_form
        self._template_args["send_to_ravem_form"] = send_to_ravem_form
        self._template_args["send_to_webcast_form"] = send_to_webcast_form
        return super(IndicoEventFormView, self).edit_view()


class IndicoEventModelView(IndicoEventFormView, IndicoEventListView):
    pass
