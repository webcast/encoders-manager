import logging

from flask import flash, request
from flask_admin import expose

from app.daos.api_tokens import ApiTokenDAO
from app.forms import ApiKeyForm
from app.views.admin_protected_view import AdminProtectedView

logger = logging.getLogger("webapp")


class ApiTokenDetailView(AdminProtectedView):
    create_template = "admin/api_token_new.html"

    @expose("/new/", methods=("GET", "POST"))
    def create_view(self):
        logger.debug("edit view")
        api_form = ApiKeyForm()

        if request.method == "POST":
            if api_form.validate_on_submit():
                token_name = api_form.token_name.data
                scope = api_form.scope.data
                token = ApiTokenDAO.create({"tokenName": token_name, "scope": scope})
                self._template_args["token"] = token.access_token
                flash("Api token has been created")
            else:
                flash("Form is not valid")

        self._template_args["api_form"] = api_form
        return super(ApiTokenDetailView, self).create_view()
