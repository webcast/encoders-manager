from app.views.admin_protected_view import AdminProtectedView


class IndicoEventListView(AdminProtectedView):
    column_exclude_list = [
        "encoder_camera",
        "encoder_camera_type",
        "encoder_slides",
        "encoder_slides_type",
        "stream_camera_name",
        "stream_slides_name",
        "recording_camera_path",
        "recording_slides_path",
        "stream_start_datetime",
        "stream_stop_datetime",
        "recording_start_datetime",
        "recording_stop_datetime",
    ]
    column_searchable_list = ["title", "indico_id"]
