from app.views.admin.admin_encoder_list import EncoderListView
from app.views.admin_protected_view import AdminProtectedView


class EncoderFormView(AdminProtectedView):
    form_excluded_columns = ["heartbeats", "encoder_type"]


class EncoderModelView(EncoderFormView, EncoderListView):
    pass
