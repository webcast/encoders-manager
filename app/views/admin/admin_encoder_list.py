from app.views.admin_protected_view import AdminProtectedView


class EncoderListView(AdminProtectedView):
    column_exclude_list = [
        "stream_name",
        "recording_path",
        "recording_start_datetime",
        "recording_stop_datetime",
        "stream_start_datetime",
        "stream_stop_datetime",
    ]
    column_searchable_list = [
        "hostname",
        "stream_application",
        "stream_server",
        "encoder_type",
        "signal_type",
    ]
