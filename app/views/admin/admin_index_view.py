from datetime import date

import flask_admin as admin
from flask import current_app, flash, redirect, request, session, url_for
from flask_admin import expose
from flask_login import current_user

from app.daos.settings import SettingsDAO
from app.forms import MetricsForm, SettingsForm
from app.utils.metrics import get_metrics

NOT_SET = "Not set"


class ProtectedAdminIndexView(admin.AdminIndexView):
    @expose(
        "/",
        methods=(
            "GET",
            "POST",
        ),
    )
    def index(self):
        if not current_user.is_authenticated:
            return redirect(url_for("authentication.login"))

        if session.get("next_url"):
            next_url = session.get("next_url")
            session.pop("next_url", None)
            return redirect(next_url)

        selected_year = date.today().year
        db_instance = current_app.config.get("DB_SERVICE_NAME", NOT_SET)
        ravem_instance = current_app.config.get("RAVEM_URL", NOT_SET)
        webcast_website_instance = current_app.config.get("WEBCAST_WEBSITE_URL", NOT_SET)
        ces_instance = current_app.config.get("CES_URL", NOT_SET)

        settings = SettingsDAO.get_all()
        settings_form = SettingsForm(formdata=None, obj=settings)
        metrics_form = MetricsForm()

        if request.method == "POST" and metrics_form.validate_on_submit():
            selected_year = metrics_form.selected_year.data
            flash(f"Displaying stats for year {selected_year}")

        if selected_year:
            selected_year = int(selected_year)

        metrics = get_metrics(year=selected_year)

        return self.render(
            "admin/index.html",
            metrics_form=metrics_form,
            metrics=metrics,
            settings_form=settings_form,
            selected_year=selected_year,
            db_instance=db_instance,
            ravem_instance=ravem_instance,
            ces_instance=ces_instance,
            webcast_website_instance=webcast_website_instance,
        )
