from app.views.admin.admin_room_list import RoomListView
from app.views.admin_protected_view import AdminProtectedView


class RoomFormView(AdminProtectedView):
    form_excluded_columns = [
        "indico_events",
    ]


class RoomModelView(RoomFormView, RoomListView):
    pass
