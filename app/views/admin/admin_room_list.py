from app.views.admin_protected_view import AdminProtectedView


class RoomListView(AdminProtectedView):
    column_searchable_list = ["indico_name", "room_full_name"]
