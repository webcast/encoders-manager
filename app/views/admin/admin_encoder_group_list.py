from app.views.admin_protected_view import AdminProtectedView


class EncoderGroupListView(AdminProtectedView):
    column_list = [
        "encoders",
        "name",
        "is_test_running",
        "is_running",
        "room",
        "created_on",
    ]
    column_searchable_list = ["encoders.hostname", "room.indico_name"]
    column_labels = {"encoders.hostname": "hostname"}
