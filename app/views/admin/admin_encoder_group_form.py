from app.views.admin.admin_encoder_group_list import EncoderGroupListView
from app.views.admin_protected_view import AdminProtectedView


class EncoderGroupFormView(AdminProtectedView):
    form_excluded_columns = [
        "indico_events",
    ]
    column_descriptions = {
        "start_as_composite": """<p>This field is used to determine if the encoder
         group should start as a composite.
        Its value will only take effect when the encoder group starts.</p>
        <p><strong>Only applies to Epiphan Pearl 2 devices.</stron></p>
        <p><strong>Configuration of the encoder: </strong>
        <ul>
            <li>Channel 1 must be slides.</li>
            <li>Channel 2 must be camera.
                <ul>
                    <li>Layout ID1: Camera.</li>
                    <li>Layout ID2: Composite. 75% slides / 25% camera / vertically
                     centered</li>
                </ul>
            </li>
        </ul>
        </p>
        """,
    }


class EncoderGroupModelView(EncoderGroupFormView, EncoderGroupListView):
    pass
