from .indico_events_actions import (  # noqa
    manual_send_to_ces_view,
    manual_send_to_ravem_view,
    manual_send_to_webcast_view,
)
from .settings_form_handler import settings_form_handle  # noqa
