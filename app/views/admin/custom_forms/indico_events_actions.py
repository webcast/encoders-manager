import logging

from flask import flash, redirect, url_for
from flask_login import login_required

from app.daos.indico_events import IndicoEventsDAO
from app.daos.settings import SettingsDAO
from app.extensions import db
from app.forms import SendToApiForm
from app.models.indico_events import EventType, IndicoEvent
from app.models.settings import Settings
from app.services.ces_notifications.ces import CesService
from app.services.ravem_notifications.ravem import RavemService
from app.services.webcast_website_notifications.webcast import WebcastWebsiteService
from app.views.blueprints import admin_custom_forms

logger = logging.getLogger("webapp.notifications")


@admin_custom_forms.route("_send_to_ces/<int:event_id>/", methods=("POST",))
@login_required
def manual_send_to_ces_view(event_id: int):
    form = SendToApiForm()
    logger.debug(f"form: {form}")
    if form.validate_on_submit():
        settings: Settings = SettingsDAO.get_all()
        if settings.enable_ces_notifications:
            event = IndicoEventsDAO.get_by_id(event_id)
            logger.debug(f"Sending event to CES {event.indico_id} ({event.title})")

            message_type = "error"
            message = "Error while sending event to CES"
            result = False
            service = CesService(logger=logger)
            result = service.notify(event.id)

            if result:
                message_type = "success"
                message = "CES notified successfully"
                event.ces_notified = True
                db.session.commit()
        else:
            message_type = "warning"
            message = "CES notifications are disabled"

        flash(message, message_type)

    return redirect(url_for("indicoevent.edit_view", id=event_id))


@admin_custom_forms.route("_send_to_ravem/<int:event_id>/", methods=("POST",))
@login_required
def manual_send_to_ravem_view(event_id: int):
    form = SendToApiForm()
    logger.debug(f"form: {form}")
    if form.validate_on_submit():
        settings: Settings = SettingsDAO.get_all()
        if settings.enable_ravem_notifications:
            event: IndicoEvent = IndicoEventsDAO.get_by_id(event_id)
            logger.debug(f"Notifying event to Ravem {event.indico_id} ({event.title})")

            message_type = "error"
            message = "Error while sending event to Ravem"
            result = False
            service = RavemService(event.room.indico_name, logger=logger)
            result = service.notify(
                recording=event.event_type in [EventType.RECORDING, EventType.STREAM_RECORDING],
                webcast=event.event_type in [EventType.STREAM, EventType.STREAM_RECORDING],
            )
            event.ravem_notified = True
            db.session.commit()

            if result:
                message_type = "success"
                message = "Ravem notified successfully"
        else:
            message_type = "warning"
            message = "Ravem notifications are disabled"
            result = False

        flash(message, message_type)

    return redirect(url_for("indicoevent.edit_view", id=event_id))


@admin_custom_forms.route("_send_to_webcast/<int:event_id>/", methods=("POST",))
@login_required
def manual_send_to_webcast_view(event_id: int):
    form = SendToApiForm()
    logger.debug(f"form: {form}")
    if form.validate_on_submit():
        settings: Settings = SettingsDAO.get_all()
        if settings.enable_webcast_website_notifications:
            if not settings.webcast_website_url:
                message_type = "warning"
                message = "Event doesn't have an encoder group associated. " "Won't to Webcast Website"
                result = False
            else:
                event = IndicoEventsDAO.get_by_id(event_id)
                logger.debug("Notifying event to Webcast Website " f"{event.indico_id} ({event.title})")

                message_type = "error"
                message = "Error while sending event to Webcast Website"
                result = False

                service = WebcastWebsiteService(logger=logger)
                result = service.notify_stream(event, event.encoder_group.encoders)

                if result:
                    message_type = "success"
                    message = "Webcast Website notified successfully"
                event.webcast_website_notified = True
                db.session.commit()
        else:
            message_type = "warning"
            message = "Webcast Website notifications are disabled"
            result = False

        flash(message, message_type)

    return redirect(url_for("indicoevent.edit_view", id=event_id))
