from datetime import date

from flask_wtf import FlaskForm
from wtforms import BooleanField, IntegerField, SelectField, StringField
from wtforms.validators import DataRequired, InputRequired

from app.models.token_blacklist import ApiTokenResources

first_year = 2023
years = list(reversed(range(first_year, date.today().year + 1)))


class MetricsForm(FlaskForm):
    choices = (
        years
        if len(years) > 0
        else [
            first_year,
        ]
    )

    selected_year = SelectField(
        "Select the metrics for the following year",
        choices=choices,
        validators=[InputRequired()],
    )


class SettingsForm(FlaskForm):
    enable_ravem_notifications = BooleanField()
    enable_webcast_website_notifications = BooleanField()
    enable_ces_notifications = BooleanField()


class SendToApiForm(FlaskForm):
    event_id = IntegerField()


class ApiKeyForm(FlaskForm):
    token_name = StringField("Token name", validators=[DataRequired()])
    choices = [(item.value, item.name) for item in ApiTokenResources]

    scope = SelectField(
        "Select an scope",
        choices=choices,
        validators=[InputRequired()],
    )
