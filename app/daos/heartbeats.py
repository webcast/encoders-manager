from app.daos.encoders import EncoderDAO
from app.extensions import db
from app.models.heartbeats import HeartBeat


class HeartBeatDAO:
    @staticmethod
    def get_heartbeat_by_hostname(hostname: str) -> HeartBeat:
        encoder = EncoderDAO.get_by_hostname(hostname)
        # Get the last heartbeat for the encoder
        last_heartbeat = sorted(encoder.heartbeats, key=lambda hb: hb.created_on, reverse=True)[0]
        return last_heartbeat

    @staticmethod
    def create(hostname: str, message: str, is_reachable: bool) -> HeartBeat:
        encoder = EncoderDAO.get_by_hostname(hostname)
        heartbeat = HeartBeat(message=message, is_reachable=is_reachable)
        db.session.add(heartbeat)
        encoder.heartbeats.append(heartbeat)
        db.session.commit()
        return heartbeat
