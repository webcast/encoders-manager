import logging
from datetime import datetime

from flask_jwt_extended import create_access_token, decode_token

from app.extensions import db
from app.models.token_blacklist import ApiToken, ApiTokenResources

logger = logging.getLogger("webapp.daos")


class ApiTokenException(Exception):
    pass


def add_token_to_database(name, encoded_token, resource) -> ApiToken:
    """
    Adds a new token to the database. It is not revoked when it is added.
    :param identity_claim:
    """
    logger.debug("Adding token to database")
    decoded_token = decode_token(encoded_token)
    jti = decoded_token["jti"]
    token_type = decoded_token["type"]
    user_identity = decoded_token["sub"]
    revoked = False
    logger.debug(f"Resource is {resource} {type(resource)}")
    db_token = ApiToken(
        name=name,
        access_token=encoded_token,
        jti=jti,
        token_type=token_type,
        user_identity=user_identity,
        revoked=revoked,
        resource=ApiTokenResources(resource),
    )
    db.session.add(db_token)
    db.session.commit()
    return db_token


class ApiTokenDAO:
    @staticmethod
    def get_by_name(token_name: str) -> ApiToken:
        query = db.select(ApiToken).filter_by(name=token_name)
        token = db.session.execute(query).scalar_one_or_none()
        return token

    @staticmethod
    def get_by_jti(jti: str) -> ApiToken:
        query = db.select(ApiToken).filter_by(jti=jti)
        token = db.session.execute(query).scalar_one_or_none()
        return token

    @staticmethod
    def update_last_used(token_id):
        query = db.select(ApiToken).filter_by(id=token_id)
        token = db.session.execute(query).scalar_one()
        if token:
            token.last_used = datetime.now()
            db.session.commit()
        return token

    @staticmethod
    def create(info: dict):
        logger.debug("DAO: Creating API token")
        token_name = info["tokenName"]
        token = ApiTokenDAO.get_by_name(token_name)
        if token:
            raise ApiTokenException(f"The token with name {token_name} already exists")
        else:
            logger.info(f"Creating Token with name {token_name}")
            access_token = create_access_token(identity=token_name)
            db_token = add_token_to_database(token_name, access_token, info["scope"])
            return db_token

    @staticmethod
    def delete_expired_tokens():
        now = datetime.now()
        query = db.select(ApiToken).filter(ApiToken.expires < now)
        expired_tokens = db.session.execute(query).scalars().all()

        for token in expired_tokens:
            db.session.delete(token)
        db.session.commit()
