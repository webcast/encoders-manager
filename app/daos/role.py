from typing import List

from app.extensions import db
from app.models.roles import Role


class RoleDAO:
    @staticmethod
    def get_all() -> List[Role]:
        roles = db.session.execute(db.select(Role)).scalars().all()
        return roles

    @staticmethod
    def get_user_app_roles(user_roles: List[str]) -> List[Role]:
        roles = db.session.execute(db.select(Role).filter(Role.name.in_(user_roles))).scalars().all()
        return roles
