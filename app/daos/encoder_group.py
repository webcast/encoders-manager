import logging
from typing import List

from app.extensions import db
from app.models.encoder_groups import EncoderGroup

logger = logging.getLogger("webapp.encoder_group_dao")


class EncoderGroupDAOException(Exception):
    pass


class EncoderGroupDAO:
    @staticmethod
    def get_by_id(id: int) -> EncoderGroup:
        encoder_object = db.session.get(EncoderGroup, id)
        if not encoder_object:
            raise EncoderGroupDAOException(f"Encoder Group {id} not found")
        return encoder_object

    @staticmethod
    def get_by_room_id(room_id: int) -> List[EncoderGroup]:
        groups = db.session.execute(db.select(EncoderGroup).filter_by(room_id=room_id)).scalars().all()
        return groups

    @staticmethod
    def get_all() -> List[EncoderGroup]:
        groups = db.session.execute(db.select(EncoderGroup)).scalars().all()
        return groups

    @staticmethod
    def get_all_running() -> List[EncoderGroup]:
        groups = db.session.execute(db.select(EncoderGroup).filter_by(is_test_running=True)).scalars().all()

        return groups

    @staticmethod
    def get_all_not_running() -> List[EncoderGroup]:
        groups = db.session.execute(db.select(EncoderGroup).filter_by(is_test_running=False)).scalars().all()

        return groups
