import logging
from datetime import datetime, timedelta
from typing import List, Optional

from sqlalchemy import desc, func

from app.extensions import db
from app.models.indico_events import EventType, IndicoEvent
from app.models.rooms import Room

logger = logging.getLogger("webapp.stats_dao")


class StatsDAO:
    @staticmethod
    def get_recording_count_between_dates(from_date: str, to_date: Optional[str] = None) -> int:
        if not to_date:
            to_date_str = datetime.strptime(from_date, "%Y-%m-%d") + timedelta(days=1)
        else:
            to_date_str = datetime.strptime(to_date, "%Y-%m-%d")

        query = (
            db.select(func.count(IndicoEvent.id))
            .filter(
                IndicoEvent.start_datetime > from_date,
                IndicoEvent.start_datetime < to_date_str,
            )
            .filter_by(is_recorded=True)
            .filter_by(event_type=EventType.RECORDING)
        )
        events = db.session.execute(query).scalar()
        return events

    @staticmethod
    def get_stream_count_between_dates(from_date: str, to_date: Optional[str] = None) -> int:
        if not to_date:
            to_date_str = datetime.strptime(from_date, "%Y-%m-%d") + timedelta(days=1)
        else:
            to_date_str = datetime.strptime(to_date, "%Y-%m-%d")
        query = (
            db.select(func.count(IndicoEvent.id))
            .filter(
                IndicoEvent.start_datetime > from_date,
                IndicoEvent.start_datetime < to_date_str,
            )
            .filter_by(is_streamed=True)
            .filter_by(event_type=EventType.STREAM)
        )
        events = db.session.execute(query).scalar()
        return events

    @staticmethod
    def get_stream_recording_count_between_dates(from_date: str, to_date: Optional[str] = None) -> int:
        if not to_date:
            to_date_str = datetime.strptime(from_date, "%Y-%m-%d") + timedelta(days=1)
        else:
            to_date_str = datetime.strptime(to_date, "%Y-%m-%d")
        query = (
            db.select(func.count(IndicoEvent.id))
            .filter(
                IndicoEvent.start_datetime > from_date,
                IndicoEvent.start_datetime < to_date_str,
            )
            .filter_by(is_streamed=True)
            .filter_by(is_recorded=True)
            .filter_by(event_type=EventType.STREAM_RECORDING)
        )
        events = db.session.execute(query).scalar()
        return events

    @staticmethod
    def get_recordings_per_room_between_dates(from_date: str, to_date: Optional[str] = None) -> List[dict[str, int]]:
        if not to_date:
            to_date_str = datetime.strptime(from_date, "%Y-%m-%d") + timedelta(days=1)
        else:
            to_date_str = datetime.strptime(to_date, "%Y-%m-%d")
        query = (
            db.select(
                Room.indico_name.label("room_name"),
                func.count(IndicoEvent.id).label("events"),
            )
            .join(IndicoEvent, Room.id == IndicoEvent.room_id)
            .filter(
                IndicoEvent.start_datetime > from_date,
                IndicoEvent.start_datetime < to_date_str,
            )
            .filter_by(is_recorded=True)
            .filter_by(event_type=EventType.RECORDING)
            .order_by(desc("events"))
            .group_by(Room.indico_name)
        )
        results = db.session.execute(query).all()
        formatted_results = [{row.room_name: row.events} for row in results]
        logger.debug(formatted_results)
        return formatted_results

    @staticmethod
    def get_streams_per_room_between_dates(from_date: str, to_date: Optional[str] = None) -> List[dict[str, int]]:
        if not to_date:
            to_date_str = datetime.strptime(from_date, "%Y-%m-%d") + timedelta(days=1)
        else:
            to_date_str = datetime.strptime(to_date, "%Y-%m-%d")
        query = (
            db.select(
                Room.indico_name.label("room_name"),
                func.count(IndicoEvent.id).label("events"),
            )
            .join(IndicoEvent, Room.id == IndicoEvent.room_id)
            .filter(
                IndicoEvent.start_datetime > from_date,
                IndicoEvent.start_datetime < to_date_str,
            )
            .filter_by(is_streamed=True)
            .filter_by(event_type=EventType.STREAM)
            .order_by(desc("events"))
            .group_by(Room.indico_name)
        )
        results = db.session.execute(query).all()
        formatted_results = [{row.room_name: row.events} for row in results]
        return formatted_results

    @staticmethod
    def get_stream_recordings_per_room_between_dates(
        from_date: str, to_date: Optional[str] = None
    ) -> List[dict[str, int]]:
        if not to_date:
            to_date_str = datetime.strptime(from_date, "%Y-%m-%d") + timedelta(days=1)
        else:
            to_date_str = datetime.strptime(to_date, "%Y-%m-%d")
        query = (
            db.select(
                Room.indico_name.label("room_name"),
                func.count(IndicoEvent.id).label("events"),
            )
            .join(IndicoEvent, Room.id == IndicoEvent.room_id)
            .filter(
                IndicoEvent.start_datetime > from_date,
                IndicoEvent.start_datetime < to_date_str,
            )
            .filter_by(is_streamed=True)
            .filter_by(is_recorded=True)
            .filter_by(event_type=EventType.STREAM_RECORDING)
            .order_by(desc("events"))
            .group_by(Room.indico_name)
        )
        results = db.session.execute(query).all()
        formatted_results = [{row.room_name: row.events} for row in results]
        return formatted_results
