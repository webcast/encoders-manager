import logging

from app.extensions import db
from app.models.settings import Settings

logger = logging.getLogger("webapp.daos_settings")


class SettingsDAOException(Exception):
    pass


class SettingsDAO:
    @staticmethod
    def create():
        settings = db.session.execute(db.select(Settings).filter_by(id=1)).scalar_one_or_none()

        if not settings:
            logger.info("No settings object found in the DB. Creating one...")
            settings = Settings()
            db.session.add(settings)
            db.session.commit()
        return settings

    @staticmethod
    def get_all() -> Settings:
        """

        :return: The application settings
        :rtype: app.models.settings.Settings
        """
        settings = db.session.execute(db.select(Settings).filter_by(id=1)).scalar_one_or_none()
        return settings

    @staticmethod
    def update_precondition(info):
        pass
        # assert info.get('enable_ravem_notifications', None)
        # assert info.get('enable_webcast_website_notifications', None)

    @staticmethod
    def update(details) -> Settings:
        logger.info("Updating settings...")
        SettingsDAO.update_precondition(details)

        settings = SettingsDAO.get_all()

        if not settings:
            logger.info("Settings not set. Creating a new Settings object")
            settings = Settings()

        enable_ravem_notifications = details["enable_ravem_notifications"]
        enable_webcast_website_notifications = details["enable_webcast_website_notifications"]
        enable_ces_notifications = details["enable_ces_notifications"]

        settings.enable_ravem_notifications = enable_ravem_notifications
        settings.enable_webcast_website_notifications = enable_webcast_website_notifications
        settings.enable_ces_notifications = enable_ces_notifications

        db.session.add(settings)
        db.session.commit()

        return settings
