import logging
from typing import List

from app.extensions import db
from app.models.base_encoder import Encoder

logger = logging.getLogger("webapp.encoders")


class EncoderDAOException(Exception):
    pass


class HeartBeatDAOException(Exception):
    pass


class EncoderDAO:
    @staticmethod
    def get_by_id(encoder_id: int) -> Encoder:
        encoder_object = db.session.get(Encoder, encoder_id)
        if not encoder_object:
            raise EncoderDAOException(f"Encoder {encoder_id} not found")
        return encoder_object

    @staticmethod
    def get_by_hostname(hostname: str) -> Encoder:
        encoder_object = db.session.execute(db.select(Encoder).filter_by(hostname=hostname)).scalar_one_or_none()
        if not encoder_object:
            raise EncoderDAOException(f"Encoder {hostname} not found")
        return encoder_object

    @staticmethod
    def get_type_by_hostname(hostname: str) -> str:
        encoder_object: Encoder = db.session.execute(
            db.select(Encoder).filter_by(hostname=hostname)
        ).scalar_one_or_none()
        encoder_type = encoder_object.encoder_type.value
        if not encoder_object:
            raise EncoderDAOException(f"Encoder {hostname} not found")
        return encoder_type

    @staticmethod
    def get_all() -> List[Encoder]:
        encoders = db.session.execute(db.select(Encoder).order_by(Encoder.hostname)).scalars().all()
        return encoders
