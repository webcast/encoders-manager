import logging
import os
import typing
from importlib import import_module

import sentry_sdk
import sqlalchemy
from celery import Celery, Task
from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_admin.menu import MenuLink
from flask_cors import CORS
from flask_login import LoginManager, current_user
from sentry_sdk.integrations.flask import FlaskIntegration
from werkzeug.middleware.proxy_fix import ProxyFix

from app.api_loader import load_api_private_blueprints, load_api_public_blueprints
from app.daos.settings import SettingsDAO
from app.extensions import cache, db, jwt, migrate
from app.models.encoder_groups import EncoderGroup
from app.models.encoders.epiphan_pearl2 import EpiphanPearl2
from app.models.encoders.epiphan_pearl_nano import EpiphanPearlNano
from app.models.encoders.monarch_hd import MonarchHD
from app.models.encoders.monarch_hdx import MonarchHDX
from app.models.heartbeats import HeartBeat
from app.models.indico_events import IndicoEvent
from app.models.roles import Role
from app.models.rooms import Room
from app.models.settings import Settings
from app.models.token_blacklist import ApiToken
from app.models.users import BackendUser, User
from app.services.encoders import encoders_cli, groups_cli
from app.services.indico import indico_cli
from app.utils import cli_utils
from app.utils.blacklist_helpers import check_if_token_revoked
from app.utils.celery_probes import LivenessProbe
from app.utils.cern_openid import load_cern_openid
from app.utils.logger import setup_logs
from app.views.admin.admin_encoder_form import EncoderModelView
from app.views.admin.admin_encoder_group_form import EncoderGroupModelView
from app.views.admin.admin_index_view import ProtectedAdminIndexView
from app.views.admin.admin_indico_event_form import IndicoEventModelView
from app.views.admin.admin_room_form import RoomModelView
from app.views.admin.api_token_detail_view import ApiTokenDetailView
from app.views.admin_protected_view import AdminProtectedView
from app.views.blueprints import all_blueprints

logger = logging.getLogger("webapp.app_factory")


def create_app(
    config_filename: str = "config/config.py",
    static_url_path: str = "/static-files",
):
    """
    Create the application from the configuration file passed as parameter
    :param config_object: The python module that will be used to populate
    the configuration
    :type config_object: Python module
    :return:
    """
    print("Creating Flask application...", end=" ")
    app = Flask(
        __name__,
        static_url_path=static_url_path,
    )
    app.config.from_pyfile(config_filename)
    print("ok")
    CORS(app)

    setup_logs(
        app,
        "webapp",
        to_file=app.config.get("LOG_FILE_ENABLED", False),
    )

    if app.config["USE_PROXY"]:
        app.wsgi_app = ProxyFix(  # type:ignore
            app.wsgi_app, x_for=1, x_proto=1, x_host=1
        )  # noqa

    setup_database(app)

    initialize_models(app)
    load_cern_openid(app)
    initialize_settings(app)
    init_jwt_auth(app)
    init_flask_admin(app)

    print("Initialize cli...", end="")
    # initialize cli, where we add additional commands to the CLI
    initialize_cli(app)
    print("ok")

    init_cache(app)

    load_private_api_v1(app)
    load_public_api_v1(app)
    initialize_views(app)
    celery_init_app(app)
    init_sentry(app)

    @app.context_processor
    def inject_template_variables():
        is_dev = app.config["IS_DEV"]
        return dict(is_dev=is_dev, current_user=current_user)

    return app


def init_cache(app):
    print("Initialize cache: ", end="")

    if app.config["CACHE_ENABLE"]:
        if app.config["CACHE_TYPE"] == "redis":
            print("Initializing Redis Caching... ", end="")
            cache_config = {
                "CACHE_TYPE": "redis",
                "CACHE_REDIS_HOST": app.config.get("CACHE_REDIS_HOST", None),
                "CACHE_REDIS_PASSWORD": app.config.get("CACHE_REDIS_PASSWORD", None),
                "CACHE_REDIS_PORT": app.config.get("CACHE_REDIS_PORT", None),
                "CACHE_DEFAULT_TIMEOUT": app.config["CACHE_DEFAULT_TIMEOUT"],
            }
        elif app.config["CACHE_TYPE"] == "SimpleCache":
            cache_config = {
                "CACHE_TYPE": "SimpleCache",
                "CACHE_DEFAULT_TIMEOUT": app.config["CACHE_DEFAULT_TIMEOUT"],
            }

        else:
            print("Caching disabled... ", end="")
            print("ok")
            return
        print(type(cache_config))
        cache.init_app(app, config=cache_config)
        print("ok")


def initialize_cli(app):
    app.cli.add_command(indico_cli.indico_cli)
    app.cli.add_command(encoders_cli.encoders_cli)
    app.cli.add_command(groups_cli.groups_cli)
    app.cli.add_command(cli_utils.cli)


def setup_database(app):
    """
    Initialize the database and the migrations
    """

    if app.config.get("DB_ENGINE", None) == "postgresql":
        print("Initializing Postgres Database... ", end="")
        user = app.config["DB_USER"]
        password = app.config["DB_PASS"]
        service_name = app.config["DB_SERVICE_NAME"]
        db_port = app.config["DB_PORT"]
        db_name = app.config["DB_NAME"]
        app.config["SQLALCHEMY_DATABASE_URI"] = (
            f"postgresql://{user}:{password}@{service_name}:{db_port}/{db_name}" "?options=-csearch_path=encoders"
        )
        app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {
            # Avoid annoying errors when the pods are restarted in Openshift
            "pool_pre_ping": True,
            # "pool_size": 5,
            # "pool_timeout": 10,
            # "pool_recycle": 120,
            # "max_overflow": 3,
        }

    elif app.config.get("DB_ENGINE", None) == "mysql":
        print("Initializing Mysql Database... ", end="")
        user = app.config["DB_USER"]
        password = app.config["DB_PASS"]
        service_name = app.config["DB_SERVICE_NAME"]
        db_port = app.config["DB_PORT"]
        db_name = app.config["DB_NAME"]
        app.config["SQLALCHEMY_DATABASE_URI"] = (
            f"mysql+pymysql://{user}:{password}@{service_name}:{db_port}/{db_name}" + "?charset=utf8mb4"
        )
        app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {
            # Avoid annoying errors when the pods are restarted in Openshift
            "pool_pre_ping": True,
            "pool_size": 5,
            "pool_timeout": 10,
            "pool_recycle": 120,
            "max_overflow": 3,
        }

    else:
        print("Initializing Sqlite Database...", end="")
        _basedir = os.path.abspath(os.path.dirname(__file__))
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(_basedir, "webapp.db")

    app.config["SQLALCHEMY_RECORD_QUERIES"] = False
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    app.db = db
    print("ok")

    print("Initializing Database Migrations... ", end="")
    migrate.init_app(app, db)
    print("ok")


def initialize_models(app):
    """
    Load all the available models
    """
    with app.app_context():
        print("Loading models... ", end="")
        application_models = [
            "app.models.settings",
            "app.models.rooms",
            "app.models.indico_events",
            "app.models.base_encoder",
            "app.models.encoders.epiphan_pearl_nano",
            "app.models.encoders.monarch",
            "app.models.encoders.monarch_hd",
            "app.models.encoders.monarch_hdx",
            "app.models.encoder_groups",
            "app.models.heartbeats",
            "app.models.roles",
            "app.models.users",
            "app.models.token_blacklist",
        ]
        for module in application_models:
            import_module(module)
        print("ok")


def initialize_settings(app):
    with app.app_context():
        try:
            print("Creating the settings object if needed... ", end="")
            SettingsDAO.create()
            print("ok")
        except (sqlalchemy.exc.OperationalError, sqlalchemy.exc.ProgrammingError):
            print("error")
            logger.error("Unable to create the settings object. Please check the database connection.")


def init_flask_admin(app: Flask) -> None:
    """Initializes the flask admin extension.

    Args:
        application (Flask): The flask application to initialize the flask admin with.
    """
    print("Initializing Admin... ", end="")
    with app.app_context():
        app.config["FLASK_ADMIN_SWATCH"] = "flatly"
        admin = Admin(
            app,
            name="Encoders Manager Administration",
            index_view=ProtectedAdminIndexView(),
            template_mode="bootstrap4",
        )
        admin.add_link(MenuLink(name="Dashboard", url="/"))
        admin.add_link(MenuLink(name="Logout", url="/logout/"))
        admin.add_view(RoomModelView(Room, db.session))
        admin.add_view(IndicoEventModelView(IndicoEvent, db.session))
        admin.add_view(EncoderModelView(MonarchHD, db.session, category="Encoders"))
        admin.add_view(EncoderModelView(MonarchHDX, db.session, category="Encoders"))
        admin.add_view(ModelView(HeartBeat, db.session, category="Encoders"))
        admin.add_view(EncoderModelView(EpiphanPearlNano, db.session, category="Encoders"))
        admin.add_view(EncoderModelView(EpiphanPearl2, db.session, category="Encoders"))
        admin.add_view(EncoderGroupModelView(EncoderGroup, db.session, name="Encoder Groups"))
        admin.add_view(AdminProtectedView(Role, db.session))
        admin.add_view(AdminProtectedView(Settings, db.session))
        admin.add_view(ApiTokenDetailView(ApiToken, db.session))
        admin.add_view(AdminProtectedView(User, db.session, category="Users"))
        admin.add_view(AdminProtectedView(BackendUser, db.session, category="Users"))

        # setup login manager
        login_manager = LoginManager()
        login_manager.login_view = "cern_openid.login"  # type: ignore

    print("ok")


def load_private_api_v1(application: Flask) -> None:
    """Loads the private API v1.
    Args:
        application (Flask): The application to load the API into.
    """
    api_blueprints = load_api_private_blueprints()
    prefix = "/api"
    version = "v1"

    for blueprint in api_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/private/{version}",
        )


def load_public_api_v1(application: Flask) -> None:
    """Loads the private API v1.
    Args:
        application (Flask): The application to load the API into.
    """
    api_blueprints = load_api_public_blueprints()
    prefix = "/api"
    version = "v1"

    for blueprint in api_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/public/{version}",
        )


def initialize_views(application: Flask) -> None:
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """
    print("initiliaze views")

    for blueprint in all_blueprints:
        import_module(blueprint.import_name)
        application.register_blueprint(blueprint)


def celery_init_app(app: Flask) -> Celery:
    """
    Initialize the Celery tasks support.

    :param app: The current Flask application. If it's not set,
    it will be created using the config parameter
    :param config: Configuration object for the Flask application
    :return: The Celery instance
    """

    class FlaskTask(Task):
        def __call__(self, *args: object, **kwargs: object) -> object:
            with app.app_context():
                return self.run(*args, **kwargs)

    tasks_modules = [
        "app.tasks.example",
        "app.tasks.reminders",
        "app.tasks.encoders",
        "app.tasks.auto_stop",
        "app.tasks.indico_events",
    ]

    celery_app = Celery(app.name, task_cls=FlaskTask)
    celery_app.conf.broker_url = app.config["CELERY_BROKER_URL"]  # type: ignore
    celery_app.conf.result_backend = app.config["CELERY_RESULTS_BACKEND"]  # noqa type: ignore
    celery_app.conf.beat_schedule = app.config["CELERY_BEAT_SCHEDULE"]
    celery_app.conf.include = tasks_modules
    celery_app.conf.task_serializer = "json"
    celery_app.conf.result_serializer = "json"

    celery_app.conf.broker_connection_retry_on_startup = True
    celery_app.conf.timezone = "Europe/Zurich"

    print("Adding Celery Liveness Probe... ", end="")
    celery_app.steps["worker"].add(LivenessProbe)
    celery_app.set_default()
    app.extensions["celery"] = celery_app
    return celery_app


@typing.no_type_check
def init_sentry(application: Flask) -> None:
    if application.config["ENABLE_SENTRY"]:
        print("Initializing Sentry... ", end="")
        sentry_sdk.init(
            dsn=application.config["SENTRY_DSN"],
            integrations=[FlaskIntegration()],
            environment=application.config["SENTRY_ENVIRONMENT"],
        )
        print("OK")


def init_jwt_auth(application: Flask) -> None:
    """
    Initializes the JWT Authentication and sets the global configuration
    for the application JWT
    :param app: Application that will be set up
    :return: -
    """
    print("Initializing JWT Authentication... ", end="")
    application.config["JWT_ACCESS_TOKEN_EXPIRES"] = False
    application.config["JWT_BLACKLIST_ENABLED"] = True
    application.config["JWT_BLACKLIST_TOKEN_CHECKS"] = ["access", "refresh"]

    jwt.init_app(application)

    jwt.token_in_blocklist_loader(check_if_token_revoked)
    print("ok")
