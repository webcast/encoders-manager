from datetime import datetime

from sqlalchemy.orm import Mapped, mapped_column

from app.extensions import db
from app.models.base import ModelBase


class HeartBeat(ModelBase):
    """
    Represents a heartbeat message sent by an encoder.

    Attributes:
        message (str): The message sent by the encoder.
        is_reachable (bool): Whether the encoder is reachable or not.
        created_on (datetime): The date and time when the heartbeat was created.
        encoder_id (int): The ID of the encoder that sent the heartbeat.
    """

    message: Mapped[str] = mapped_column(db.Text, nullable=False)
    is_reachable: Mapped[bool] = mapped_column(default=False, nullable=True)
    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)

    encoder_id: Mapped[int] = mapped_column(db.ForeignKey("encoder.id"))

    def __str__(self):
        return f"{self.created_on} - is_reachable: {self.is_reachable} - {self.message}"
