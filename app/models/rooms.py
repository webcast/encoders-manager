from datetime import datetime
from typing import TYPE_CHECKING, List

from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.extensions import db
from app.models.base import ModelBase

if TYPE_CHECKING:
    from app.models.encoder_groups import EncoderGroup
    from app.models.indico_events import IndicoEvent

external_uri_room = "External URI"


class Room(ModelBase):
    """
    Represents a room where an event can be streamed.

    Attributes:
        indico_name (str): The name of the room in Indico.
        room_full_name (str): The full name of the room.
        supported (bool): Whether the room is supported by the system.
        created_on (datetime): The date and time when the room was created.
        updated_on (datetime): The date and time when the room was last updated.
        indico_events (List[IndicoEvent]): The list of Indico events associated with
        the room.
        encoder_groups (List[EncoderGroup]): The list of encoder groups associated with
        the room.
        role_id (int): The ID of the role associated with the room.
    """

    indico_name: Mapped[str] = mapped_column(String(255), unique=True, nullable=False)
    room_full_name: Mapped[str] = mapped_column(String(255), nullable=True)
    supported: Mapped[bool] = mapped_column(default=False, nullable=True)

    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)
    updated_on: Mapped[datetime] = mapped_column(default=db.func.now(), onupdate=db.func.now(), nullable=True)

    indico_events: Mapped[List["IndicoEvent"]] = relationship("IndicoEvent", backref="room", lazy=True)

    encoder_groups: Mapped[List["EncoderGroup"]] = relationship("EncoderGroup", backref="room", lazy=True)

    role_id: Mapped[int] = mapped_column(db.ForeignKey("role.id"), nullable=True)

    def __str__(self):
        room_full_name = f" - {self.room_full_name}" if self.room_full_name else ""
        return f"{self.indico_name}{room_full_name}"

    @staticmethod
    def get_or_create(room_name, room_full_name=None):
        if not room_name:
            room_object = Room.create_external_room()
        else:
            room_object = Room.create_normal_room(room_name, room_full_name)
        return room_object

    @staticmethod
    def create_normal_room(room_name, room_full_name=None):
        room_object = db.session.execute(db.select(Room).filter_by(indico_name=room_name)).scalar_one_or_none()
        if not room_object:
            room_object = Room(indico_name=room_name)
            if room_full_name:
                room_object.room_full_name = room_full_name
            db.session.add(room_object)
            db.session.commit()
        return room_object

    @staticmethod
    def create_external_room():
        room_object = db.session.execute(db.select(Room).filter_by(indico_name=external_uri_room)).scalar_one_or_none()
        if not room_object:
            room_object = Room(indico_name=external_uri_room)
            db.session.add(room_object)
            db.session.commit()
        return room_object
