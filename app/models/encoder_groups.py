import logging
from datetime import datetime
from threading import Lock
from typing import Any, Dict, List, Optional

from flask import current_app
from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.daos.encoders import EncoderDAO
from app.daos.indico_events import IndicoEventsDAO
from app.extensions import db
from app.models.base import ModelBase
from app.models.base_encoder import (
    Encoder,
    EncoderStatus,
    EncoderType,
    association_table,
)
from app.models.indico_events import EventType, IndicoEvent
from app.services.ces_notifications.ces import CesService
from app.services.ravem_notifications.ravem import RavemService
from app.services.webcast_website_notifications.webcast import WebcastWebsiteService
from app.utils.flask_threads import FlaskThread

logger = logging.getLogger("webapp.encoder_groups")


class EncoderGroup(ModelBase):
    name: Mapped[str] = mapped_column(String(255), nullable=False)
    is_test_running: Mapped[bool] = mapped_column(default=False, nullable=True)
    is_running: Mapped[bool] = mapped_column(default=False, nullable=True)
    recording_path: Mapped[str] = mapped_column(String(255), nullable=True)
    start_as_composite: Mapped[bool] = mapped_column(default=False, nullable=True)

    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)

    encoders: Mapped[List["Encoder"]] = relationship(secondary=association_table, back_populates="encoder_groups")

    room_id: Mapped[int] = mapped_column(db.ForeignKey("room.id"), nullable=True)

    indico_event_id: Mapped[int] = mapped_column(
        db.ForeignKey(
            "indico_event.id",
            use_alter=True,
            name="fk_encoder_group_indico_event_id",
        ),
        nullable=True,
    )
    # one-to-one relationship
    # set post_update=True to avoid circular dependency during
    indico_event: Mapped["IndicoEvent"] = relationship("IndicoEvent", foreign_keys=indico_event_id, post_update=True)

    CAMERA_SIGNAL = "CAMERA"
    SLIDES_SIGNAL = "SLIDES"

    @property
    def is_stream_running(self):
        # Check if any encoder is streaming
        return any([encoder.stream_running for encoder in self.encoders])

    @property
    def is_recording_running(self):
        # Check if any encoder is recording
        return any([encoder.recording_running for encoder in self.encoders])

    def __str__(self):
        return f"{self.name} " f"({', '.join([encoder.hostname for encoder in self.encoders])})"

    def __repr__(self):
        return f"<EncoderGroup {self.name} " f"({', '.join([encoder.hostname for encoder in self.encoders])})>"

    def _process_encoder_with_context(self, encoder, operation, results_dict, lock, **kwargs):
        """
        Helper method to process a single encoder operation with Flask context.

        Args:
            encoder: The encoder to process
            operation: The operation function to call
            results_dict: Shared dictionary to store results
            lock: Threading lock for safe dict access
            **kwargs: Additional arguments to pass to the operation
        """
        with current_app.app_context():
            result = operation(encoder, **kwargs)
            with lock:
                results_dict[encoder.hostname] = result

    def _process_encoders_parallel(self, operation, **kwargs):
        """
        Process encoders in parallel using FlaskThread.

        Args:
            operation: The operation function to call on each encoder
            **kwargs: Additional arguments to pass to the operation

        Returns:
            dict: Results keyed by encoder hostname
        """
        threads = []
        results_dict = {}
        lock = Lock()

        # Create and start threads for each encoder
        for encoder in self.encoders:
            thread = FlaskThread(
                target=self._process_encoder_with_context,
                args=(encoder, operation, results_dict, lock),
                kwargs=kwargs,
            )
            thread.start()
            threads.append(thread)

        # Wait for all threads to complete
        for thread in threads:
            thread.join()

        return results_dict

    def handle_operation_results(self, results):
        """
        Handle the results of an operation on a group of encoders.

        Args:
            target_function (function): The function that was run on the encoders.
            results (dict): A dictionary containing the results of the operation,
            keyed by encoder hostname.

        Returns:
            bool: True if there was an error during the operation, False otherwise.
        """
        error = False
        for encoder in self.encoders:
            try:
                encoder_result = results[encoder.hostname]

                if encoder_result["status"] != EncoderStatus.OK:
                    error = True

                if encoder_result["status"] == EncoderStatus.OK:
                    encoder_result["signal_type"] = encoder.signal_type.value
                    encoder_result["room_indico_name"] = self.room.indico_name

                encoder.create_heartbeat(
                    encoder_result["is_reachable"],
                    encoder_result["message"],
                )

            except (TypeError, KeyError):
                error = True

                encoder_result = {}
                encoder_result["status"] = EncoderStatus.ERROR
                encoder_result["signal_type"] = encoder.signal_type.value
                encoder_result["room_indico_name"] = self.room.indico_name
                encoder_result["is_reachable"] = False
                encoder_result["message"] = "Error during the operation."
                results[encoder.hostname] = encoder_result

        return error

    def get_status(self):
        """
        Returns the status of all encoders in the group.

        Parameters:
        None

        Returns:
        A dictionary containing the status of all encoders in the group.
        """

        def get_status_operation(encoder):
            return encoder.get_status()

        encoders_results = self._process_encoders_parallel(get_status_operation)
        operation_error = self.handle_operation_results(encoders_results)

        logger.debug(f"Operation error: {operation_error}")

        result = {
            "error": operation_error,
            "rollback_error": None,
            "result": encoders_results,
            "rollback": [],
        }

        encoders_running = []
        for encoder in self.encoders:
            encoder_running = encoder.recording_running or encoder.stream_running
            encoders_running.append(encoder_running)

        if any(encoders_running):
            self.is_running = True
        else:
            self.is_running = False

        if self.indico_event:
            self.indico_event.encoder_group = None
            self.indico_event = None

        db.session.commit()

        return result

    def start_test(self):
        """
        Starts the stream and sets the is_test_running flag to True if
        any of the encoders are running.

        Parameters:
        None

        Returns:
        result (dict): A dictionary containing the results of
        the process_encoders method.
        """

        def start_test_operation(encoder):
            return encoder.start_test()

        logger.debug("Starting test")

        encoders_results = self._process_encoders_parallel(start_test_operation)
        operation_error = self.handle_operation_results(encoders_results)

        logger.debug(f"Operation error: {operation_error}")

        # If there was an error, stop test on all encoders
        if operation_error:

            def stop_test_operation(encoder):
                return encoder.stop_test()

            rollback_results = self._process_encoders_parallel(stop_test_operation)
            rollback_error = self.handle_operation_results(rollback_results)
            logger.debug(f"Rollback error: {rollback_error}")
        else:
            rollback_error = False
            rollback_results = {}

        result = {
            "error": operation_error,
            "rollback_error": rollback_error,
            "result": encoders_results,
            "rollback": rollback_results,
        }

        encoders_running = []
        for encoder in self.encoders:
            encoder_running = encoder.recording_running or encoder.stream_running
            encoders_running.append(encoder_running)

        if any(encoders_running):
            self.is_test_running = True
        else:
            self.is_test_running = False
        db.session.commit()
        return result

    def stop_test(self):
        """
        Stop the test by stopping all encoders in the group.

        Parameters:
        None

        Returns:
        result (any): The result of the process_encoders function.
        """

        def stop_test_operation(encoder):
            return encoder.stop_test(
                save_heartbeat=False,
            )

        encoders_results = self._process_encoders_parallel(stop_test_operation)
        operation_error = self.handle_operation_results(encoders_results)

        result = {
            "error": operation_error,
            "rollback_error": False,
            "result": encoders_results,
            "rollback": {},
        }

        encoders_running = []
        for encoder in self.encoders:
            encoder_running = encoder.recording_running or encoder.stream_running
            encoders_running.append(encoder_running)

        if any(encoders_running):
            self.is_test_running = True
        else:
            self.is_test_running = False
        db.session.commit()
        return result

    def start_stream(self, indico_id: str, is_restart: bool = False, base_group_path: str = None):
        """
        Starts a stream for all encoders in the group.

        Args:
            indico_id (str): The name of the stream to start.
            is_restart (bool, optional): Whether the stream is being restarted.
            Defaults to False.

        Returns:
            dict: A dictionary containing the result of the process_encoders method.
        """

        def start_stream_operation(encoder):
            return encoder.start_stream(
                indico_id,
                is_restart=is_restart,
                save_heartbeat=False,
                base_group_path=self.recording_path,
                start_as_composite=self.start_as_composite,
            )

        encoders_results = self._process_encoders_parallel(start_stream_operation)

        operation_error = self.handle_operation_results(encoders_results)

        if operation_error:

            def stop_stream_operation(encoder):
                return encoder.stop_stream(
                    save_heartbeat=False,
                    start_as_composite=self.start_as_composite,
                )

            rollback_results = self._process_encoders_parallel(stop_stream_operation)

            rollback_error = self.handle_operation_results(rollback_results)
            logger.debug(f"Rollback error: {rollback_error}")
        else:
            rollback_error = False
            rollback_results = {}

        result = {
            "error": operation_error,
            "rollback_error": rollback_error,
            "result": encoders_results,
            "rollback": rollback_results,
        }

        encoders_running = []
        for encoder in self.encoders:
            encoder_running = encoder.recording_running or encoder.stream_running
            encoders_running.append(encoder_running)

        if any(encoders_running):
            self.is_running = True
        else:
            self.is_running = False

        if "error" in result and not result["error"] or "error" not in result:
            indico_event = IndicoEventsDAO.get_by_indico_id(indico_id)
            if indico_event:
                indico_event.encoder_group = self
                self.indico_event = indico_event
                self.indico_event.stream_start_datetime = db.func.now()
                self.indico_event.recording_start_datetime = db.func.now()
                self._send_notifications(recording=True, streaming=True)

        db.session.commit()

        return result

    def stop_stream(
        self,
    ):
        """
        Stop the stream for all encoders in the group.

        :return: A dictionary with the result of the operation.
        """

        def stop_stream_operation(encoder):
            return encoder.stop_stream(
                save_heartbeat=False,
                start_as_composite=self.start_as_composite,
            )

        encoders_results = self._process_encoders_parallel(stop_stream_operation)

        operation_error = self.handle_operation_results(encoders_results)

        result = {
            "error": operation_error,
            "rollback_error": False,
            "result": encoders_results,
            "rollback": {},
        }

        encoders_running = []
        for encoder in self.encoders:
            encoder_running = encoder.recording_running or encoder.stream_running
            encoders_running.append(encoder_running)

        if any(encoders_running):
            self.is_running = True
        else:
            self.is_running = False

        if "error" in result and not result["error"] or "error" not in result:
            if not self.is_recording_running:
                if self.indico_event:
                    self.indico_event.is_streamed = True
                    self.indico_event.stream_stop_datetime = db.func.now()
                    self._send_notifications(recording=False, streaming=False)

                self.indico_event.encoder_group = None
                self.indico_event = None

        db.session.commit()

        return result

    def start_recording(self, indico_id: str, is_restart: bool = False):
        """
        Starts recording for all encoders in the group for the given stream name.

        Args:
            indico_id (str): The name of the stream to start recording.
            is_restart (bool, optional): Whether the recording is a restart.
            Defaults to False.

        Returns:
            dict: A dictionary containing the result of the process_encoders method.
        """

        def start_recording_operation(encoder):
            if is_restart:
                return encoder.start_recording(
                    indico_id,
                    save_heartbeat=False,
                    base_group_path=self.recording_path,
                    start_as_composite=self.start_as_composite,
                )
            else:
                return encoder.start_all(
                    indico_id,
                    only_recording=True,
                    save_heartbeat=False,
                    is_restart=is_restart,
                    base_group_path=self.recording_path,
                    start_as_composite=self.start_as_composite,
                )

        # Start recording on all encoders in parallel
        encoders_results = self._process_encoders_parallel(start_recording_operation)
        operation_error = self.handle_operation_results(encoders_results)

        # If there was an error, stop recording on all encoders
        if operation_error:

            def stop_recording_operation(encoder):
                if is_restart:
                    return encoder.stop_recording(
                        save_heartbeat=False,
                        start_as_composite=self.start_as_composite,
                    )
                else:
                    return encoder.stop_all(
                        save_heartbeat=False,
                        start_as_composite=self.start_as_composite,
                    )

            rollback_results = self._process_encoders_parallel(stop_recording_operation)
            rollback_error = self.handle_operation_results(rollback_results)
            logger.debug(f"Rollback error: {rollback_error}")
        else:
            rollback_error = False
            rollback_results = {}

        result = {
            "error": operation_error,
            "rollback_error": rollback_error,
            "result": encoders_results,
            "rollback": rollback_results,
        }

        encoders_running = []
        for encoder in self.encoders:
            encoder_running = encoder.recording_running or encoder.stream_running
            encoders_running.append(encoder_running)

        if any(encoders_running):
            self.is_running = True
        else:
            self.is_running = False

        if "error" in result and not result["error"] or "error" not in result:
            indico_event = IndicoEventsDAO.get_by_indico_id(indico_id)
            if indico_event:
                indico_event.encoder_group = self
                self.indico_event = indico_event
                self.indico_event.recording_start_datetime = db.func.now()
                self._send_notifications(recording=True, streaming=False)

        db.session.commit()

        return result

    def stop_recording(self):
        """
        Stops recording on all encoders in the group and updates the Indico
        event attributes
        with the recording and streaming information. Also sends notifications
        to Ravem and CES
        services if the recording was successful.

        Returns:
        dict: A dictionary containing the result of the `process_encoders` method.
        """

        def stop_recording_operation(encoder):
            return encoder.stop_recording(
                save_heartbeat=False,
                start_as_composite=self.start_as_composite,
            )

        encoders_results = self._process_encoders_parallel(stop_recording_operation)
        operation_error = self.handle_operation_results(encoders_results)

        result = {
            "error": operation_error,
            "rollback_error": False,
            "result": encoders_results,
            "rollback": {},
        }

        # Check if any encoders are still running
        encoders_running = []
        for encoder in self.encoders:
            encoder_running = encoder.recording_running or encoder.stream_running
            encoders_running.append(encoder_running)

        # Update the `is_running` attribute based on whether any
        # encoders are still running
        if any(encoders_running):
            self.is_running = True
        else:
            self.is_running = False

        # Update the Indico event attributes if the recording was successful
        if "error" in result and not result["error"] or "error" not in result:
            if not self.is_stream_running:
                if self.indico_event:
                    self.indico_event.is_recorded = True
                    self.indico_event.is_streamed = True
                    self.indico_event.recording_stop_datetime = db.func.now()
                    self.indico_event.stream_stop_datetime = db.func.now()
                    self.indico_event.recording_path = self.recording_path

                    # Get the recording_path from the encoder in the result dict
                    # where the signal_type is CAMERA
                    has_epiphan_encoders = []
                    for hostname, encoder_result in result["result"].items():
                        if encoder_result["signal_type"] == self.CAMERA_SIGNAL:
                            self.indico_event.recording_camera_path = encoder_result["recording_path"]
                            self.indico_event.encoder_camera = hostname
                            self.indico_event.stream_camera_name = encoder_result["stream_name"]
                            camera_type = EncoderDAO.get_type_by_hostname(hostname)
                            self.indico_event.encoder_camera_type = camera_type
                            if (
                                camera_type == EncoderType.EPIPHAN_PEARL_NANO.value
                                or camera_type == EncoderType.EPIPHAN_PEARL2.value
                            ):
                                has_epiphan_encoders.append(True)
                        if encoder_result["signal_type"] == self.SLIDES_SIGNAL:
                            self.indico_event.recording_slides_path = encoder_result["recording_path"]
                            self.indico_event.encoder_slides = hostname
                            self.indico_event.stream_slides_name = encoder_result["stream_name"]
                            slides_type = EncoderDAO.get_type_by_hostname(hostname)
                            self.indico_event.encoder_slides_type = slides_type
                            if (
                                slides_type == EncoderType.EPIPHAN_PEARL_NANO.value
                                or slides_type == EncoderType.EPIPHAN_PEARL2.value
                            ):
                                has_epiphan_encoders.append(True)

                    self.logger.debug("has_epiphan_encoders")
                    self.logger.debug(has_epiphan_encoders)
                    if any(has_epiphan_encoders):
                        self.indico_event.recording_path = current_app.config["EPIPHAN_NANO_RECORDING_PATH"]

                    # Send notifications to Ravem and CES services
                    self._send_notifications(recording=True, streaming=False)

                self.indico_event.encoder_group = None
                self.indico_event = None

        db.session.commit()

        return result

    def start_all(self, indico_id: str):
        """Start both recording and streaming for all encoders."""

        def start_operation(encoder):
            return encoder.start_all(
                indico_id,
                base_group_path=self.recording_path,
                save_heartbeat=False,
                start_as_composite=self.start_as_composite,
            )

        def rollback_operation(encoder):
            return encoder.stop_all(
                save_heartbeat=False,
                start_as_composite=self.start_as_composite,
            )

        result = self._execute_encoder_operation(start_operation, rollback_operation)

        if not result["error"]:
            self._update_running_status()
            self._update_indico_event(indico_id, recording=True, streaming=True)
            self._send_notifications(recording=True, streaming=True)

        db.session.commit()
        return result

    def stop_all(self):
        """Stop all encoders in the group"""

        def stop_all_operation(encoder):
            return encoder.stop_all(
                save_heartbeat=False,
                start_as_composite=self.start_as_composite,
            )

        encoders_results = self._process_encoders_parallel(stop_all_operation)
        operation_error = self.handle_operation_results(encoders_results)

        result = self._create_operation_result(operation_error, encoders_results)

        self._update_running_status()

        if not operation_error and self.indico_event:
            self.indico_event.is_recorded = True
            self.indico_event.is_streamed = True
            self.indico_event.recording_stop_datetime = db.func.now()
            self.indico_event.stream_stop_datetime = db.func.now()
            self.indico_event.recording_path = self.recording_path

            self._set_encoder_paths(result["result"])
            # Only notify the recording if the event should have been recorded
            # In case of streaming only, the event recording should not be notified to
            # Recordings Manager app
            self._send_notifications(
                recording=self.indico_event.event_type in [EventType.RECORDING, EventType.STREAM_RECORDING],
                streaming=True,
            )

            self.indico_event.encoder_group = None
            self.indico_event = None

        db.session.commit()
        return result

    def _send_notifications(self, recording=True, streaming=True):
        """
        Send notifications to various services based on the recording/streaming state.

        Args:
            recording (bool): Whether to send recording-related notifications
            streaming (bool): Whether to send streaming-related notifications
        """
        if not self.indico_event:
            return

        if streaming:
            webcast_service = WebcastWebsiteService(logger=logger)
            webcast_result = webcast_service.notify_stream(self.indico_event, self.encoders)
            if webcast_result:
                self.indico_event.webcast_website_notified = True

        if recording:
            ces_service = CesService()
            ces_result = ces_service.notify(self.indico_event.id)
            if ces_result:
                self.indico_event.ces_notified = True

        ravem_service = RavemService(self.indico_event.room.indico_name)
        if recording and streaming:
            ravem_result = ravem_service.notify_all()
        elif recording:
            ravem_result = ravem_service.notify_recording()
        elif streaming:
            ravem_result = ravem_service.notify_stream()
        else:
            ravem_result = ravem_service.notify(recording=False, webcast=False)

        if ravem_result:
            self.indico_event.ravem_notified = True

    def _set_encoder_paths(self, operation_results):
        """
        Set encoder paths and related attributes on the indico event based on
        operation results.

        Args:
            operation_results (dict): Results from encoder operations containing
            path information
        """
        has_epiphan_encoders = []

        for hostname, encoder_result in operation_results.items():
            if encoder_result["signal_type"] == self.CAMERA_SIGNAL:
                self._set_camera_attributes(hostname, encoder_result, has_epiphan_encoders)
            elif encoder_result["signal_type"] == self.SLIDES_SIGNAL:
                self._set_slides_attributes(hostname, encoder_result, has_epiphan_encoders)

        if any(has_epiphan_encoders):
            self.indico_event.recording_path = current_app.config["EPIPHAN_NANO_RECORDING_PATH"]

    def _set_camera_attributes(self, hostname, encoder_result, has_epiphan_encoders):
        """Set camera-related attributes on the indico event"""
        self.indico_event.recording_camera_path = encoder_result["recording_path"]
        self.indico_event.encoder_camera = hostname
        self.indico_event.stream_camera_name = encoder_result["stream_name"]
        camera_type = EncoderDAO.get_type_by_hostname(hostname)
        self.indico_event.encoder_camera_type = camera_type
        if camera_type in (
            EncoderType.EPIPHAN_PEARL_NANO.value,
            EncoderType.EPIPHAN_PEARL2.value,
        ):
            has_epiphan_encoders.append(True)

    def _set_slides_attributes(self, hostname, encoder_result, has_epiphan_encoders):
        """Set slides-related attributes on the indico event"""
        self.indico_event.recording_slides_path = encoder_result["recording_path"]
        self.indico_event.encoder_slides = hostname
        self.indico_event.stream_slides_name = encoder_result["stream_name"]
        slides_type = EncoderDAO.get_type_by_hostname(hostname)
        self.indico_event.encoder_slides_type = slides_type
        if slides_type in (
            EncoderType.EPIPHAN_PEARL_NANO.value,
            EncoderType.EPIPHAN_PEARL2.value,
        ):
            has_epiphan_encoders.append(True)

    def _update_running_status(self):
        """Update the running status of the encoder group based on encoder states"""
        encoders_running = [encoder.recording_running or encoder.stream_running for encoder in self.encoders]
        self.is_running = any(encoders_running)
        return encoders_running

    def _create_operation_result(
        self,
        operation_error: bool,
        encoders_results: Dict[str, Any],
        rollback_results: Optional[Dict[str, Any]] = None,
        rollback_error: bool = False,
    ) -> Dict[str, Any]:
        """Create standardized operation result dictionary."""
        return {
            "error": operation_error,
            "rollback_error": rollback_error,
            "result": encoders_results,
            "rollback": rollback_results or {},
        }

    def _execute_encoder_operation(self, operation_func, rollback_func=None):
        """
        Execute an operation on all encoders with rollback support.

        Args:
            operation_func: Function to execute on each encoder
            rollback_func: Optional rollback function if operation fails
        """
        encoders_results = self._process_encoders_parallel(operation_func)
        operation_error = self.handle_operation_results(encoders_results)

        if operation_error and rollback_func:
            rollback_results = self._process_encoders_parallel(rollback_func)
            rollback_error = self.handle_operation_results(rollback_results)
            logger.debug(f"Rollback error: {rollback_error}")
        else:
            rollback_error = False
            rollback_results = {}

        return self._create_operation_result(operation_error, encoders_results, rollback_results, rollback_error)

    def _update_indico_event(self, indico_id=None, recording=False, streaming=False):
        """
        Update indico event attributes and relationships.

        Args:
            indico_id: Optional ID to fetch new indico event
            recording: Whether recording attributes should be updated
            streaming: Whether streaming attributes should be updated
        """
        if indico_id:
            indico_event = IndicoEventsDAO.get_by_indico_id(indico_id)
            if indico_event:
                indico_event.encoder_group = self
                self.indico_event = indico_event

        if self.indico_event:
            if recording:
                self.indico_event.is_recorded = True
                self.indico_event.recording_start_datetime = db.func.now()
            if streaming:
                self.indico_event.is_streamed = True
                self.indico_event.stream_start_datetime = db.func.now()
