from datetime import datetime

from flask_dance.consumer.storage.sqla import OAuthConsumerMixin
from flask_login import UserMixin
from sqlalchemy import ForeignKey, String, event
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.orm.attributes import get_history

from app.extensions import db


class User(db.Model, UserMixin):  # type: ignore
    __tablename__ = "user"
    # __table_args__ = {"schema": "encoders"}

    id: Mapped[int] = mapped_column(db.Integer, unique=True, primary_key=True)
    first_name: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    username: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    last_name: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    email: Mapped[str] = mapped_column(String(255), unique=True, nullable=True)

    is_admin: Mapped[bool] = mapped_column(nullable=False, default=False)
    active: Mapped[bool] = mapped_column(default=True)
    revoked_on: Mapped[datetime] = mapped_column(nullable=True)

    confirmed_at: Mapped[datetime] = mapped_column(default=datetime.now, nullable=True)
    last_login: Mapped[datetime] = mapped_column(default=datetime.now, nullable=True)
    roles: Mapped[str] = mapped_column(String(255), nullable=True)


class BackendUser(db.Model, UserMixin):  # type: ignore
    __tablename__ = "backend_user"

    id: Mapped[int] = mapped_column(db.Integer, unique=True, primary_key=True)
    first_name: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    username: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    last_name: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    email: Mapped[str] = mapped_column(String(255), unique=True, nullable=True)
    is_admin: Mapped[bool] = mapped_column(String(255), nullable=False, default=False)

    department: Mapped[str] = mapped_column(String(255), nullable=True)
    group: Mapped[str] = mapped_column(String(255), nullable=True)
    section: Mapped[str] = mapped_column(String(255), nullable=True)
    organization: Mapped[str] = mapped_column(String(255), nullable=True)
    building: Mapped[str] = mapped_column(String(255), nullable=True)

    active: Mapped[bool] = mapped_column(default=True)
    confirmed_at: Mapped[datetime] = mapped_column(default=datetime.now, nullable=True)
    last_login: Mapped[datetime] = mapped_column(default=datetime.now)

    def __repr__(self):
        return f"<User {self.id}: {self.first_name} {self.last_name} {self.username} >"


class OAuth(db.Model, OAuthConsumerMixin):  # type: ignore
    """
    Represents an Oauth connection in the application
    """

    __tablename__ = "flask_dance_oauth"

    user_id: Mapped[int] = mapped_column(ForeignKey(BackendUser.id))
    user: Mapped["BackendUser"] = relationship(BackendUser)


@event.listens_for(db.session, "before_flush")
def receive_before_flush_user(session, flush_context, instances):
    "listen for the 'after_flush' event"
    # pylint: disable=unused-argument
    for instance in session.dirty:
        if not isinstance(instance, User):
            continue
        previous_value = get_history(instance, "active")[2]
        if len(previous_value) > 0:
            active_value = previous_value[0]
            if instance.active != active_value:
                if instance.active:
                    instance.revoked_on = datetime.now()
                else:
                    instance.revoked_on = None
