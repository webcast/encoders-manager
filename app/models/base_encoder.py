import enum
import logging
from abc import abstractmethod
from datetime import datetime
from typing import List, Optional, TypedDict, Union

from flask import current_app
from requests.exceptions import ConnectionError
from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.extensions import Base, db
from app.models.base import ModelBase
from app.models.heartbeats import HeartBeat

logger = logging.getLogger("webapp.models_base_encoder")

association_table = db.Table(
    "encoder_group_encoders",
    Base.metadata,
    db.Column("encoder", db.Integer, db.ForeignKey("encoder.id")),
    db.Column("encoder_group", db.ForeignKey("encoder_group.id")),
)


class EncoderStatus(enum.Enum):
    OK = "OK"
    ERROR = "ERROR"


class EncoderException(Exception):
    pass


class EncoderType(enum.Enum):
    MONARCH_HD = "MONARCH_HD"
    MONARCH_HDX = "MONARCH_HDX"
    EPIPHAN_PEARL_NANO = "EPIPHAN_PEARL_NANO"
    EPIPHAN_PEARL2 = "EPIPHAN_PEARL2"


class EncoderSignalType(enum.Enum):
    CAMERA = "CAMERA"
    SLIDES = "SLIDES"


class EncoderResponse(TypedDict):
    status: EncoderStatus
    message: str
    is_reachable: bool
    recording_path: Optional[str]
    stream_name: str


class Encoder(ModelBase):

    hostname: Mapped[str] = mapped_column(String(255), unique=True, nullable=False)
    stream_application: Mapped[str] = mapped_column(String(255), nullable=False)
    stream_name: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    recording_path: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )
    stream_server: Mapped[str] = mapped_column(String(255), nullable=False)

    recording_running: Mapped[bool] = mapped_column(default=False, nullable=True)
    stream_running: Mapped[bool] = mapped_column(default=False, nullable=True)

    recording_start_datetime: Mapped[datetime] = mapped_column(
        nullable=True,
    )
    recording_stop_datetime: Mapped[datetime] = mapped_column(
        nullable=True,
    )

    stream_start_datetime: Mapped[datetime] = mapped_column(
        nullable=True,
    )
    stream_stop_datetime: Mapped[datetime] = mapped_column(
        nullable=True,
    )

    encoder_type: Mapped[EncoderType] = mapped_column(nullable=False)
    signal_type: Mapped[EncoderSignalType] = mapped_column(nullable=False, default=EncoderSignalType.CAMERA)

    product_name: Mapped[str] = mapped_column(String(255), nullable=True)
    firmware_version: Mapped[str] = mapped_column(String(255), nullable=True)

    notification_email: Mapped[str] = mapped_column(
        String(255),
        nullable=True,
    )

    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)
    updated_on: Mapped[datetime] = mapped_column(
        default=db.func.now(),
        onupdate=db.func.now(),
        nullable=True,
    )

    heartbeats: Mapped[List["HeartBeat"]] = relationship(
        "HeartBeat", backref="encoder", lazy=True, cascade="all,delete"
    )

    encoder_group_id: Mapped[int] = mapped_column(
        db.Integer,
        db.ForeignKey("encoder_group.id"),
        nullable=True,
    )

    encoder_groups: Mapped[List["EncoderGroup"]] = relationship(  # type: ignore # noqa
        secondary=association_table, back_populates="encoders"
    )

    __mapper_args__ = {
        "polymorphic_identity": "encoder",
        "polymorphic_on": encoder_type,
    }

    def __str__(self):
        return self.hostname

    @property
    def last_hearbeat(self):
        return self.get_heartbeat()

    @property
    def is_reachable(self):
        if len(self.heartbeats) > 0:
            return self.heartbeats[-1].is_reachable
        return False

    @property
    def origin_no_protocol(self):
        new_origin = self.stream_server.replace("rtmp://", "")
        streamlock_origin = self.get_streamlock_origin(new_origin)

        return streamlock_origin

    @property
    def edge_server(self):
        edge_url = current_app.config["WOWZA_EDGE_URL"]
        edge_url = edge_url.replace("https://", "")

        return edge_url

    def get_streamlock_origin(self, origin):
        # Get the streamlock origin from the dictionary streamlock_origins
        origin = current_app.config["STREAMLOCK_ORIGINS"][origin.strip().strip("\t")]
        return origin

    def build_server_origin(self, is_test: bool = False):
        stream_app = self.test_strean_application if is_test else self.stream_application
        if self.stream_server.endswith("/"):
            origin = f"{self.stream_server}{stream_app.strip('/')}"
        else:
            origin = f"{self.stream_server}/{stream_app.strip('/')}"

        logger.debug(f"Origin for the stream: {origin}")
        return origin

    def start_test(self, save_heartbeat=False) -> EncoderResponse:
        logger.debug(f"{self.hostname}: Starting test")
        self.set_test_stream_name()
        origin = self.build_server_origin(is_test=True)

        set_rtmp_result, _ = self.configure_stream_recording(
            self.stream_name,
            wowza_url=origin,
        )
        if set_rtmp_result == EncoderStatus.OK:
            logger.debug(f"{self.hostname}: Set RTMP result: {set_rtmp_result}")
            logger.debug(f"{self.hostname}: Start test encoder...")
            start_result = self.start_test_encoder()

            logger.debug(f"{self.hostname}: Start result: {start_result}")

            if start_result == EncoderStatus.OK:
                self.stream_running = True
                self.stream_start_datetime = db.func.now()
                db.session.commit()
                is_reachable = True
                message = "OK"
            else:
                is_reachable = False
                message = "Unable to start test stream"

            if save_heartbeat:
                self.create_heartbeat(is_reachable, message)

            return {
                "status": start_result,
                "message": message,
                "is_reachable": is_reachable,
                "stream_name": self.stream_name,
                "recording_path": None,
            }
        logger.debug(f"{self.hostname}: set result: {set_rtmp_result}")
        return {
            "status": set_rtmp_result,
            "message": "Unable to configure the device",
            "is_reachable": False,
            "stream_name": self.stream_name,
            "recording_path": None,
        }

    def stop_test(self, save_heartbeat=False) -> EncoderResponse:
        logger.debug(f"{self.hostname}: Stopping test")

        status = self.stop_test_encoder()

        if status == EncoderStatus.OK:
            self.stream_running = False
            self.stream_stop_datetime = db.func.now()
            db.session.commit()
            is_reachable = True
            message = "OK"
        else:
            is_reachable = False
            message = "Unable to stop test stream"
        if save_heartbeat:
            self.create_heartbeat(is_reachable, message)
        return {
            "status": status,
            "message": message,
            "is_reachable": is_reachable,
            "stream_name": self.stream_name,
            "recording_path": None,
        }

    def start_stream(
        self,
        indico_id: str,
        only_recording: bool = False,
        save_heartbeat=False,
        is_restart: bool = False,
        base_group_path: str = None,
        start_as_composite: bool = False,
    ) -> EncoderResponse:
        logger.debug(f"Starting stream {indico_id} on encoder {self.hostname}")

        try:
            origin_url = self.build_server_origin(is_test=only_recording)
            recording_path = self.get_recording_path(indico_id, base_group_path=base_group_path)
            if not is_restart:
                if only_recording:
                    self.set_test_stream_name()
                else:
                    self.set_stream_name(indico_id)

                self.set_recording_path(recording_path)
                self.configure_stream_recording(
                    self.stream_name,
                    recording_path=recording_path,
                    wowza_url=origin_url,
                )

            kwargs = {
                "stream_name": self.stream_name,
                "origin_url": origin_url,
                "recording_path": recording_path,
                "is_test": only_recording,
                "recording_prefix": self.get_stream_name(indico_id),
                "start_as_composite": start_as_composite,
            }
            start_result = self.start_stream_encoder(**kwargs)
            stream_result = self.handle_start_stream_result(start_result, save_heartbeat)
            return stream_result
        except ConnectionError as error:
            is_reachable = False
            message = "Unable to start stream"
            logger.error(
                message,
                exc_info=True,
                extra={
                    "error": error,
                    "tags": {"stream_name": self.stream_name, "encoder": self.hostname},
                },
            )
            if save_heartbeat:
                self.create_heartbeat(is_reachable, message)

            return {
                "status": EncoderStatus.ERROR,
                "message": message,
                "is_reachable": is_reachable,
                "stream_name": self.stream_name,
                "recording_path": None,
            }

    def handle_start_stream_result(self, streaming_result, save_heartbeat) -> EncoderResponse:
        is_reachable = False
        message = "ERROR"

        logger.debug(f"Handle start stream. Status: {streaming_result}")

        if streaming_result == EncoderStatus.OK:
            self.stream_running = True
            self.stream_stop_datetime = db.func.now()
            db.session.commit()
            is_reachable = True
            message = "OK"
        else:
            is_reachable = False
            message = "Unable to stop stream"

        if save_heartbeat:
            self.create_heartbeat(is_reachable, message)
        return {
            "status": streaming_result,
            "message": message,
            "is_reachable": is_reachable,
            "stream_name": self.stream_name,
            "recording_path": None,
        }

    def handle_stop_stream_result(self, status, save_heartbeat) -> EncoderResponse:
        if status == EncoderStatus.OK:
            self.stream_running = False
            self.stream_stop_datetime = db.func.now()
            db.session.commit()
            is_reachable = True
            message = "OK"
        else:
            is_reachable = False
            message = "Unable to stop stream"
        if save_heartbeat:
            self.create_heartbeat(is_reachable, message)
        return {
            "status": status,
            "message": message,
            "is_reachable": is_reachable,
            "stream_name": self.stream_name,
            "recording_path": None,
        }

    def start_recording(
        self,
        indico_id: str,
        save_heartbeat=False,
        is_restart: bool = False,
        base_group_path: str = None,
        start_as_composite: bool = False,
    ) -> EncoderResponse:
        self.set_stream_name(indico_id)
        recording_path = self.get_recording_path(indico_id, base_group_path=base_group_path)
        self.set_recording_path(recording_path)
        status = EncoderStatus.ERROR
        message = "Unknown error"
        is_reachable = False
        try:
            if not is_restart:
                stream_result, recording_result = self.configure_stream_recording(
                    self.stream_name, recording_path=recording_path
                )
            if not is_restart or (stream_result != EncoderStatus.ERROR and recording_result != EncoderStatus.ERROR):
                kwargs = {
                    "stream_name": self.stream_name,
                    "recording_path": recording_path,
                    "recording_prefix": self.get_stream_name(indico_id),
                    "start_as_composite": start_as_composite,
                }
                status = self.start_recording_encoder(**kwargs)

                start_result = self.handle_start_recording_result(status, save_heartbeat)
                return start_result
            else:
                message = "Unable to configure the device"

        except ConnectionError as error:
            message = "Unable to start recording"
            logger.error(
                message,
                exc_info=True,
                extra={
                    "error": error,
                    "tags": {"stream_name": self.stream_name, "encoder": self.hostname},
                },
            )
            if save_heartbeat:
                self.create_heartbeat(is_reachable, message)

        final_result: EncoderResponse = {
            "status": status,
            "message": message,
            "is_reachable": is_reachable,
            "stream_name": self.stream_name,
            "recording_path": recording_path,
        }
        return final_result

    def handle_start_recording_result(self, status: EncoderStatus, save_heartbeat: bool) -> EncoderResponse:
        if status == EncoderStatus.OK:
            self.recording_running = True
            self.recording_start_datetime = db.func.now()
            db.session.commit()
            is_reachable = True
            message = "OK"
        else:
            is_reachable = False
            message = "Unable to start recording"

        if save_heartbeat:
            self.create_heartbeat(is_reachable, message)

        return {
            "status": status,
            "message": message,
            "is_reachable": is_reachable,
            "stream_name": self.stream_name,
            "recording_path": self.recording_path,
        }

    def handle_stop_recording_result(self, status, save_heartbeat) -> EncoderResponse:
        logger.debug(f"Handle stop. Status: {status}")
        if status == EncoderStatus.OK:
            self.recording_running = False
            self.recording_stop_datetime = db.func.now()
            db.session.commit()
            is_reachable = True
            message = "OK"
        else:
            is_reachable = False
            message = "Unable to stop recording"
        if save_heartbeat:
            self.create_heartbeat(is_reachable, message)

        return {
            "status": status,
            "message": message,
            "is_reachable": is_reachable,
            "stream_name": self.stream_name,
            "recording_path": self.recording_path,
        }

    def start_all(
        self,
        indico_id: str,
        only_recording=False,
        save_heartbeat=False,
        is_restart: bool = False,
        base_group_path: str = None,
        start_as_composite: bool = False,
    ) -> EncoderResponse:
        logger.debug(f"{self.hostname}: Starting streaming and recording")

        origin_url = self.build_server_origin(is_test=only_recording)
        recording_path = self.get_recording_path(indico_id, base_group_path=base_group_path)

        if not is_restart:
            if only_recording:
                self.set_test_stream_name()
            else:
                self.set_stream_name(indico_id)

            self.set_recording_path(recording_path)
            self.configure_stream_recording(self.stream_name, wowza_url=origin_url, recording_path=recording_path)

        kwargs = {
            "stream_name": self.stream_name,
            "origin_url": origin_url,
            "recording_path": recording_path,
            "is_test": only_recording,
            "recording_prefix": self.get_stream_name(indico_id),
            "start_as_composite": start_as_composite,
        }
        logger.debug(f"Starting all encoders with kwargs: {kwargs}")
        streaming_result, recording_result = self.start_all_encoders(**kwargs)

        streaming_started = self.handle_start_stream_result(streaming_result, save_heartbeat)
        recording_started = self.handle_start_recording_result(recording_result, save_heartbeat)

        if streaming_started["status"] == EncoderStatus.OK and recording_started["status"] == EncoderStatus.OK:
            return {
                "status": recording_started["status"],
                "message": recording_started["message"],
                "is_reachable": recording_started["is_reachable"],
                "stream_name": self.stream_name,
                "recording_path": recording_path,
            }

        return {
            "status": EncoderStatus.ERROR,
            "message": recording_started["message"],
            "is_reachable": recording_started["is_reachable"],
            "stream_name": self.stream_name,
            "recording_path": recording_path,
        }

    def stop_all(
        self,
        save_heartbeat=False,
        start_as_composite: bool = False,
    ) -> EncoderResponse:
        kwargs = {"start_as_composite": start_as_composite}
        streaming_result, recording_result = self.stop_all_encoders(**kwargs)

        streaming_stopped = self.handle_stop_stream_result(streaming_result, save_heartbeat)
        recording_stopped = self.handle_stop_recording_result(recording_result, save_heartbeat)

        if streaming_stopped["status"] == EncoderStatus.OK and recording_stopped["status"] == EncoderStatus.OK:
            return {
                "status": recording_stopped["status"],
                "message": recording_stopped["message"],
                "is_reachable": recording_stopped["is_reachable"],
                "stream_name": self.stream_name,
                "recording_path": self.recording_path,
            }

        return {
            "status": EncoderStatus.ERROR,
            "message": recording_stopped["message"],
            "is_reachable": recording_stopped["is_reachable"],
            "stream_name": self.stream_name,
            "recording_path": self.recording_path,
        }

    @abstractmethod
    def stop_all_encoders(self, **kwargs) -> tuple[EncoderStatus, EncoderStatus]:
        raise NotImplementedError

    def stop_recording(
        self,
        save_heartbeat=False,
        start_as_composite: bool = False,
    ) -> EncoderResponse:
        try:
            kwargs = {"start_as_composite": start_as_composite}
            status = self.stop_recording_encoder(**kwargs)
            stop_result = self.handle_stop_recording_result(status, save_heartbeat)
            return stop_result
        except ConnectionError as error:
            is_reachable = False
            message = "Unable to stop recording"
            logger.error(
                message,
                exc_info=True,
                extra={
                    "error": error,
                    "tags": {"stream_name": self.stream_name, "encoder": self.hostname},
                },
            )
            if save_heartbeat:
                self.create_heartbeat(is_reachable, message)

            return {
                "status": EncoderStatus.ERROR,
                "message": message,
                "is_reachable": is_reachable,
                "stream_name": self.stream_name,
                "recording_path": self.recording_path,
            }

    @abstractmethod
    def stop_recording_encoder(self) -> EncoderStatus:
        raise NotImplementedError

    def stop_stream(self, save_heartbeat=False, start_as_composite=False) -> EncoderResponse:
        logger.debug(f"Stopping stream on encoder {self.hostname}")

        try:
            kwargs = {"start_as_composite": start_as_composite}
            status = self.stop_stream_encoder(**kwargs)
            stream_result = self.handle_stop_stream_result(status, save_heartbeat)
            return stream_result
        except ConnectionError as error:
            is_reachable = False
            message = "Unable to stop stream"
            logger.error(
                message,
                exc_info=True,
                extra={
                    "error": error,
                    "tags": {"stream_name": self.stream_name, "encoder": self.hostname},
                },
            )
            if save_heartbeat:
                self.create_heartbeat(is_reachable, message)

            return {
                "status": EncoderStatus.ERROR,
                "message": message,
                "is_reachable": is_reachable,
                "stream_name": self.stream_name,
                "recording_path": None,
            }

    @abstractmethod
    def stop_stream_encoder(self, **kwargs) -> EncoderStatus:
        raise NotImplementedError

    def get_status(self, save_heartbeat=False) -> EncoderResponse:
        is_reachable = False
        message = "OK"
        status = EncoderStatus.ERROR
        try:
            status = self.get_status_encoder()
            logger.debug(f"Encoder {self.hostname} status: {status}")
            is_reachable = True
            status = EncoderStatus.OK
        except ConnectionError as error:
            logger.warning(
                "Encoder is not reachable",
                extra={
                    "error": error,
                    "tags": {"stream_name": self.stream_name, "encoder": self.hostname},
                },
            )
            message = f"Encoder {self.hostname} is not reachable: {error}"

        if save_heartbeat:
            logger.debug(f"Saving heartbeat for encoder {self.hostname}")
            self.create_heartbeat(is_reachable, message)
            logger.debug(f"Heartbeat saved for encoder {self.hostname}")

        return {
            "status": status,
            "message": message,
            "is_reachable": is_reachable,
            "stream_name": self.stream_name,
            "recording_path": self.recording_path,
        }

    def create_heartbeat(self, is_reachable: bool, message: str):
        heartbeat = HeartBeat(
            message=message,
            is_reachable=is_reachable,
            encoder_id=self.id,
        )
        db.session.add(heartbeat)
        self.heartbeats.append(heartbeat)
        db.session.commit()

    def get_heartbeat(self) -> Union[HeartBeat, None]:
        # Get the latest heartbeat from the heartbeats
        if len(self.heartbeats) > 0:
            return self.heartbeats[-1]
        return None

    def get_encoder_type(self) -> str:
        return self.encoder_type.value

    def set_test_stream_name(self):
        """
        Set the correct test stream name for each camera and slides of monarch encoder
        """
        encoder_name = self.hostname.split(".cern.ch")[0]
        if self.signal_type == EncoderSignalType.CAMERA:
            self.stream_name = f"{encoder_name}_camera_test"

        elif self.signal_type == EncoderSignalType.SLIDES:
            self.stream_name = f"{encoder_name}_slides_test"
        else:
            self.stream_name = self.hostname

        db.session.commit()

    def set_stream_name(self, stream_name: str):
        """
        Set the correct test stream name for each camera and slides of monarch encoder
        """
        new_stream_name = self.get_stream_name(stream_name)
        self.stream_name = new_stream_name

        db.session.commit()

    def set_recording_path(self, recording_path: str):
        self.recording_path = recording_path
        db.session.commit()

    def get_stream_name(self, stream_name: str):
        logger.debug(f"{self.hostname}: signal type: {self.signal_type.value}")
        if self.signal_type == EncoderSignalType.CAMERA:
            stream_name = f"{stream_name}_camera"

        elif self.signal_type == EncoderSignalType.SLIDES:
            stream_name = f"{stream_name}_slides"
        else:
            stream_name = self.hostname
        return stream_name

    def get_recording_path(self, stream_name: str, base_group_path: Optional[str] = None) -> str:
        new_stream_name = self.get_stream_name(stream_name)
        if self.encoder_type == EncoderType.EPIPHAN_PEARL_NANO:
            recording_path = current_app.config["EPIPHAN_NANO_RECORDING_PATH"]
        else:
            recording_path = current_app.config["DEFAULT_RECORDING_PATH"]

            if base_group_path:
                reformatted_recording_path = base_group_path.replace("\\", "/")
                recording_path = f"{reformatted_recording_path}/{new_stream_name}"

                logger.debug(f"Setting up recording file name: {recording_path}")

        return recording_path

    @abstractmethod
    def start_test_encoder(self) -> EncoderStatus:
        raise NotImplementedError

    @abstractmethod
    def stop_test_encoder(self) -> EncoderStatus:
        raise NotImplementedError

    @abstractmethod
    def start_stream_encoder(self, **kwargs) -> EncoderStatus:
        raise NotImplementedError

    @abstractmethod
    def start_recording_encoder(self, **kwargs) -> EncoderStatus:
        raise NotImplementedError

    @abstractmethod
    def start_all_encoders(self, **kwargs) -> tuple[EncoderStatus, EncoderStatus]:
        raise NotImplementedError

    @abstractmethod
    def get_status_encoder(self) -> EncoderStatus:
        raise NotImplementedError

    @abstractmethod
    def configure_stream_recording(
        self,
        stream_name,
        wowza_url: Optional[str] = None,
        recording_path: Optional[str] = None,
    ) -> tuple[EncoderStatus, EncoderStatus]:
        raise NotImplementedError

    def test_function(self):
        logger.debug(f"{self.hostname}: Calling test function on encoder")

    def test_rollback_function(self):
        logger.debug(f"{self.hostname}: Calling rollback function on encoder")
