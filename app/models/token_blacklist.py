import enum
import logging
from datetime import datetime

from sqlalchemy import String, event
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.orm.attributes import get_history

from app.extensions import db
from app.models.base import ModelBase

logger = logging.getLogger("webapp.models")


class ApiTokenResources(enum.Enum):
    ALL = "all"
    STATS = "stats"


class ApiToken(ModelBase):

    # __table_args__ = {"schema": "encoders"}

    name: Mapped[str] = mapped_column(String(255), nullable=False)
    access_token: Mapped[str] = mapped_column(String(1024), nullable=False)
    jti: Mapped[str] = mapped_column(String(255), nullable=False)
    token_type: Mapped[str] = mapped_column(String(255), nullable=False)
    user_identity: Mapped[str] = mapped_column(String(255), nullable=False)
    revoked: Mapped[bool] = mapped_column(nullable=False)
    revoked_on: Mapped[datetime] = mapped_column(nullable=True)
    expires: Mapped[datetime] = mapped_column(nullable=True)
    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)
    last_used: Mapped[datetime] = mapped_column(nullable=True)
    resource: Mapped[ApiTokenResources] = mapped_column(
        nullable=False,
    )

    def to_dict(self) -> dict:
        """Return the values of the object as a dictionary

        Returns:
            dict: The object values
        """
        return {
            "token_id": self.id,
            "jti": self.jti,
            "token_type": self.token_type,
            "user_identity": self.user_identity,
            "revoked": self.revoked,
            "expires": self.expires,
            "resource": self.resource,
            "name": self.name,
        }


@event.listens_for(db.session, "before_flush")
def receive_before_flush_api_token(session, flush_context, instances):
    "listen for the 'after_flush' event"
    # pylint: disable=unused-argument
    for instance in session.dirty:
        if not isinstance(instance, ApiToken):
            continue

        previous_value = get_history(instance, "revoked")[2]
        if len(previous_value) > 0:
            revoked_value = previous_value[0]
            if instance.revoked != revoked_value:
                if instance.revoked:
                    instance.revoked_on = datetime.now()
                else:
                    instance.revoked_on = None
