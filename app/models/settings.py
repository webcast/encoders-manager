import logging

from sqlalchemy.orm import Mapped, mapped_column

from app.extensions import db
from app.models.base import ModelBase

logger = logging.getLogger("webapp.models_settings")


class Settings(ModelBase):
    """
    Settings model object
    """

    # __table_args__ = {"schema": "encoders"}

    enable_ravem_notifications: Mapped[bool] = mapped_column(db.Boolean, default=False, nullable=True)
    enable_webcast_website_notifications: Mapped[bool] = mapped_column(db.Boolean, default=False, nullable=True)
    enable_ces_notifications: Mapped[bool] = mapped_column(db.Boolean, default=False, nullable=True)
