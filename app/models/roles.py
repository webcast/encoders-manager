from datetime import datetime
from typing import TYPE_CHECKING

from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.extensions import db
from app.models.base import ModelBase

if TYPE_CHECKING:
    from app.models.rooms import Room

external_uri_room = "External URI"


class Role(ModelBase):

    name: Mapped[str] = mapped_column(String(255), unique=True, nullable=False)

    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)
    updated_on: Mapped[datetime] = mapped_column(default=db.func.now(), onupdate=db.func.now(), nullable=True)

    rooms: Mapped["Room"] = relationship("Room", backref="role", lazy=True)

    def __str__(self):
        return self.name
