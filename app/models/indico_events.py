import enum
from datetime import datetime
from typing import TYPE_CHECKING, List

from sqlalchemy import ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.extensions import db
from app.models.base import ModelBase

if TYPE_CHECKING:
    from app.models.encoder_groups import EncoderGroup


class Audience(enum.Enum):
    ATLAS = "ATLAS"
    CMS = "CMS"
    PUBLIC = "PUBLIC"


class EventType(enum.Enum):
    STREAM = "STREAM"
    RECORDING = "RECORDING"
    STREAM_RECORDING = "STREAM_RECORDING"


class IndicoEventException(Exception):
    pass


class IndicoEvent(ModelBase):
    __tablename__ = "indico_event"
    # __table_args__ = {"schema": "encoders_local"}

    title: Mapped[str] = mapped_column(String(255), nullable=False)
    audience: Mapped[Audience] = mapped_column(nullable=False)
    indico_id: Mapped[str] = mapped_column(String(255), nullable=False)
    indico_url: Mapped[str] = mapped_column(String(255), nullable=True)
    start_datetime: Mapped[datetime] = mapped_column(nullable=False)
    end_datetime: Mapped[datetime] = mapped_column(nullable=False)

    is_locked: Mapped[bool] = mapped_column(default=False, nullable=True)

    event_type: Mapped[EventType] = mapped_column(nullable=False, default=EventType.STREAM)

    is_streamed: Mapped[bool] = mapped_column(default=False, nullable=True)
    is_recorded: Mapped[bool] = mapped_column(default=False, nullable=True)

    encoder_camera: Mapped[str] = mapped_column(String(255), nullable=True)
    encoder_camera_type: Mapped[str] = mapped_column(String(255), nullable=True)
    encoder_slides: Mapped[str] = mapped_column(String(255), nullable=True)
    encoder_slides_type: Mapped[str] = mapped_column(String(255), nullable=True)

    stream_camera_name: Mapped[str] = mapped_column(String(255), nullable=True)
    stream_slides_name: Mapped[str] = mapped_column(String(255), nullable=True)

    recording_path: Mapped[str] = mapped_column(String(255), nullable=True)
    recording_camera_path: Mapped[str] = mapped_column(String(255), nullable=True)
    recording_slides_path: Mapped[str] = mapped_column(String(255), nullable=True)

    stream_start_datetime: Mapped[datetime] = mapped_column(nullable=True)
    stream_stop_datetime: Mapped[datetime] = mapped_column(nullable=True)

    recording_start_datetime: Mapped[datetime] = mapped_column(nullable=True)
    recording_stop_datetime: Mapped[datetime] = mapped_column(nullable=True)

    auto_stop_datetime: Mapped[datetime] = mapped_column(nullable=True)
    auto_stopped: Mapped[bool] = mapped_column(default=False, nullable=True)

    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)
    updated_on: Mapped[datetime] = mapped_column(default=db.func.now(), onupdate=db.func.now(), nullable=True)

    room_id: Mapped[int] = mapped_column(ForeignKey("room.id"))
    stats: Mapped[List["Stats"]] = relationship("Stats", backref="indico_event", lazy=True)

    encoder_group_id: Mapped[int] = mapped_column(ForeignKey("encoder_group.id"), nullable=True)
    encoder_group: Mapped["EncoderGroup"] = relationship(
        "EncoderGroup",
        foreign_keys=encoder_group_id,
    )

    ces_notified: Mapped[bool] = mapped_column(default=False, nullable=True)
    webcast_website_notified: Mapped[bool] = mapped_column(default=False, nullable=True)
    ravem_notified: Mapped[bool] = mapped_column(default=False, nullable=True)
    reminder_sent: Mapped[bool] = mapped_column(default=False, nullable=True)

    @property
    def is_running(self):
        # If there is an encoder group associated to the event and it is running,
        # return True
        if self.encoder_group and self.encoder_group.is_running:
            return True
        return False

    def __str__(self):
        return f"{self.indico_id} - {self.title}"

    def __repr__(self):
        return f"<IndicoEvent {self.indico_id} - {self.title}>"


class Stats(ModelBase):
    __tablename__ = "stats"
    # __table_args__ = {"schema": "encoders_local"}

    max_connections = mapped_column(db.Integer, nullable=True)
    mean_connections = mapped_column(db.Integer, nullable=True)
    start_datetime = mapped_column(db.DateTime)
    end_datetime = mapped_column(db.DateTime)

    created_on: Mapped[datetime] = mapped_column(default=datetime.utcnow)
    updated_on = mapped_column(db.DateTime, default=db.func.now(), onupdate=db.func.now(), nullable=True)

    indico_event_id = mapped_column(db.Integer, db.ForeignKey("indico_event.id"))
