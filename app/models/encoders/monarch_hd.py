import enum
import logging
from typing import Optional

from flask import current_app
from sqlalchemy.orm import reconstructor

from app.extensions import db
from app.models.base_encoder import EncoderStatus, EncoderType
from app.models.encoders.monarch import Monarch
from app.services.encoders.matrox.monarch_hd import MonarchHDApiClient

logger = logging.getLogger("webapp.monarch_hd")


class MonarchHDStatuses(enum.Enum):
    ON = "ON"
    DISABLED = "DISABLED"
    READY = "READY"


class MonarchHD(Monarch):
    __mapper_args__ = {
        "polymorphic_identity": EncoderType.MONARCH_HD,
    }

    @reconstructor
    def init_on_load(self):
        self.api_client = MonarchHDApiClient(
            self.hostname,
            current_app.config["MONARCH_USERNAME"],
            password=current_app.config["MONARCH_PASSWORD"],
        )
        self.test_strean_application = current_app.config["TEST_STREAM_APP"]

    def get_status_encoder(self) -> EncoderStatus:
        status = self.api_client.get_status()

        logger.debug(f"Status: {status}")

        if status == "ERROR" or status == "RETRY":
            return EncoderStatus.ERROR

        # RECORD:READY,STREAM:RTMP,READY,NAME:Matrox01
        # RECORD:ON,STREAM:RTMP,ON,NAME:Matrox01
        # RECORD:DISABLED,STREAM:RTMP,DISABLED,NAME:Matrox01
        status_items = status.split(",")
        recording_status = status_items[0].split(":")[1]
        stream_status = status_items[2]

        self.stream_running = True if stream_status == MonarchHDStatuses.ON.value else False
        self.recording_running = True if recording_status == MonarchHDStatuses.ON.value else False

        db.session.commit()

        if recording_status == MonarchHDStatuses.DISABLED.value or stream_status == MonarchHDStatuses.DISABLED.value:
            return EncoderStatus.ERROR

        return EncoderStatus.OK

    def start_stream_encoder(self, **kwargs) -> EncoderStatus:
        logger.debug(f"Starting stream {self.stream_name}")
        result = self.api_client.start_streaming()

        if result == "SUCCESS":
            return EncoderStatus.OK

        return EncoderStatus.ERROR

    def stop_stream_encoder(self, **kwargs) -> EncoderStatus:
        result = self.api_client.stop_streaming()

        if result == "SUCCESS":
            return EncoderStatus.OK

        return EncoderStatus.ERROR

    def start_test_encoder(self) -> EncoderStatus:
        result = self.api_client.start_streaming()
        logger.debug(f"{self.hostname}: Starting test stream on MonarchHD")
        if result == "SUCCESS":
            return EncoderStatus.OK
        return EncoderStatus.ERROR

    def stop_test_encoder(self, **kwargs) -> EncoderStatus:
        result = self.api_client.stop_streaming()

        if result == "SUCCESS":
            return EncoderStatus.OK

        return EncoderStatus.ERROR

    def start_recording_encoder(self, **kwargs) -> EncoderStatus:
        result = self.api_client.start_recording()
        if result == "SUCCESS":
            return EncoderStatus.OK

        return EncoderStatus.ERROR

    def stop_recording_encoder(self, **kwargs) -> EncoderStatus:
        result = self.api_client.stop_recording()
        if result == "SUCCESS":
            return EncoderStatus.OK

        return EncoderStatus.ERROR

    def start_all_encoders(self, **kwargs):
        logger.debug("Start all encoders")
        result = self.api_client.start_both_encoders()

        if result == "SUCCESS":
            return EncoderStatus.OK, EncoderStatus.OK

        return EncoderStatus.ERROR, EncoderStatus.ERROR

    def stop_all_encoders(self, **kwargs):
        result = self.api_client.stop_both_encoders()

        if result == "SUCCESS":
            return EncoderStatus.OK, EncoderStatus.OK

        return EncoderStatus.ERROR, EncoderStatus.ERROR

    def configure_stream_recording(
        self,
        stream_name: str,
        wowza_url: Optional[str] = None,
        recording_path: Optional[str] = None,
    ) -> tuple[EncoderStatus, EncoderStatus]:
        stream_result, recording_result = self.api_client.configure_stream_recording(
            stream_name,
            wowza_origin_url=wowza_url,
            recording_path=recording_path,
        )
        logger.debug(f"{self.hostname}: set_rtmp: {stream_result}, " f"set_recording: {recording_result}")

        if stream_result == "FAILED":
            stream_result = EncoderStatus.ERROR
        else:
            stream_result = EncoderStatus.OK

        if recording_result == "FAILED":
            recording_result = EncoderStatus.ERROR
        else:
            recording_result = EncoderStatus.OK

        return stream_result, recording_result
