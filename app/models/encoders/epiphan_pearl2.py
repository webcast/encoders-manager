import logging
from typing import Optional

from flask import current_app
from sqlalchemy.orm import Mapped, mapped_column, reconstructor

from app.extensions import db
from app.models.base_encoder import (
    Encoder,
    EncoderSignalType,
    EncoderStatus,
    EncoderType,
)
from app.services.encoders.epiphan.epiphan_pearl2_api_client import (
    EpiphanPearl2APIClient,
)

logger = logging.getLogger("webapp.encoders")


class EpiphanPearl2(Encoder):

    __mapper_args__ = {
        "polymorphic_identity": EncoderType.EPIPHAN_PEARL2,
    }

    camera_channel_id: Mapped[int] = mapped_column(
        db.Integer,
        nullable=True,
    )

    slides_channel_id: Mapped[int] = mapped_column(
        db.Integer,
        nullable=True,
    )

    composite_channel_id: Mapped[int] = mapped_column(
        db.Integer,
        nullable=True,
    )

    normal_layout_id: Mapped[int] = mapped_column(
        db.Integer,
        nullable=True,
    )

    composite_layout_id: Mapped[int] = mapped_column(
        db.Integer,
        nullable=True,
    )

    @reconstructor
    def init_on_load(self):
        self.api_client = EpiphanPearl2APIClient(
            self.hostname,
            current_app.config["EPIPHAN_USERNAME"],
            password=current_app.config["EPIPHAN_PASSWORD"],
        )
        self.test_strean_application = current_app.config["TEST_STREAM_APP"]

    def start_recording_encoder(self, **kwargs) -> EncoderStatus:
        recording_prefix = kwargs["recording_prefix"]
        start_as_composite = kwargs["start_as_composite"]
        channel_id = self.select_encoder_channel(start_as_composite)

        self.set_desired_layout(start_as_composite, channel_id)

        result = self.api_client.start_recording(recording_prefix, channel_id)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def stop_recording_encoder(self, **kwargs) -> EncoderStatus:
        start_as_composite = kwargs["start_as_composite"]
        channel_id = self.select_encoder_channel(start_as_composite)
        result = self.api_client.stop_recording(channel_id)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def start_test_encoder(self, **kwargs) -> EncoderStatus:
        origin_url = self.build_server_origin(is_test=True)
        start_as_composite = kwargs["start_as_composite"]
        channel_id = self.select_encoder_channel(start_as_composite)
        self.set_desired_layout(start_as_composite, channel_id)
        result = self.api_client.start_streaming(channel_id, origin_url, self.stream_name)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def stop_test_encoder(self, **kwargs) -> EncoderStatus:
        start_as_composite = kwargs["start_as_composite"]
        channel_id = self.select_encoder_channel(start_as_composite)

        result = self.api_client.stop_streaming(channel_id)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def start_stream_encoder(
        self,
        **kwargs,
    ) -> EncoderStatus:
        origin_url = self.build_server_origin(is_test=kwargs["is_test"])
        start_as_composite = kwargs["start_as_composite"]
        channel_id = self.select_encoder_channel(start_as_composite)

        self.set_desired_layout(start_as_composite, channel_id)

        result = self.api_client.start_streaming(channel_id, origin_url, self.stream_name)

        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def stop_stream_encoder(
        self,
        **kwargs,
    ) -> EncoderStatus:
        start_as_composite = kwargs["start_as_composite"]
        channel_id = self.select_encoder_channel(start_as_composite)

        result = self.api_client.stop_streaming(channel_id)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def get_status_encoder(self) -> EncoderStatus:
        status = self.api_client.get_status()

        self.stream_running = True if status["publish_enabled"] == "on" else False
        self.recording_running = True if status["rec_enabled"] == "on" else False
        self.product_name = status["product_name"]
        self.firmware_version = status["firmware_version"]

        db.session.commit()

        return EncoderStatus.OK

    def start_all_encoders(self, **kwargs):
        stream_status = self.start_stream_encoder(**kwargs)
        recording_status = self.start_recording_encoder(**kwargs)

        stream_return_status = EncoderStatus.OK
        recording_return_status = EncoderStatus.OK

        if stream_status == "ERROR":
            stream_return_status = EncoderStatus.ERROR

        if recording_status == "ERROR":
            recording_return_status = EncoderStatus.ERROR

        return stream_return_status, recording_return_status

    def stop_all_encoders(self, **kwargs) -> tuple[EncoderStatus, EncoderStatus]:
        stream_status = self.stop_stream_encoder(**kwargs)
        recording_status = self.stop_recording_encoder(**kwargs)

        stream_return_status = EncoderStatus.OK
        recording_return_status = EncoderStatus.OK

        if stream_status == "ERROR":
            stream_return_status = EncoderStatus.ERROR

        if recording_status == "ERROR":
            recording_return_status = EncoderStatus.ERROR

        return stream_return_status, recording_return_status

    def configure_stream_recording(
        self,
        stream_name,
        wowza_url: Optional[str] = None,
        recording_path: Optional[str] = None,
    ) -> tuple[EncoderStatus, EncoderStatus]:
        return EncoderStatus.OK, EncoderStatus.OK

    def set_recording_size_limit(self, limit: int) -> EncoderStatus:
        result = self.api_client.set_recording_file_size_limit(limit)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def set_recording_time_limit(self, limit: str) -> EncoderStatus:
        result = self.api_client.set_recording_time_limit(limit)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def set_active_layout(self, layout_index: int, channel_id: int) -> EncoderStatus:
        result = self.api_client.set_active_layout(layout_index, channel_id)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def select_encoder_channel(self, start_as_composite: int) -> int:
        if start_as_composite:
            channel_id = self.composite_channel_id
        else:
            if self.signal_type == EncoderSignalType.CAMERA:
                channel_id = self.camera_channel_id
            else:
                channel_id = self.slides_channel_id

        logger.debug(f"Selected channel id: {channel_id}")
        return channel_id

    def set_desired_layout(self, start_as_composite: int, channel_id: int) -> EncoderStatus:
        logger.debug(f"Setting desired layout for channel {channel_id}")
        if start_as_composite:
            logger.debug(f"Selected layout index: {self.composite_layout_id}")
            layout_index = self.composite_layout_id
        else:
            layout_index = self.normal_layout_id

        logger.debug(f"Selected layout index: {layout_index}")

        result = self.set_active_layout(layout_index, channel_id)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK
