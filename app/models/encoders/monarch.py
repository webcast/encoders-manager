import logging

from app.models.base_encoder import Encoder

logger = logging.getLogger("webapp.monarch_hd")


class Monarch(Encoder):
    __mapper_args__ = {"polymorphic_abstract": True}
