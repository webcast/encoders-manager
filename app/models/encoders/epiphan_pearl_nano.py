import logging
from typing import Optional

from flask import current_app
from sqlalchemy.orm import reconstructor

from app.extensions import db
from app.models.base_encoder import Encoder, EncoderStatus, EncoderType
from app.services.encoders.epiphan.epiphan_nano_api_client import EpiphanNanoAPIClient

logger = logging.getLogger("webapp.encoders")


class EpiphanPearlNano(Encoder):

    __mapper_args__ = {
        "polymorphic_identity": EncoderType.EPIPHAN_PEARL_NANO,
    }

    @reconstructor
    def init_on_load(self):
        self.api_client = EpiphanNanoAPIClient(
            self.hostname,
            current_app.config["EPIPHAN_USERNAME"],
            password=current_app.config["EPIPHAN_PASSWORD"],
        )
        self.test_strean_application = current_app.config["TEST_STREAM_APP"]

    def start_recording_encoder(self, **kwargs) -> EncoderStatus:
        recording_prefix = kwargs["recording_prefix"]
        result = self.api_client.start_recording(recording_prefix)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def stop_recording_encoder(self, **kwargs) -> EncoderStatus:
        result = self.api_client.stop_recording()
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def start_test_encoder(self, **kwargs) -> EncoderStatus:
        origin_url = self.build_server_origin(is_test=True)
        result = self.api_client.start_streaming(origin_url, self.stream_name)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def stop_test_encoder(self) -> EncoderStatus:
        result = self.api_client.stop_streaming()
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def start_stream_encoder(
        self,
        **kwargs,
    ) -> EncoderStatus:
        origin_url = self.build_server_origin(is_test=kwargs["is_test"])
        result = self.api_client.start_streaming(origin_url, self.stream_name)

        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def stop_stream_encoder(self, **kwargs) -> EncoderStatus:
        result = self.api_client.stop_streaming()
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def get_status_encoder(self) -> EncoderStatus:
        status = self.api_client.get_status()

        self.stream_running = True if status["publish_enabled"] == "on" else False
        self.recording_running = True if status["rec_enabled"] == "on" else False
        self.product_name = status["product_name"]
        self.firmware_version = status["firmware_version"]

        db.session.commit()

        return EncoderStatus.OK

    def start_all_encoders(self, **kwargs):
        stream_status = self.start_stream_encoder(**kwargs)
        recording_status = self.start_recording_encoder(**kwargs)

        stream_return_status = EncoderStatus.OK
        recording_return_status = EncoderStatus.OK

        if stream_status == "ERROR":
            stream_return_status = EncoderStatus.ERROR

        if recording_status == "ERROR":
            recording_return_status = EncoderStatus.ERROR

        return stream_return_status, recording_return_status

    def stop_all_encoders(self, **kwargs) -> tuple[EncoderStatus, EncoderStatus]:
        stream_status = self.stop_stream_encoder()
        recording_status = self.stop_recording_encoder()

        stream_return_status = EncoderStatus.OK
        recording_return_status = EncoderStatus.OK

        if stream_status == "ERROR":
            stream_return_status = EncoderStatus.ERROR

        if recording_status == "ERROR":
            recording_return_status = EncoderStatus.ERROR

        return stream_return_status, recording_return_status

    def configure_stream_recording(
        self,
        stream_name,
        wowza_url: Optional[str] = None,
        recording_path: Optional[str] = None,
    ) -> tuple[EncoderStatus, EncoderStatus]:
        return EncoderStatus.OK, EncoderStatus.OK

    def set_recording_size_limit(self, limit: int) -> EncoderStatus:
        result = self.api_client.set_recording_file_size_limit(limit)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK

    def set_recording_time_limit(self, limit: str) -> EncoderStatus:
        result = self.api_client.set_recording_time_limit(limit)
        if result == "ERROR":
            return EncoderStatus.ERROR
        return EncoderStatus.OK
