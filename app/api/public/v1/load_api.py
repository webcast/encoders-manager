import logging

from flask import Blueprint
from flask_restx import Api

from app.api.public.v1.namespaces.stats import namespace as stats_namespace

logger = logging.getLogger("webapp")


def load_api_namespaces() -> Blueprint:
    """Loadd all the namespaces from the private API.
    Private API is the API that is only accessible to
    the application internal use fro CRUD operations.

    Returns:
        Blueprint: The blueprint for the API
    """
    blueprint = Blueprint("public_api", __name__)
    api = Api(blueprint)
    api.add_namespace(stats_namespace)

    return blueprint
