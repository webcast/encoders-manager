import logging
from typing import Union

from flask_restx import Namespace, Resource, reqparse

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required
from app.daos.stats import StatsDAO

logger = logging.getLogger("webapp.api_public")

namespace = Namespace(
    "stats",
    description="Stats operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class StatsEndpoint(Resource):
    method_decorators = [jwt_resource_required("stats")]

    parser = reqparse.RequestParser()
    parser.add_argument(
        "startDate",
        type=str,
        help="Date of the stats to retrieve. Format: YYYY-MM-DD",
    )
    parser.add_argument(
        "endDate",
        type=str,
        help="End date of the stats to retrieve. Format: YYYY-MM-DD",
    )

    @namespace.doc("stats_endpoint")
    @namespace.expect(parser)
    def get(self) -> tuple[Union[dict, None], int]:
        logger.debug("Stats endpoint")

        args = self.parser.parse_args()
        start_date = args.get("startDate", None)
        end_date = args.get("endDate", None)

        if not start_date or not end_date:
            return {
                "message": "Date is required",
            }, 400

        stats_dao = StatsDAO()
        recordings = stats_dao.get_recording_count_between_dates(start_date, end_date)
        streams = stats_dao.get_stream_count_between_dates(start_date, end_date)
        streams_recordings = stats_dao.get_stream_recording_count_between_dates(start_date, end_date)

        return {
            "startDate": start_date,
            "endDate": end_date,
            "recordings": recordings,
            "streams": streams,
            "streamsRecordings": streams_recordings,
        }, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/rooms/")
class StatsRoomEndpoint(Resource):
    method_decorators = [jwt_resource_required("stats")]

    parser = reqparse.RequestParser()
    parser.add_argument(
        "startDate",
        type=str,
        help="Date of the stats to retrieve. Format: YYYY-MM-DD",
    )
    parser.add_argument(
        "endDate",
        type=str,
        help="End date of the stats to retrieve. Format: YYYY-MM-DD",
    )

    @namespace.doc("stats_room_endpoint")
    @namespace.expect(parser)
    def get(self) -> tuple[Union[dict, None], int]:
        logger.debug("Stats endpoint")

        args = self.parser.parse_args()
        start_date = args.get("startDate", None)
        end_date = args.get("endDate", None)

        if not start_date:
            return {
                "message": "Date is required",
            }, 400

        stats_dao = StatsDAO()
        recordings = stats_dao.get_recordings_per_room_between_dates(start_date, end_date)
        streams = stats_dao.get_streams_per_room_between_dates(start_date, end_date)
        streams_recordings = stats_dao.get_stream_recordings_per_room_between_dates(start_date, end_date)

        return {
            "startDate": start_date,
            "endDate": end_date,
            "recordings": recordings,
            "streams": streams,
            "streamsRecordings": streams_recordings,
        }, 200
