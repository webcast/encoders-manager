import logging

from flask import current_app, g
from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request
from flask_login import current_user

from app.daos.api_tokens import ApiTokenDAO
from app.daos.role import RoleDAO
from app.utils.auth.client_oidc import set_dummy_api_key

logger = logging.getLogger("webapp.decorators")


def admin_required(func):
    """
    Custom decorator that verifies the JWT is present in
    the request, as well as insuring that this user has access to the given resource.
    This way, the access token will only work with the selected API endpoints

    :param resource: Name of the resource (all, test)
    :return:
    """

    def function_wrapper(*args, **kwargs):
        if current_app.config["LOGIN_DISABLED"]:
            return func(*args, **kwargs)
        if not current_user and not current_user.is_admin:
            return "Forbidden", 401

        return func(*args, **kwargs)

    function_wrapper.__name__ = func.__name__
    return function_wrapper


def roles_required(func):
    """
    Custom decorator that verifies the JWT is present in
    the request, as well as insuring that this user has access to the given resource.
    This way, the access token will only work with the selected API endpoints

    :param resource: Name of the resource (all, test)
    :return:
    """

    def function_wrapper(*args, **kwargs):
        if current_app.config["LOGIN_DISABLED"]:
            return func(*args, **kwargs)

        has_access = False

        if current_user and hasattr(current_user, "is_admin") and current_user.is_admin:
            has_access = True
        else:
            logger.debug(
                f"Checking roles for user {current_user}",
            )
            logger.debug(f"Roles: {current_user.roles}")
            if not current_user or not hasattr(current_user, "roles"):
                return "Forbidden", 401

            app_roles = [role.name.strip() for role in RoleDAO.get_all()]
            user_roles = [role.strip() for role in current_user.roles.split(",")]

            if current_app.config["ADMIN_ROLE"] in user_roles:
                has_access = True

            for role in user_roles:
                if role in app_roles:
                    has_access = True
                    break

        if has_access:
            return func(*args, **kwargs)

        return "Forbidden", 401

    function_wrapper.__name__ = func.__name__
    return function_wrapper


def jwt_resource_required(resource):
    """
    Custom decorator that verifies the JWT is present in
    the request, as well as insuring that this user has access to the given resource.
    This way, the access token will only work with the selected API endpoints

    :param resource: Name of the resource (all, test)
    :return:
    """

    def decorator(func):
        def wrapper(*args, **kwargs):
            if current_app.config["LOGIN_DISABLED"]:
                set_dummy_api_key()
                return func(*args, **kwargs)
            verify_jwt_in_request()
            name = get_jwt_identity()
            api_key = ApiTokenDAO.get_by_name(name)
            g.account_id = api_key.account_id
            if api_key.resource.value != resource:
                return "Forbidden", 401
            ApiTokenDAO.update_last_used(api_key.id)
            if not api_key or api_key.revoked:
                return "Forbidden", 401
            else:
                return func(*args, **kwargs)

        return wrapper

    return decorator
