import logging

from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.api.decorators import roles_required
from app.daos.encoders import EncoderDAO
from app.models.base_encoder import Encoder, EncoderResponse
from app.services.mailer.mail_service import MailService
from app.tasks.encoders import check_encoders_task
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "encoders",
    description="Encoder",
    authorizations=authorizations,
    security=["Bearer Token"],
)

heartbeat_model = namespace.model(
    "HeartbeatModel",
    {
        "message": fields.String(required=True, description="The message", attribute="message"),
        "createdOn": fields.DateTime(required=True, description="The creation date", attribute="created_on"),
        "isReachable": fields.Boolean(required=True, description="Is reachable", attribute="is_reachable"),
    },
)

encoder_model = namespace.model(
    "EncoderModel",
    {
        "id": fields.Integer(required=True, description="The ID"),
        "hostname": fields.String(required=True, description="The encoder hostname", attribute="hostname"),
        "recordingRunning": fields.Boolean(attribute="recording_running"),
        "streamRunning": fields.Boolean(attribute="stream_running"),
        "encoderType": fields.String(attribute="encoder_type.value"),
        "signalType": fields.String(attribute="signal_type.value"),
        "encoderGroupId": fields.Integer(attribute="encoder_group_id"),
        "streamStartDatetime": fields.DateTime(attribute="stream_start_datetime"),
        "lastHeartbeat": fields.Nested(heartbeat_model, attribute="last_hearbeat"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:encoder_id>/")
class EncoderDetailsEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoder_details_endpoint")
    @namespace.marshal_with(encoder_model, as_list=False)
    def get(self, encoder_id: int) -> Encoder:
        """Get the details of an encoder

        Returns:
            Response: Flask response object
        """

        encoder = EncoderDAO.get_by_id(encoder_id)

        return encoder


encoder_response_model = namespace.model(
    "EncoderResponseModel",
    {
        "status": fields.String(attribute="status.value"),
        "isReachable": fields.Boolean(attribute="is_reachable"),
        "message": fields.String(attribute="message"),
        "recordingPath": fields.String(attribute="recording_path"),
        "streamName": fields.String(attribute="stream_name"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:encoder_id>/refresh/")
class EncoderGroupRoomEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoders_group_room_endpoint")
    @namespace.marshal_with(encoder_response_model, as_list=False)
    def put(self, encoder_id: int) -> EncoderResponse:
        """Refresh the status of an encoder

        Returns:
            Response: Flask response object
        """
        encoder: Encoder = EncoderDAO.get_by_id(encoder_id)

        status_response = encoder.get_status()

        return status_response


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:encoder_id>/status-email/")
class EncoderEmailEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoder_details_endpoint")
    def post(self, encoder_id: int) -> dict:
        """Get the details of an encoder

        Returns:
            Response: Flask response object
        """

        encoder = EncoderDAO.get_by_id(encoder_id)
        last_status = encoder.last_hearbeat
        if not last_status.is_reachable and last_status.message != "OK":
            status = [
                {
                    "hostname": encoder.hostname,
                    "status": "unreachable",
                }
            ]
            mail_service = MailService(logger=logger)
            mail_service.send_encoder_status_email(
                encoder.encoder_group.room.indico_name,
                status,
                notification_email=encoder.notification_email,
            )
            return {"message": "Email sent"}
        else:
            return {"message": "Encoder is reachable. Email not sent"}


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class EncodersListEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoder_list_endpoint")
    @namespace.marshal_with(encoder_model, as_list=True)
    def get(self) -> list[Encoder]:
        """Get the details of an encoder

        Returns:
            Response: Flask response object
        """

        encoders = EncoderDAO.get_all()

        return encoders


@namespace.doc(security=["Bearer Token"])
@namespace.route("/check/")
class EncodersCheckEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoder_list_endpoint")
    def post(self) -> dict:
        """Check the status of all encoders

        Returns:
            Response: Flask response object
        """

        check_encoders_task.delay()

        return {"message": "OK"}
