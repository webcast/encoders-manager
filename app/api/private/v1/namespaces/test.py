import logging
from typing import Union

from flask_restx import Namespace, Resource

from app.api.api_authorizations import authorizations
from app.api.decorators import roles_required
from app.daos.encoder_group import EncoderGroupDAO
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "test",
    description="Test operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/start/")
class StartTestEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("test_endpoint")
    def put(self, group_id: int) -> Union[dict, None]:
        """Operate over an encoder group

        Returns:
            Response: Flask response object
        """

        result = self.handle_start_test(group_id)

        logger.debug("Update encoder group with action: start_test")

        return result

    def handle_start_test(self, group_id: int):
        encoder_group = EncoderGroupDAO.get_by_id(group_id)
        logger.debug(
            f"{encoder_group.name} - {encoder_group.room.indico_name}: " "Handling start_test for encoder group"
        )

        if encoder_group.is_running or encoder_group.is_test_running:
            messages = []
            message1 = "Encoder group is running or test is running. " "Please, stop it first before starting it again."
            logger.warning(message1)
            messages.append(message1)
            if encoder_group.indico_event:
                message2 = (
                    f"Indico event {encoder_group.indico_event.indico_id} - "
                    f"{encoder_group.indico_event.title} "
                    "is associated to the encoder group"
                )
                logger.warning(message2)
                messages.append(message2)
            return {
                "error": True,
                "result": None,
                "rollback": None,
                "message": " ".join(messages),
            }

        start_result = encoder_group.start_test()

        logger.debug(f"Start test result before: {start_result}")

        # For each result in status["results"] Replace the value of key "status"
        # with the value of the enum
        for encoder_hostname in start_result["result"]:
            status = start_result["result"][encoder_hostname]["status"]
            start_result["result"][encoder_hostname]["status"] = status.value

        for encoder_hostname in start_result["rollback"]:
            status = start_result["rollback"][encoder_hostname]["status"]
            start_result["rollback"][encoder_hostname]["status"] = status.value

        logger.debug(f"Start test result: {start_result}")

        return start_result


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/stop/")
class StopTestEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("test_endpoint")
    def put(self, group_id: int) -> Union[dict, None]:
        """Operate over an encoder group

        Returns:
            Response: Flask response object
        """

        result = self.handle_stop_test(group_id)

        logger.debug("Update encoder group with action: stop_test")

        return result

    def handle_stop_test(self, group_id: int):
        encoder_group = EncoderGroupDAO.get_by_id(group_id)
        if not encoder_group.is_test_running:
            messages = []
            message1 = "Test is not running."
            logger.warning(message1)
            messages.append(message1)
            return {"error": True, "message": " ".join(messages), "status_code": 400}

        stop_result = encoder_group.stop_test()

        for encoder_hostname in stop_result["result"]:
            try:
                logger.debug(f"Encoder hostname: {encoder_hostname}")
                status = stop_result["result"][encoder_hostname]["status"]
                stop_result["result"][encoder_hostname]["status"] = status.value
            except (TypeError, KeyError) as erro:
                logger.warning(f"API: No key found: {erro}")

        for encoder_hostname in stop_result["rollback"]:
            try:
                status = stop_result["rollback"][encoder_hostname]["status"]
                stop_result["rollback"][encoder_hostname]["status"] = status.value
            except (TypeError, KeyError) as erro:
                logger.warning(f"API: No key found: {erro}")

        logger.debug(f"Stop test result: {stop_result}")
        return stop_result
