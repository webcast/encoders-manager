import logging
from typing import Union

from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.api.decorators import roles_required
from app.daos.encoder_group import EncoderGroupDAO
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "all",
    description="Stream and recording operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


action_payload = namespace.model(
    "ActionPayload",
    {
        "indicoId": fields.String(required=False, description="The indico event id to start the stream"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/start/")
class StartStreamEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoders_group_endpoint")
    @namespace.expect(action_payload)
    def put(self, group_id: int) -> Union[dict, None]:
        """Start both stream and recording

        Returns:
            Response: Flask response object
        """

        indico_id = namespace.payload.get("indicoId", None)

        result = self.handle_start(group_id, indico_id)

        logger.debug(f"Update encoder group with action: start_stream and Indico ID: {indico_id}")

        return result

    def handle_start(self, group_id: int, indico_id: str):
        encoder_group = EncoderGroupDAO.get_by_id(group_id)
        logger.debug(
            f"{encoder_group.name} - {encoder_group.room.indico_name}: " "Handling start_stream for encoder group"
        )

        if encoder_group.is_running or encoder_group.is_test_running:
            messages = []
            message1 = "Encoder group is running or test is running. " "Please, stop it first before starting it again."
            logger.warning(message1)
            messages.append(message1)
            if encoder_group.indico_event:
                message2 = (
                    f"Indico event {encoder_group.indico_event.indico_id} - "
                    f"{encoder_group.indico_event.title} "
                    "is associated to the encoder group"
                )
                logger.warning(message2)
                messages.append(message2)
            return {
                "error": True,
                "result": None,
                "message": " ".join(messages),
                "status_code": 400,
            }

        start_result = encoder_group.start_all(indico_id)

        # For each result in status["results"] Replace the value of key "status"
        # with the value of the enum
        for encoder_hostname in start_result["result"]:
            status = start_result["result"][encoder_hostname]["status"]
            start_result["result"][encoder_hostname]["status"] = status.value

        for encoder_hostname in start_result["rollback"]:
            status = start_result["rollback"][encoder_hostname]["status"]
            start_result["rollback"][encoder_hostname]["status"] = status.value

        logger.debug(f"Start stream result: {start_result}")

        return start_result


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/stop/")
class StopAllEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("all_endpoint")
    @namespace.expect(action_payload)
    def put(self, group_id: int) -> Union[dict, None]:
        """Operate over an encoder group

        Returns:
            Response: Flask response object
        """

        result = self.handle_stop(group_id)

        logger.debug("Update encoder group with action: stop_all")

        return result

    def handle_stop(self, group_id: int):
        encoder_group = EncoderGroupDAO.get_by_id(group_id)
        if not encoder_group.is_running:
            messages = []
            message1 = "Encoder is not running."
            logger.warning(message1)
            messages.append(message1)
            return {"error": True, "message": " ".join(messages), "status_code": 400}

        stop_result = encoder_group.stop_all()

        for encoder_hostname in stop_result["result"]:
            try:
                logger.debug(f"Encoder hostname: {encoder_hostname}")
                status = stop_result["result"][encoder_hostname]["status"]
                stop_result["result"][encoder_hostname]["status"] = status.value
            except (TypeError, KeyError) as erro:
                logger.warning(f"API: No key found: {erro}")

        for encoder_hostname in stop_result["rollback"]:
            try:
                status = stop_result["rollback"][encoder_hostname]["status"]
                stop_result["rollback"][encoder_hostname]["status"] = status.value
            except (TypeError, KeyError) as erro:
                logger.warning(f"API: No key found: {erro}")

        logger.debug(f"Stop test result: {stop_result}")
        return stop_result
