import logging
from typing import Union

from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.api.decorators import roles_required
from app.daos.encoder_group import EncoderGroupDAO
from app.models.encoder_groups import EncoderGroup
from app.models.indico_events import EventType
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "recording",
    description="Recording only operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


action_payload = namespace.model(
    "ActionPayload",
    {
        "indicoId": fields.String(required=False, description="The indico event id to start the stream"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/start/")
class RecordingStartEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("recording_endpoint")
    @namespace.expect(action_payload)
    def put(self, group_id: int) -> Union[dict, None]:
        """Start a recording

        Returns:
            Response: Flask response object
        """

        indico_id = namespace.payload.get("indicoId", None)

        result = handle_start_recording(group_id, indico_id)

        logger.debug(f"Update encoder group with action: " f"start_recording and Indico ID: {indico_id}")

        return result


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/restart/")
class RecordingRestartEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("recording_endpoint")
    @namespace.expect(action_payload)
    def put(self, group_id: int) -> Union[dict, None]:
        """Restart a recording without configuring the encoders again

        Returns:
            Response: Flask response object
        """

        indico_id = namespace.payload.get("indicoId", None)

        result = handle_restart_recording(group_id, indico_id)

        logger.debug("Update encoder group with action: " f"restart_recording and Indico ID: {indico_id}")

        return result


def handle_start_recording(group_id: int, indico_id: str):
    encoder_group = EncoderGroupDAO.get_by_id(group_id)
    logger.debug(
        f"{encoder_group.name} - {encoder_group.room.indico_name}: " "Handling start_recording for encoder group"
    )

    if encoder_group.is_running or encoder_group.is_test_running:
        messages = []
        message1 = "Encoder group is running or test is running. " "Please, stop it first before starting it again."
        logger.warning(message1)
        messages.append(message1)
        if encoder_group.indico_event:
            message2 = (
                f"Indico event {encoder_group.indico_event.indico_id} - "
                f"{encoder_group.indico_event.title} "
                "is associated to the encoder group"
            )
            logger.warning(message2)
            messages.append(message2)
        return {
            "error": True,
            "result": [],
            "message": " ".join(messages),
            "status_code": 400,
        }

    start_result = encoder_group.start_recording(indico_id, is_restart=False)

    # For each result in status["results"] Replace the value of key "
    # status" with the value of the enum
    for encoder_hostname in start_result["result"]:
        status = start_result["result"][encoder_hostname]["status"]
        start_result["result"][encoder_hostname]["status"] = status.value

    for encoder_hostname in start_result["rollback"]:
        status = start_result["rollback"][encoder_hostname]["status"]
        start_result["rollback"][encoder_hostname]["status"] = status.value

    logger.debug(f"Start stream result: {start_result}")

    return start_result


def handle_restart_recording(group_id: int, indico_id: str):
    encoder_group = EncoderGroupDAO.get_by_id(group_id)
    logger.debug(
        f"{encoder_group.name} - {encoder_group.room.indico_name}: " "Handling restart_recording for encoder group"
    )

    if encoder_group.is_test_running:
        messages = []
        message1 = "Encoder group test is running. " "Please, stop it first before restarting it."
        logger.warning(message1)
        messages.append(message1)
        if not encoder_group.indico_event:
            message2 = "No Indico event is associated to the encoder group"
            logger.warning(message2)
            messages.append(message2)
        return {
            "error": True,
            "result": [],
            "message": " ".join(messages),
            "status_code": 400,
        }

    start_result = encoder_group.start_recording(indico_id, is_restart=True)

    # For each result in status["results"] Replace the value of key
    # "status" with the value of the enum
    for encoder_hostname in start_result["result"]:
        status = start_result["result"][encoder_hostname]["status"]
        start_result["result"][encoder_hostname]["status"] = status.value

    for encoder_hostname in start_result["rollback"]:
        status = start_result["rollback"][encoder_hostname]["status"]
        start_result["rollback"][encoder_hostname]["status"] = status.value

    logger.debug(f"Start stream result: {start_result}")

    return start_result


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/stop/")
class RecordingStopEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("recording_endpoint")
    def put(self, group_id: int) -> Union[dict, None]:
        """Stop the recording

        Returns:
            Response: Flask response object
        """

        result = self.handle_stop_recording(group_id)

        logger.debug(f"Update encoder group {group_id} with action: stop_recording")

        return result

    def handle_stop_recording(self, group_id: int):
        encoder_group: EncoderGroup = EncoderGroupDAO.get_by_id(group_id)
        if not encoder_group.is_running:
            messages = []
            message1 = "Encoder is not running."
            logger.warning(message1)
            messages.append(message1)
            return {"error": True, "message": " ".join(messages), "status_code": 400}

        if encoder_group.indico_event and encoder_group.indico_event.event_type != EventType.RECORDING:
            stop_result = encoder_group.stop_recording()
        else:
            stop_result = encoder_group.stop_all()

        for encoder_hostname in stop_result["result"]:
            try:
                logger.debug(f"Encoder hostname: {encoder_hostname}")
                status = stop_result["result"][encoder_hostname]["status"]
                stop_result["result"][encoder_hostname]["status"] = status.value
            except (TypeError, KeyError) as erro:
                logger.warning(f"API: No key found: {erro}")

        for encoder_hostname in stop_result["rollback"]:
            try:
                status = stop_result["rollback"][encoder_hostname]["status"]
                stop_result["rollback"][encoder_hostname]["status"] = status.value
            except (TypeError, KeyError) as erro:
                logger.warning(f"API: No key found: {erro}")

        logger.debug(f"Stop test result: {stop_result}")
        return stop_result
