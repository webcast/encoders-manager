import logging
from typing import Union

from flask_restx import Namespace, Resource

from app.api.api_authorizations import authorizations
from app.api.decorators import roles_required
from app.daos.encoder_group import EncoderGroupDAO
from app.models.encoder_groups import EncoderGroup
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "status",
    description="Get status operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/")
class StartStreamEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("get_status_endpoint")
    def put(self, group_id: int) -> Union[dict, None]:
        """Get the status of a group

        Returns:
            Response: Flask response object
        """

        result = handle_get_status(group_id)

        logger.debug("Get status from encoder group with action: get_status")

        return result


def handle_get_status(group_id: int):
    encoder_group: EncoderGroup = EncoderGroupDAO.get_by_id(group_id)
    logger.debug(f"{encoder_group.name}: Handling get_status for encoder group")

    status_result = encoder_group.get_status()

    # For each result in status["results"] Replace the value of key "status"
    # with the value of the enum
    for encoder_hostname in status_result["result"]:
        status = status_result["result"][encoder_hostname]["status"]
        status_result["result"][encoder_hostname]["status"] = status.value

    for encoder_hostname in status_result["rollback"]:
        status = status_result["rollback"][encoder_hostname]["status"]
        status_result["rollback"][encoder_hostname]["status"] = status.value

    logger.debug(f"Get status result: {status_result}")

    return status_result
