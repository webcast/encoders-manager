import logging
from datetime import datetime
from typing import List

from flask_login import current_user
from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.api.decorators import roles_required
from app.daos.indico_events import IndicoEventsDAO
from app.daos.role import RoleDAO
from app.extensions import db
from app.models.indico_events import Audience, EventType, IndicoEvent
from app.models.rooms import Room
from app.services.indico.indico_service import IndicoService
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "indico",
    description="Indico Events",
    authorizations=authorizations,
    security=["Bearer Token"],
)

event_model = namespace.model(
    "IndicoEventModel",
    {
        "id": fields.Integer(required=True, description="The event ID"),
        "title": fields.String(required=True, description="The event title", attribute="title"),
        "audience": fields.String(attribute="audience.value"),
        "indicoId": fields.String(attribute="indico_id"),
        "indicoUrl": fields.String(attribute="indico_url"),
        "startDateTime": fields.DateTime(attribute="start_datetime"),
        "endDateTime": fields.DateTime(attribute="end_datetime"),
        "isLocked": fields.Boolean(attribute="is_locked"),
        "isStreamed": fields.Boolean(attribute="is_streamed"),
        "isRunning": fields.Boolean(attribute="is_running"),
        "isRecorded": fields.Boolean(attribute="is_recorded"),
        "roomIndicoName": fields.String(attribute="room.indico_name"),
        "roomId": fields.Integer(attribute="room.id"),
        "autoStopDateTime": fields.DateTime(attribute="auto_stop_datetime"),
        "eventType": fields.String(attribute="event_type.value"),
        "encoderGroupId": fields.Integer(attribute="encoder_group_id"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class IndicoEventEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("indico_endpoint")
    @namespace.marshal_with(event_model, as_list=True)
    def get(self) -> list[IndicoEvent]:
        """Get the indico events

        Returns:
            Response: Flask response object
        """
        today = datetime.today().date()
        midnight_datetime = datetime.combine(today, datetime.min.time())

        events = []
        if current_user.is_admin:
            logger.debug(f"User is admin. Fetching all events from {midnight_datetime}")
            events = IndicoEventsDAO.get_next_events_admin()
        else:
            user_roles = [role.strip() for role in current_user.roles.split(",")]
            app_roles = RoleDAO.get_user_app_roles(user_roles)
            # Get all the rooms from each role
            rooms_from_roles: List[Room] = []
            for role in app_roles:
                rooms_from_roles.extend(role.rooms)
            room_names = ", ".join([room.indico_name for room in rooms_from_roles])
            logger.debug("User is not admin. Fetching only events from " f"selected rooms: {room_names}")
            rooms_with_access = [room.id for room in rooms_from_roles]
            events = IndicoEventsDAO.get_next_events_from_rooms(rooms_with_access)
        logger.debug(current_user.roles)

        return events


@namespace.doc(security=["Bearer Token"])
@namespace.route("/fetch/")
class IndicoEventsFetchEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("indico_endpoint")
    @namespace.marshal_with(event_model, as_list=True)
    def post(self) -> list[IndicoEvent]:
        """Get the indico events

        Returns:
            Response: Flask response object
        """
        indico_service = IndicoService()
        events = indico_service.get_events()

        return events


single_event_payload = namespace.model(
    "SingleEventPayload",
    {
        "indicoId": fields.String(required=True, description="The indico event ID"),
        "audience": fields.String(required=True, description="The audience", enum=[a.value for a in Audience]),
        "eventType": fields.String(
            required=True,
            description="The event type",
            enum=[a.value for a in EventType],
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/fetch-event/")
class IndicoEventFetchSingleEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("indico_endpoint")
    @namespace.marshal_with(event_model)
    @namespace.expect(single_event_payload)
    def post(self) -> IndicoEvent:
        """Get the indico events

        Returns:
            Response: Flask response object
        """

        audience = Audience(namespace.payload["audience"])
        event_type = EventType(namespace.payload["eventType"])
        indico_id = namespace.payload["indicoId"]

        indico_service = IndicoService()

        logger.debug(f"Fetching Indico event {indico_id}")
        logger.debug(f"- Audicence {audience}")
        logger.debug(f"- Type {event_type}")

        result = indico_service.get_event(indico_id, event_type, audience)
        logger.debug(result.event_type)
        logger.debug(result.audience)

        return result


update_event_payload = namespace.model(
    "UpdateEventPayload",
    {
        "isLocked": fields.Boolean(required=False, description="The event lock status"),
        "autoStopDatetime": fields.DateTime(required=False, description="The event stop datetime"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:event_id>/")
class IndicoEventDetailsEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("indico_endpoint")
    @namespace.marshal_with(event_model)
    @namespace.expect(update_event_payload)
    def patch(self, event_id: int) -> IndicoEvent:
        """Get the indico events

        Returns:
            Response: Flask response object
        """
        event = IndicoEventsDAO.get_by_id(event_id)
        logger.debug(namespace.payload)
        # If isLocked in keys
        if "isLocked" in namespace.payload:
            is_locked = namespace.payload["isLocked"]
            event.is_locked = is_locked
            logger.debug(f"Set is_locked to: {is_locked}")

        if "autoStopDatetime" in namespace.payload:
            auto_stop_datetime = namespace.payload["autoStopDatetime"]
            datetime_object = None
            if auto_stop_datetime:
                datetime_object = datetime.strptime(auto_stop_datetime, "%Y-%m-%d %H:%M")
            event.auto_stop_datetime = datetime_object
            event.auto_stopped = False
            logger.debug(f"Set auto_stop_datetime to: {datetime_object}")

        db.session.commit()

        return event
