import logging

from flask_restx import Namespace, Resource, fields, reqparse

from app.api.api_authorizations import authorizations
from app.api.decorators import roles_required
from app.daos.encoder_group import EncoderGroupDAO
from app.models.encoder_groups import EncoderGroup
from app.utils.auth.client_oidc import oidc_validate

# from app.daos.accounts import AccountDAO
# from app.models.accounts import Account


logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "encoder-groups",
    description="Encoder Groups",
    authorizations=authorizations,
    security=["Bearer Token"],
)

encoder_model = namespace.model(
    "EncoderModel",
    {
        "id": fields.Integer(required=True, description="The ID"),
        "hostname": fields.String(required=True, description="The encoder hostname", attribute="hostname"),
        "recordingRunning": fields.Boolean(attribute="recording_running"),
        "streamRunning": fields.Boolean(attribute="stream_running"),
        "encoderType": fields.String(attribute="encoder_type.value"),
        "signalType": fields.String(attribute="signal_type.value"),
        "encoderGroupId": fields.Integer(attribute="encoder_group_id"),
        "streamStartDatetime": fields.DateTime(attribute="stream_start_datetime"),
        "streamServer": fields.String(attribute="stream_server"),
        "streamName": fields.String(attribute="stream_name"),
        "streamApplication": fields.String(attribute="stream_application"),
        "originServer": fields.String(attribute="origin_no_protocol"),
        "edgeServer": fields.String(attribute="edge_server"),
    },
)

encoder_group_model = namespace.model(
    "EncoderGroupModel",
    {
        "id": fields.Integer(required=True, description="The ID"),
        "name": fields.String(required=True, description="The group name", attribute="name"),
        "isTestRunning": fields.Boolean(attribute="is_test_running"),
        "isRunning": fields.Boolean(attribute="is_running"),
        "recordingPath": fields.String(attribute="recording_path"),
        "encoders": fields.List(fields.Nested(encoder_model)),
        "roomId": fields.Integer(attribute="room_id"),
        "indicoEventId": fields.String(attribute="indico_event.indico_id"),
        "indicoEventTitle": fields.String(attribute="indico_event.title"),
        "indicoEventType": fields.String(attribute="indico_event.event_type.value"),
        "isStreamRunning": fields.Boolean(attribute="is_stream_running"),
        "isRecordingRunning": fields.Boolean(attribute="is_recording_running"),
        "startAsComposite": fields.Boolean(attribute="start_as_composite"),
        "roomName": fields.String(attribute="room.indico_name"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class EncoderGroupEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    parser = reqparse.RequestParser()
    parser.add_argument(
        "running",
        type=bool,
        help="Whether or not to retrieve only running encoders groups",
    )
    parser.add_argument(
        "notRunning",
        type=bool,
        help="Whether or not to retrieve only not running encoders groups",
    )

    @namespace.doc("encoders_group_room_endpoint")
    @namespace.marshal_with(encoder_group_model, as_list=True)
    def get(self) -> list[EncoderGroup]:
        """Get all the encoders groups

        Returns:
            Response: Flask response object
        """

        # Get running query param
        args = self.parser.parse_args()
        only_running = args.get("running", False)
        only_not_running = args.get("notRunning", False)

        if only_running:
            encoders_groups = EncoderGroupDAO.get_all_running()
        elif only_not_running:
            encoders_groups = EncoderGroupDAO.get_all_not_running()
        else:
            encoders_groups = EncoderGroupDAO.get_all()

        return encoders_groups


@namespace.doc(security=["Bearer Token"])
@namespace.route("/room/<int:room_id>/")
class EncoderGroupRoomEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoders_group_room_endpoint")
    @namespace.marshal_with(encoder_group_model, as_list=True)
    def get(self, room_id: int) -> list[EncoderGroup]:
        """Get the encoders groups in a room

        Returns:
            Response: Flask response object
        """
        encoders_groups = EncoderGroupDAO.get_by_room_id(room_id)

        return encoders_groups


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:group_id>/")
class EncoderGroupDetailsEndpoint(Resource):
    method_decorators = [roles_required, oidc_validate]

    @namespace.doc("encoders_group_endpoint")
    @namespace.marshal_with(encoder_group_model, as_list=False)
    def get(self, group_id: int) -> list[EncoderGroup]:
        """Get the encoder group details

        Returns:
            Response: Flask response object
        """
        encoders_groups = EncoderGroupDAO.get_by_id(group_id)

        return encoders_groups
