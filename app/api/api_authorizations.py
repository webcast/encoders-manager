authorizations = {
    "Bearer Token": {
        "type": "apiKey",
        "in": "header",
        "name": "Authorization",
        "description": ("Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, " "where JWT is the token"),
    },
    "Digest Auth": {"type": "basic"},
}
