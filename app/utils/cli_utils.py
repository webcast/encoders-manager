import enum
import logging

import click
from flask.cli import AppGroup

from app.models.encoders.epiphan_pearl2 import EpiphanPearl2
from app.models.encoders.epiphan_pearl_nano import EpiphanPearlNano
from app.services.encoders.epiphan.epiphan_nano_api_client import RecordingTimeLimits
from app.tasks.auto_stop import stop_events_task
from app.tasks.example import dummy_task
from app.tasks.reminders import send_reminders_task

logger = logging.getLogger("webapp.cli")

cli = AppGroup("utils")


@cli.command("check-stop")
def check_stop():
    logger.info("Triggering stop check")
    stop_events_task()


@cli.command("dummy")
def dummy():
    logger.info("Triggering dummy task")
    dummy_task.apply_async()


@cli.command("reminders")
def reminders():
    logger.info("Triggering send reminders task")
    send_reminders_task()


class SizeValues(enum.Enum):
    ONE_GB = "1"
    TWO_GB = "2"
    THREE_GB = "3"
    FOUR_GB = "4"
    EIGHT_GB = "8"
    TWELVE_GB = "12"
    SIXTEEN_GB = "16"


@cli.command("size-limit")
@click.argument("limit", type=click.Choice([limit.value for limit in SizeValues]))
def set_size_limit(limit: str):
    size = int(limit)
    size = size * 1000000

    enanos: list[EpiphanPearlNano] = EpiphanPearlNano.query.all()
    results = []
    for epn in enanos:
        result = epn.set_recording_size_limit(size)
        results.append(result)

    # Log the number of EncoderStatus.OK and EncoderStatus.ERROR
    results = [result for result in results if result == "OK"]
    logger.info(f"Set size limit to {size} for {len(results)} encoders of {len(enanos)}")


@cli.command("time-limit")
@click.argument("limit", type=click.Choice([limit.value for limit in RecordingTimeLimits]))
def set_time_limit(limit: str):

    enanos: list[EpiphanPearlNano] = EpiphanPearlNano.query.all()
    results = []
    for epn in enanos:
        result = epn.set_recording_time_limit(limit)
        results.append(result)

    # Log the number of EncoderStatus.OK and EncoderStatus.ERROR
    results = [result for result in results if result == "OK"]
    logger.info(f"Set time limit to {limit} for {len(results)} encoders of {len(enanos)}")


@cli.command("active-layout")
@click.argument("hostname", type=str)
@click.argument("layout_index", type=int)
@click.argument("active_channel", type=int, default=1)
def set_active_layout(hostname: str, layout_index: int, active_channel: int):

    epiphan_pearl2: EpiphanPearl2 = EpiphanPearl2.query.filter_by(hostname=hostname).first()
    if not epiphan_pearl2:
        logger.error(f"Epiphan Pearl 2 with hostname {hostname} not found")
        return

    result = epiphan_pearl2.set_active_layout(layout_index, active_channel)

    logger.info(f"Set active layout to {layout_index} for {hostname}: {result}")


# @indico_cli.command("import-event")
# @click.argument("indico_id")
# def import_event(indico_id: str):
#     logger.info("Importing event from Indico")
#     service = IndicoService(logger=logger)
#     service.get_event(indico_id)
