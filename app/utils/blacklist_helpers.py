import logging
from datetime import datetime

from flask_jwt_extended import create_access_token, decode_token

from app.daos.api_tokens import ApiTokenDAO
from app.extensions import db
from app.models.token_blacklist import ApiToken

logger = logging.getLogger("webapp.blacklist_helper")


class TokenNotFound(Exception):
    """
    Indicates that a token could not be found in the database
    """


def _epoch_utc_to_datetime(epoch_utc):
    """
    Helper function for converting epoch timestamps (as stored in JWTs) into
    python datetime objects (which are easier to use with sqlalchemy).
    """
    return datetime.fromtimestamp(epoch_utc)


def update_token_in_database(token: ApiToken) -> ApiToken:
    """
    Adds a new token to the database. It is not revoked when it is added.
    :param identity_claim:
    """
    encoded_token = create_access_token(identity=token.name)
    decoded_token = decode_token(encoded_token)
    jti = decoded_token["jti"]
    token_type = decoded_token["type"]
    user_identity = decoded_token["sub"]

    token.access_token = encoded_token
    token.jti = jti
    token.token_type = token_type
    token.user_identity = user_identity
    db.session.commit()

    return token


def is_token_revoked(decoded_token: dict) -> bool:
    """Checks if the given token is revoked or not. Because we are adding all the
    tokens that we create into this database, if the token is not present
    in the database we are going to consider it revoked, as we don't know where
    it was created.

    Args:
        decoded_token (dict): A dict with the jti value in it

    Returns:
        bool: Whether the token is revoked or not
    """
    jti = decoded_token["jti"]
    token = ApiTokenDAO.get_by_jti(jti)
    if token:
        return token.revoked
    else:
        return True


def prune_database():
    """
    Delete tokens that have expired from the database.
    How (and if) you call this is entirely up you. You could expose it to an
    endpoint that only administrators could call, you could run it as a cron,
    set it up with flask cli, etc.
    """
    ApiTokenDAO.delete_expired_tokens()


def check_if_token_revoked(jwt_header: dict, jwt_payload: dict) -> bool:
    """Check if the token is revoked.

    Args:
        jwt_header (dict): The header of the JWT token
        jwt_payload (dict): The payload of the JWT token

    Returns:
        bool: Whether the token is revoked or not
    """
    # pylint: disable=unused-argument
    return is_token_revoked(jwt_payload)
