import logging
from threading import Thread

from flask import current_app

logger = logging.getLogger("webapp.thread")


class FlaskThread(Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = current_app._get_current_object()

    def run(self):
        self.exc = None
        try:
            with self.app.app_context():
                super().run()
        except BaseException as e:
            self.exc = e

    def join(self):
        Thread.join(self)
        # Since join() returns in caller thread
        # we re-raise the caught exception
        # if any was caught
        if self.exc:
            raise self.exc
