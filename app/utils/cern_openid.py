import datetime
import logging

import requests
import sqlalchemy
from authlib.jose import jwk, jwt
from flask import Flask, current_app, session
from flask_dance import OAuth2ConsumerBlueprint
from flask_dance.consumer import oauth_authorized
from flask_dance.consumer.storage.sqla import SQLAlchemyStorage
from flask_login import LoginManager, current_user, login_user

from app.extensions import cache, db
from app.models.users import BackendUser, OAuth

logger = logging.getLogger("webapp.openid")


def get_user_egroups_from_session():
    """
    Retrieves the egroups of the user from the oauth api or from the cache
    :type username: string used to identify the user. Used for caching purposes
    :return:
    """
    try:
        roles = session["roles"]
        return roles
    except KeyError as error:
        logger.info(f"User does not have roles. He is annonymous ({error})")
        return []


def load_cern_openid(application: Flask):
    """
    Loads the CERN Openid into the application

    :param app: Flask application where the CERN Openid will be loaded
    :return:
    """
    authorization_url = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth"
    openid = OAuth2ConsumerBlueprint(
        "cern_openid",
        __name__,
        url_prefix="/openid",
        # openid specific settings
        token_url="https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token",
        authorization_url=authorization_url,
        # local urls
        login_url="/cern/",
        redirect_url="/admin/",
        authorized_url="/cern/authorized",
        client_id=application.config["OIDC_CONFIG_BACKEND"]["OIDC_CLIENT_ID"],
        client_secret=application.config["OIDC_CONFIG_BACKEND"]["OIDC_CLIENT_SECRET"],
    )
    application.register_blueprint(openid)
    openid.storage = SQLAlchemyStorage(OAuth, db.session, user=current_user, cache=cache)

    # setup login manager
    login_manager = LoginManager()
    login_manager.login_view = "cern_openid.login"

    @login_manager.user_loader
    def load_user(user_id):
        try:
            user = db.session.execute(db.select(BackendUser).filter_by(id=user_id)).scalar_one_or_none()
            return user
        except sqlalchemy.exc.InternalError as error:
            logger.warning(str(error))
            return None

    login_manager.init_app(application)

    @oauth_authorized.connect_via(openid)
    def cern_logged_in(blueprint, token):
        # it anymore after getting the data here.
        # 'https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo'
        # pylint: disable=unused-argument
        logger.debug("CERN Openid logged in")

        def load_key(header, payload):
            # pylint: disable=unused-argument
            jwk_set = requests.get(current_app.config["OIDC_CONFIG_BACKEND"]["OIDC_JWKS_URL"]).json()
            key_store = jwk.loads(jwk_set, header.get("kid"))
            return key_store

        payload = jwt.decode(token["access_token"], key=load_key)
        logger.debug(f"CERN Openid payload: {payload}")
        query = db.select(BackendUser).filter_by(username=payload["cern_upn"].strip())
        existing_user = db.session.execute(query).scalar_one_or_none()

        if not existing_user:
            try:
                first_name = payload["given_name"].strip()
            except AttributeError:
                first_name = "Unknown"

            try:
                last_name = payload["family_name"].strip()
            except AttributeError:
                last_name = "Unknown"

            existing_user = BackendUser(
                username=payload["cern_upn"].strip(),
                email=payload["email"],
                last_name=last_name,
                first_name=first_name,
            )

            db.session.add(existing_user)

        admin_role = current_app.config["ADMIN_ROLE"]
        is_admin = False
        if admin_role in payload["cern_roles"]:
            is_admin = True
        logger.debug(payload["cern_roles"])
        existing_user.roles = ",".join(payload["cern_roles"])
        existing_user.is_admin = is_admin
        existing_user.last_login = datetime.datetime.now()
        db.session.commit()

        session["roles"] = payload["cern_roles"]

        if login_user(existing_user):
            logger.info(f"Login user {existing_user.username} " f"with username: {existing_user.username}")

    return openid
