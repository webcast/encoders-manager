from flask_testing import TestCase

from app.app_factory import create_app
from app.extensions import db


def create_test_app():
    application = create_app("config/config.test.py")

    return application


class BaseTestCase(TestCase):
    def create_app(self):
        return create_test_app()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
