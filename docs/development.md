# Development

## Requirements

- Python 3.9
- Node v17.5

## Technologies

- Flask
- React

## Local install

### Database

You can install a database of your choice in your computer.

- MySQL: https://dev.mysql.com/downloads/mysql/

### Dependencies

### Python dependencies using Poetry

Poetry will take all the information on the `pyproject.toml` file and will install all its dependencoies.

You can install Poetry using the following command:

```bash
pip install poetry
```

#### Install dependencies and create the virtual environment

Once poetry is installed, install the dependencies:

```bash
poetry install
```

Activate the virtual env:
```bash
source .venv/bin/activate
```

### Node and React dependencies

Install the project dependencies for Javascript/Typescript:

```bash
cd app/client
npm install
```

### Install the pre-commit Git hook

In order to have a common code styling, we use several tools (Eslint, Black, Prettier, etc) that verify that the code
is written correctly.

This hook should be already installed after running `npm install` since it uses Husky to set the pre-commit hooks.

These hooks will run the `app/client/.husky/pre-commit` scripts and will verify the Python and JS/TS code.

### Configuration file

Rename `app/config/config.py.sample` to `app/config/config.py` and populate it with the correct values (Database, paths, etc).

### Migrate the database

Run the migrations on the database

```bash
FLASK_APP=wsgi.py flask db upgrade
```

## Run

### Webapp

```bash
FLASK_APP=wsgi.py FLASK_DEBUG=true flask run
```

Another option is to use Visual Studio Code for running and debugging. In this case just go to the Visual Studio Code menu and select "Run" and "Run without debugging" or "Start debugging".

### Celery and Redis

#### Redis

Redis is required to run celery. It can be installed with the following commands depending on your platform:

- Ubuntu: `sudo apt-get install redis-server`
- Mac: `brew install redis`

Then run the server:

```bash
redis-server
```

You can run redis as well from the tasks in Visual Studio.

#### Celery

> Don't forget to set up the correct setting in the `config.py` file.

```bash
celery -A celery_app:celery worker
```

Another option is to use Visual Studio Code for running and debugging. In this case just go to the Visual Studio Code menu and select "Run" and "Run without debugging" or "Start debugging".

## Other Available commands

Get a list of the available commands running the following:

```bash
FLASK_APP=wsgi.py flask --help
```

## Links to libs and other docs

- Semantic UI React (UI): https://react.semantic-ui.com/
- Paas Docs: https://paas.docs.cern.ch/
