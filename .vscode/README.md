# VS Code Settings

This project includes a set of VS Code settings to ensure a consistent development environment for all contributors.

## Settings

- **Format on Save**: Automatically formats code when you save a file.
- **File Associations**: Associates specific file types with custom languages.
- **Code Actions on Save**: Automatically fixes issues and organizes imports on save.
- **Linting**: Uses ESLint for JavaScript and isort for Python to enforce coding standards.
- **Default Formatters**: Specifies default formatters for different languages to ensure consistent code formatting.
- **Environment Activation**: Automatically activates the Python virtual environment in the terminal.
- **Testing**: Enables pytest for running tests in Python files.

## Recommended Extensions

To ensure a consistent development environment, please install the following extensions:

- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [Black Formatter](https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter)
- [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Pylance](https://marketplace.visualstudio.com/items?itemName=ms-python.vscode-pylance)
- [DjLint](https://marketplace.visualstudio.com/items?itemName=monosans.djlint)

These extensions are recommended in the `.vscode/extensions.json` file.
