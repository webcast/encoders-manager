include:
  - project: "ci-tools/container-image-ci-templates"
    file: "kaniko-image.gitlab-ci.yml"
    ref: master
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

stages:
  - install_dependencies
  - test
  - build_app
  - build_container
  - redeploy

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PRE_COMMIT_HOME: .cache/pre-commit
  POETRY_HOME: "/opt/poetry"
  POETRY_VERSION: "2.1.1"
  PRE_COMMIT_VERSION: "4.1.0"

.cache_python_template:
  image: registry.cern.ch/docker.io/library/python:3.13
  cache:
    - key:
        files:
          - poetry.lock
      paths:
        - .cache/pip
        - .venv

.cache_node_template:
  image: registry.cern.ch/docker.io/library/node:20
  cache:
    - key:
        files:
          - package-lock.json
      paths:
        - app/client/.npm/
        - app/client/node_modules

.build_frontend_template:
  extends: .cache_node_template
  stage: build_app
  before_script:
    - cd app/client
  script:
    - npm run build
  artifacts:
    paths:
      - app/client/dist # The application is built and packaged

.redeploy_paas_template:
  stage: redeploy
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  variables:
    REGISTRY_APP_NAME: encoders
    OPENSHIFT_SERVER: https://api.paas.okd.cern.ch # use https://openshift-dev.cern.ch for a Test site
    OPENSHIFT_APP_NAME: encoders # this is the name of the ImageStream/DeploymentConfig objects created by oc new-app. Typically, this will be the name of the GitLab project.
  script:
    - "oc import-image $OPENSHIFT_APP_NAME --all --server=$OPENSHIFT_SERVER --namespace=$OPENSHIFT_NAMESPACE --from=registry.cern.ch/$REGISTRY_APP_NAME/$IMAGE --confirm --token=$OPENSHIFT_TOKEN"
    # wait a bit for redeployment to happen then monitor the deployment status
    - "sleep 30s && oc rollout status deploy/$OPENSHIFT_APP_NAME --server=$OPENSHIFT_SERVER --namespace $OPENSHIFT_NAMESPACE --token=$OPENSHIFT_TOKEN"

install_pip:
  extends: .cache_python_template
  stage: install_dependencies
  script:
    - python -V
    - pip install poetry==$POETRY_VERSION
    - poetry --version
    - poetry install

install_npm:
  extends: .cache_node_template
  stage: install_dependencies
  script:
    - cd app/client
    - npm ci --cache .npm --prefer-offline

pre_commit:
  image: registry.cern.ch/docker.io/library/python:3.13
  # Will run the tests with coverage.
  stage: test
  before_script:
    - pip install pre-commit==$PRE_COMMIT_VERSION
  script:
    - pre-commit run --all-files
  needs:
    - install_npm
  cache:
    - key:
        files:
          - .pre-commit-config.yaml
      paths:
        - .cache/pre-commit

# We test on all the branches
lint_frontend:
  extends: .cache_node_template
  stage: test
  before_script:
    - cd app/client
  script:
    - npm run lint
  needs:
    - install_npm

# We test on all the branches
test_frontend:
  extends: .cache_node_template
  stage: test
  before_script:
    - cd app/client
  script:
    - npm test --coverage
  needs:
    - install_npm

test_backend:
  extends: .cache_python_template
  # Will run the tests with coverage.
  stage: test
  before_script:
    - source .venv/bin/activate
  script:
    - cp app/config/config.sample.py app/config/config.py
    - pytest
  needs:
    - install_pip

build_frontend_qa:
  extends: .build_frontend_template
  variables:
    VITE_API_ENDPOINT: ${VITE_API_ENDPOINT_QA}
    VITE_OIDC_CLIENT_ID: ${VITE_OIDC_CLIENT_ID}
    VITE_OIDC_CLIENT_URL: ${VITE_OIDC_CLIENT_URL_QA}
    VITE_SENTRY_ENV: "qa"
    VITE_IS_DEV_INSTALL: "true"
  only:
    - qa

build_frontend_test:
  extends: .build_frontend_template
  variables:
    VITE_API_ENDPOINT: ${VITE_API_ENDPOINT_TEST}
    VITE_OIDC_CLIENT_ID: ${VITE_OIDC_CLIENT_ID}
    VITE_OIDC_CLIENT_URL: ${VITE_OIDC_CLIENT_URL_TEST}
    VITE_SENTRY_ENV: "test"
    VITE_IS_DEV_INSTALL: "true"
  only:
    - test

build_frontend_prod:
  extends: .build_frontend_template
  variables:
    VITE_API_ENDPOINT: ${VITE_API_ENDPOINT_PROD}
    VITE_OIDC_CLIENT_ID: ${VITE_OIDC_CLIENT_ID}
    VITE_OIDC_CLIENT_URL: ${VITE_OIDC_CLIENT_URL_PROD}
    VITE_SENTRY_ENV: "prod"
    VITE_IS_DEV_INSTALL: "false"
  only:
    - main

build_container:
  only:
    - qa
    - main
    - test
  stage: build_container
  extends: .build_kaniko
  variables:
    PUSH_IMAGE: "true"
    REGISTRY_IMAGE_PATH: "registry.cern.ch/encoders/$CI_COMMIT_REF_NAME:$CI_COMMIT_TAG"

redeploy_test:
  extends: .redeploy_paas_template
  environment:
    name: test
  variables:
    OPENSHIFT_NAMESPACE: encoders-test # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_TEST
    IMAGE: test
  only:
    - test # production or qa, the branch you want to publish

redeploy_qa:
  extends: .redeploy_paas_template
  environment:
    name: qa
  variables:
    OPENSHIFT_NAMESPACE: encoders-qa # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_QA
    IMAGE: qa
  only:
    - qa # production or qa, the branch you want to publish

redeploy_prod:
  extends: .redeploy_paas_template
  environment:
    name: main
  variables:
    OPENSHIFT_NAMESPACE: encoders # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_PROD
    IMAGE: main
  only:
    - main # main or qa, the branch you want to publish
