"""Add encoder_camera/slides_type to IndicoEvent

Revision ID: aca9b91e5ab5
Revises: 9375d3935beb
Create Date: 2023-10-27 14:03:29.175259

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "aca9b91e5ab5"
down_revision = "9375d3935beb"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("indico_event", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("encoder_camera_type", sa.String(length=255), nullable=True)
        )
        batch_op.add_column(
            sa.Column("encoder_slides_type", sa.String(length=255), nullable=True)
        )

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("indico_event", schema=None) as batch_op:
        batch_op.drop_column("encoder_slides_type")
        batch_op.drop_column("encoder_camera_type")

    # ### end Alembic commands ###
