"""Add EPIPHAN_PEARL2 to EncoderType enum

Revision ID: dab5158c5a6f
Revises: 70ebf1f33c0f
Create Date: 2025-01-22 14:22:05.282924

"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "dab5158c5a6f"
down_revision = "70ebf1f33c0f"
branch_labels = None
depends_on = None


def upgrade():
    # Update the ENUM type for EncoderType
    op.execute(
        """
        ALTER TABLE encoder
        MODIFY encoder_type ENUM('MONARCH_HD','MONARCH_HDX','EPIPHAN_PEARL_NANO','EPIPHAN_PEARL2')
    """
    )


def downgrade():
    # Downgrade logic is complex for ENUMs; usually requires recreating the ENUM
    # Drop the 'EPIPHAN_PEARL2' value by creating a new ENUM type
    op.execute(
        """
        ALTER TABLE encoder
        MODIFY encoder_type ENUM('MONARCH_HD','MONARCH_HDX','EPIPHAN_PEARL_NANO')
    """
    )
