"""Add may to many rel to encoders and groups

Revision ID: 71a2c3dcfafa
Revises: 5225cd365005
Create Date: 2024-03-21 14:23:36.897911

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '71a2c3dcfafa'
down_revision = '5225cd365005'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('encoder_group_encoders',
    sa.Column('encoder', sa.Integer(), nullable=True),
    sa.Column('encoder_group', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['encoder'], ['encoder.id'], ),
    sa.ForeignKeyConstraint(['encoder_group'], ['encoder_group.id'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('encoder_group_encoders')
    # ### end Alembic commands ###
